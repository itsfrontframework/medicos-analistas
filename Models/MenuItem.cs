﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace base_front.Models
{
    /// <summary>
    /// Representa un elemento del menú principal
    /// </summary>
    public class MenuItem
    {
        /// <summary>	
        /// Nombre del módulo asociado a esta pantalla
        /// </summary>	
        [Required(ErrorMessage = "MENU_FR_MODULE")]
        [StringLength(maximumLength: 30, MinimumLength = 1, ErrorMessage = "MENU_SL_MODULE")]
        [RegularExpression(pattern: @"^[A-Z_]{3,30}$", ErrorMessage = "MENU_RE_MODULE")]
        public string MODULE { get; set; }

        /// <summary>	
        /// Nombre de la pantalla, utilizado por las vistas para solicitar la pantalla
        /// </summary>	
        [StringLength(maximumLength: 60, ErrorMessage = "MENU_SL_SCREEN")]
        [RegularExpression(pattern: @"^[A-Za-z0-9_-]{3,60}$", ErrorMessage = "MENU_RE_CODE")]
        public string SCREEN { get; set; }

        /// <summary>	
        /// Ubicación de la pantalla en el front-end
        /// </summary>
        [StringLength(maximumLength: 150, ErrorMessage = "MENU_SL_URL")]
        public string URL { get; set; }

        /// <summary>	
        /// Descripción de la pantalla
        /// </summary>
        [Required(ErrorMessage = "MENU_FR_DESCRIPTION")]
        [StringLength(maximumLength: 250, MinimumLength = 1, ErrorMessage = "MENU_SL_DESCRIPTION")]
        public string DESCRIPTION { get; set; }

        /// <summary>	
        /// Nombre que se muestra en el menú
        /// </summary>
        [StringLength(maximumLength: 150, ErrorMessage = "MENU_SL_LABEL")]
        public string LABEL { get; set; }

        /// <summary>	
        /// Icono que se utiliza en el menu
        /// </summary>
        [StringLength(maximumLength: 50, ErrorMessage = "MENU_SL_ICON")]
        public string ICON { get; set; }

        /// <summary>	
        /// Orden jerarquico donde se ubica en el menu
        /// </summary>
        [Required(ErrorMessage = "MENU_FR_ORDER")]
        public string POSITION { get; set; }

    }
}