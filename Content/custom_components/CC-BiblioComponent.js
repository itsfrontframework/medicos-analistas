﻿
var BiblioComponent = function (parent, data, params) {
    this.elementsContainer = null;
    this.addBtn = null;

    BiblioComponent.prototype.init = function (parent, data) {
        var Super = this;
        this.elementsContainer = $('<div></div>');

        var headerDiv = $('<div></div>').addClass('form-group');
        var headerlabel = $('<label></label>').addClass('col-sm-2 control-label').html(this.settings.title);
        var headerSpace = $('<div></div>').addClass('col-sm-8').css('border-bottom', 'solid 1px #C1C1C1').html('&nbsp;');
        var headerBtnCont = $('<div></div>').addClass('col-sm-1');
        this.addBtn = $('<a></a>').addClass('btn btn-xs btn-success').css('margin-top', '10px').css('width', '90%').html('Agregar');
        
        headerBtnCont.append(this.addBtn);
        headerDiv.append(headerlabel);
        headerDiv.append(headerSpace);
        headerDiv.append(headerBtnCont);

        $(parent).append(headerDiv);
        $(parent).append(this.elementsContainer);

        this.addBtn.click(function () {
            Super.onAddClick();
        });

        if (this.settings.disabled) {
            Super.addBtn.addClass('disabled');
        }

        if (data != null) {
            for (var idx in data) {
                this.buildElement(data[idx]);
            }
        }
    }

    BiblioComponent.prototype.buildElement = function (elemData) {
        var Super = this;
        var elemCont = $('<div></div>').addClass('form-group');

        var citaLabel = $('<label></label>').addClass('col-sm-2 control-label').html('Cita');
        var citaCont = $('<div></div>').addClass('col-sm-4');
        var citaText = $('<input type="text" placeholder="Cita" />').addClass('form-control biblioCitation');

        var linkLabel = $('<label></label>').addClass('col-sm-1 control-label').html('Link');
        var linkCont = $('<div></div>').addClass('col-sm-3');
        var linkText = $('<input type="text" placeholder="Link" />').addClass('form-control biblioLink');

        var removeCont = $('<div></div>').addClass('col-sm-1');
        var removeBtn = $('<a></a>').addClass('btn btn-sm btn-x-danger').css('margin-top', '2px').html('Remover');

        if (typeof elemData != 'undefined' && elemData != null) {
            citaText.val(elemData.CITATION);
            linkText.val(elemData.LINK);
        }

        citaCont.append(citaText);
        linkCont.append(linkText);
        removeCont.append(removeBtn);
        elemCont.append(citaLabel);
        elemCont.append(citaCont);
        elemCont.append(linkLabel);
        elemCont.append(linkCont);
        elemCont.append(removeCont);

        this.elementsContainer.append(elemCont);

        if (this.settings.disabled) {
            citaText.prop('disabled', true);
            linkText.prop('disabled', true);
            removeBtn.addClass('disabled');
        }

        removeBtn.click(function () {
            Super.onRemoveClick($(this));
        });
    }

    BiblioComponent.prototype.getData = function () {
        var response = [];
        biblioComp.elementsContainer.find('.form-group').each(function () {
            var tmpElem = {
                CITATION: $(this).find('.biblioCitation').val(),
                LINK: $(this).find('.biblioLink').val()
            };
            response.push(tmpElem);
        });
        return response;
    }

    BiblioComponent.prototype.setDisabled = function (value) {
        if (value) {
            this.addBtn.addClass('disabled');
            this.elementsContainer.find('input').prop('disabled', true);
            this.elementsContainer.find('a').addClass('disabled');
            this.settings.disabled = true;
        } else {
            this.addBtn.removeClass('disabled');
            this.elementsContainer.find('input').prop('disabled', false);
            this.elementsContainer.find('a').removeClass('disabled');
            this.settings.disabled = false;
        }
    }

    BiblioComponent.prototype.onAddClick = function () {
        this.buildElement();
    }

    BiblioComponent.prototype.onRemoveClick = function (element) {
        element.parent().parent().remove();
    }

    BiblioComponent.prototype.setSettings = function (params) {
        this.settings = $.extend({
            title: 'Bibliografía',
            disabled: false
        }, params);
    }

    this.setSettings(params);
    this.init(parent, data);
};