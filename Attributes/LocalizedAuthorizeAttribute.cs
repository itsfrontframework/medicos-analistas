﻿using System;
using System.Web;
using System.Web.Mvc;

namespace base_front.Attributes {
    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    internal sealed class LocalizedAuthorizeAttribute : AuthorizeAttribute {

        private string defaultLanguage = "es-MX";

        public LocalizedAuthorizeAttribute() {
        }

        public LocalizedAuthorizeAttribute(string defaultLanguage) {
            this.defaultLanguage = defaultLanguage;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext) {
            string urlEncode = "";
            string language = "";

            language = (filterContext.RouteData.Values["lang"] == null ? defaultLanguage : filterContext.RouteData.Values["lang"].ToString());
            urlEncode = HttpUtility.UrlEncode(filterContext.HttpContext.Request.Url.PathAndQuery);

            filterContext.Result = new RedirectResult(string.Format("~/{0}/Account/Login?returnUrl={1}", language, urlEncode));
        }
    }
}
