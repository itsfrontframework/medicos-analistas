﻿using BackendCore.Authentication;
using BackendCore.Environment;
using BackendCore.Exceptions;
using BackendCore.Models;
using BackendCore.Utils;
using base_front.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using WSSDAL.Exceptions;
using WSSDAL.Utilities;

namespace base_front.Controllers.API {
    public class ScreenController : ApiController {
        /// <summary>
        /// Obtiene los datos requeridos para desplegar el menú.
        /// </summary>
        /// <remarks>GET api/Screen/Menu</remarks>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Menu")]
        public HttpResponseMessage Get() {
            DataFilter filter = new DataFilter();
            
            try {
                if (Request.Headers.Contains("UUID")) {
                    string uuid = Request.Headers.GetValues("UUID").FirstOrDefault();
                    User user = Sessions.getOwner(uuid);
                    filter.Add("USERNAME", user.USERNAME);
                    filter.Add("ENVIRONMENT", Configurations.getEnvironment());

                    return Request.CreateResponse(HttpStatusCode.OK, new MenuService().getMenu(filter));
                }
                else {
                    return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.Unauthorized, "UNAUTHORIZED_MENU");
                }
            }
            catch (DALException e) {
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.BadRequest, e);
            }
            catch (Exception e) {
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.NoContent, e);
            }
        }

        /// <summary>
        /// Obtiene la información para mostrar la pantalla solicitada
        /// </summary>
        /// <param name="data">Objeto con información de la pantalla a mostrar</param>
        /// <remarks>POST api/Screen/load</remarks>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Load")]
        public HttpResponseMessage Post([FromBody]FormDataCollection data) {
            bool isRoot = true;
            string url = string.Empty;
            string path = string.Empty;
            string screen = string.Empty;
            string sessionUUID = string.Empty;
            string[] elements;
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            //Se verifica que la petición traiga su session
            if (Request.Headers.Contains("UUID")) {

                //Se verifica que la sesión se encuentre abierta.
                sessionUUID = Request.Headers.GetValues("UUID").FirstOrDefault();



                //Se obtiene el nombre de la pantalla de la petición
                screen = data.Get("screen");
                if (data.Get("parameters") != null) {
                    parameters = JsonConvert.DeserializeObject<Dictionary<string,object>>(data.Get("parameters"));
                }
                //Se determina si la aplicación se encuentra en el root del servidor
                isRoot = Parameters.getValueOf("ROOT_DIRECTORY", isRoot);

                /*IMPLEMENTAR CÓDIGO*/

                //Si la solicitud trae el nombre de pantalla
                if (screen != null) {
                    path = "";
                    elements = Request.RequestUri.AbsoluteUri.Split('/');
                    for (int i = 0; i < (elements.Length - (isRoot ? 3 : 4)); i++) {
                        path += elements[i] + "/";
                    }

                    try {
                        //url = screenService.getScreenURL(screen);
                        url = Sessions.sessionScreenAccess(sessionUUID, screen);
                        if (url == null) {
                            throw new ServiceException(HttpStatusCode.NotFound, ServiceError.EMPTY_RESULT);
                        }
                    }
                    catch (Exception e) {
                        //return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.NotFound, "SCREEN_NOT_FOUND", e);
                        return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.Forbidden, "No tiene acceso a esta pantalla.", e);
                    }
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Found);
                    response.Headers.Location = new Uri(path + url);
                    return response;
                }
                else {
                    return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.BadRequest, new HttpError("SCREEN_BAD_REQUEST"));
                }
            }
            else {
                //En caso de no obtenerse una UUID de sesión, se regresa un mensaje de error.
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.Unauthorized, new HttpError("SCREEN_UNAUTHORIZED"));
            }
        }
    }
}
