﻿using BackendCore.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace base_front.Models
{
    /// <summary>
    /// Representa un módulo del sistema
    /// </summary>
    public class Module
    {
        /// <summary>	
        /// Código del módulo
        /// </summary>	
        [Required(ErrorMessage = "MODULE_FR_CODE")]
        [StringLength(maximumLength: 30, MinimumLength = 1, ErrorMessage = "MODULE_SL_CODE")]
        [RegularExpression(pattern: @"^[A-Z_]{3,30}$", ErrorMessage = "MODULE_RE_CODE")]
        public string CODE { get; set; }

        /// <summary>	
        /// Nombre de la pantalla, utilizado por las vistas para solicitar la pantalla
        /// </summary>	
        [Required(ErrorMessage = "MODULE_FR_NAME")]
        [StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = "MODULE_SL_NAME")]
        public string NAME { get; set; }

        /// <summary>	
        /// Descripción de la pantalla
        /// </summary>
        [Required(ErrorMessage = "MODULE_FR_DESCRIPTION")]
        [StringLength(maximumLength: 250, MinimumLength = 1, ErrorMessage = "MODULE_SL_DESCRIPTION")]
        public string DESCRIPTION { get; set; }

        /// <summary>	
        /// Propiedad que especifica si el elemento se mostrara en el MODULE
        /// </summary>
        [Required(ErrorMessage = "MODULE_FR_ENABLED")]
        [StringLength(maximumLength: 1, MinimumLength = 1, ErrorMessage = "MODULE_SL_ENABLED")]
        [EnumDataType(enumType: typeof(YesNo), ErrorMessage = "MODULE_ET_ENABLED")]
        public string ENABLED { get; set; }
    }
}