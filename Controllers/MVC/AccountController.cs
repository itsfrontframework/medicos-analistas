﻿using BackendCore.Authentication;
using BackendCore.Authentication.Services;
using BackendCore.Environment;
using BackendCore.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using WSSDAL.Utilities;

namespace base_front.Controllers.MVC {
    public class AccountController : Controller {

        /// <summary>
        /// Página de autenticación
        /// </summary>
        /// <remarks>GET: ~/Account/Login </remarks>
        /// <returns></returns>
        public ActionResult Login() {
            //GetTags("login"); //Se implementará en cuanto la internacionalización se encuentre correctamente implementada
            return View();
        }

        /// <summary>
        /// Página de autenticación
        /// </summary>
        /// <param name="guest">Objeto <see cref="User"/> con la información para realizar el login</param>
        /// <param name="returnUrl">Dirección de retorno para el caso de un login exitoso</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(User guest, string returnUrl) {
            if (ModelState.IsValid) {
                string sessionUUID = Sessions.LogIn(guest.USERNAME, guest.PASSWORD);
                if (string.IsNullOrWhiteSpace(sessionUUID)) {
                    //ModelState.AddModelError("", Texts.getText("UNAUTHORIZED_ACCESS"));
                    ModelState.AddModelError("", "UNAUTHORIZED_ACCESS");
                }
                else {
                    Session.Add("UUID", sessionUUID);
                    FormsAuthentication.SetAuthCookie(guest.USERNAME, true);
                    if (Url.IsLocalUrl(returnUrl) &&
                        returnUrl.Length > 1 &&
                        returnUrl.StartsWith("/") &&
                        !returnUrl.StartsWith("//") &&
                        !returnUrl.StartsWith("/\\")) {
                        return Redirect(returnUrl);
                    }
                    else {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            GetTags("login");
            return View(guest);
        }


        public ActionResult Logout() {
            if (Session["UUID"] != null) {
                Sessions.LogOut(Session["UUID"].ToString());
            }
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        private void GetTags(string screen) {
            DataFilter filter = new DataFilter() { { "MODULE", "SYSTEM" }, { "SCREEN", screen } };
            foreach (KeyValuePair<string, string> item in Texts.getTexts(filter)) {
                ViewData.Add(item.Key, item.Value);
            }
        }

    }
}
