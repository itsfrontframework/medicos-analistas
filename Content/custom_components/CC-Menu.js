var MenuItem = function () {
    /// <summary>Representa un elemento del men�.</summary>
    /// <field name='ROLE'>Referencia al rol que tiene acceso a la pantalla.</field>
    /// <field name='MODULE'>Referencia al m�dulo al que pertenece la pantalla.</field>
    /// <field name='SCREEN'>Referencia al control donde se colocar� el men�.</field>
    /// <field name='LABEL'>Referencia al control donde se colocar� el men�.</field>
    /// <field name='ICON'>Referencia al control donde se colocar� el men�.</field>
    /// <field name='POSITION'>Referencia al control donde se colocar� el men�.</field>
    /// <field name='SUB_ITEMS'>Referencia al control donde se colocar� el men�.</field>
    this.ROLE = "";
    this.MODULE = "";
    this.SCREEN = "";
    this.LABEL = "";
    this.ICON = "";
    this.POSITION = "";
    this.SUB_ITEMS = {};
};

var Menu = function (parent, params) {
    /// <summary>Men� din�mico generado a partir de una consulta a la base de datos para obtener el listado de elementos.</summary>
    /// <param name="parent" type="string">Nombre del div contenedor de la clase.</param>
    /// <param name="params" type="object">Los parametros que se obtienen para la clase.</param>
    /// <field name='parentControl'>Referencia al control donde se colocar� el men�.</field>
    /// <field name='menuElements'>Los elementos del men�.</field>
    this.menuElements = {};
    this.parentControl = $(parent);
    var Super = this;
    this.lastModule = "";

    Menu.prototype.setSettings = function (params) {
        /// <summary>Determina el valor de las configuraciones del componente.</summary>
        /// <param name="params" type="object">Los parametros que se obtienen para la clase men�.</param>
        this.settings = $.extend({
            url: "/api/Screen/Menu",
            headers: null,
            levels: 4,
            classes: ["", "nav nav-second-level masked", "nav nav-third-level masked", ""],
            logout: false,
            logoutUrl: ""
        }, params);
    }

    Menu.prototype.buildMenu = function () {
        /// <summary>Determina el valor de las configuraciones del componente.</summary>
        var htmlElement = "";//Contiene la lista

        $(Super.parentControl).html('');
        Super.buildListItem(Super.parentControl, Super.menuElements, 0);
        
        if (Super.settings.logout) {
            Super.buildLogout(Super.parentControl, Super.menuElements);
        }

        Super.onMenuBuilt();
        $(Super.parentControl).tooltip({ selector: "[data-toggle=tooltip]" });
    }

    Menu.prototype.getData = function () {
        /// <summary>Obtiene la informaci�n .</summary>
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onReady(JSON.parse(xmlhttp.responseText));
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                Super.onError(xmlhttp.getResponseHeader("ErrorMsg"));
            }
        }

        xmlhttp.open('GET', this.settings.url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        if (this.settings.headers !== null) {
            for (var property in this.settings.headers) {
                xmlhttp.setRequestHeader(property, this.settings.headers[property]);
            }
        }
        xmlhttp.send();
    }

    Menu.prototype.onReady = function (response) {
        for (var i = 0; i < response.length; i++) {
            //var obj = { label: response[i].LABEL, icon: response[i].ICON, screen: response[i].SCREEN, children: [] };
            //var order = response[i].POSITION;

            //Objeto del men�
            var objItem = new MenuItem();

            //Se asignan los valores del objeto
            objItem.ROLE = response[i].ROLE;
            objItem.MODULE = response[i].MODULE;
            objItem.SCREEN = response[i].SCREEN;
            objItem.LABEL = response[i].LABEL;
            objItem.ICON = response[i].ICON;
            objItem.POSITION = Super.cleanOrderField(response[i].POSITION);

            Super.insertItem(Super.menuElements, objItem, 0);
        }
        Super.buildMenu();
    }

    Menu.prototype.onError = function (response) {
        /// <param name="params" type="object">The radius of the circle.</param>
        /// <returns type="Number">The area.</returns>
    };

    Menu.prototype.cleanOrderField = function (order) {
        /// <summary>Quita cualquier caracter no alfa num�rico y procede a volver las letras may�sculas.</summary>
        /// <param name="order" type="string">Valor de orden a limpiar.</param>
        /// <return type="string">The area.</returns>
        if (order !== null && order !== undefined) {
            return order.toUpperCase().replace(/[^0-9A-Z]/gi, '');
        } else {
            return '000';
        }
    };

    Menu.prototype.obtainOrderValue = function (order, level) {
        /// <summary>Quita cualquier caracter no alfa num�rico y procede a volver las letras may�sculas.</summary>
        /// <param name="order" type="string">Valor de orden a limpiar.</param>
        /// <param name="level" type="int">Nivel actual del orden.</param>
        /// <return type="int">El valor del orden.</returns>
        var value = (order.charCodeAt(level) - ((order.charCodeAt(level) < 58 && order.charCodeAt(level) > 47) ? 48 : 55));
        return value - 1;
    };

    Menu.prototype.insertItem = function (tree, item, level) {
        /// <summary>Quita cualquier caracter no alfa num�rico y procede a volver las letras may�sculas.</summary>
        /// <param name="tree" type="MenuItem[]">Listado de elementos del men� para llenar.</param>
        /// <param name="item" type="MenuItem">Item a insertar.</param>
        /// <param name="level" type="int">Nivel actual a revisar.</param>
        var index = Super.obtainOrderValue(item.POSITION, level);
        var nextLvl = level + 1;

        //El valor del nivel no permite insertar si es 0
        if (index < 0) {
            return false;
        }

        //Si el elemento en el �ndice actual no posee nada, se genera un elemento vac�o
        if (tree[index] === null || tree[index] === undefined) {
            tree[index] = new MenuItem();
        }

        //Si el siguiente nivel posee valor distinto de 0 , se profundiza un nivel.
        if (nextLvl >= Super.settings.levels || (nextLvl < Super.settings.levels && !Super.insertItem(tree[index].SUB_ITEMS, item, nextLvl))) {
            tree[index].ROLE = item.ROLE;
            tree[index].MODULE = item.MODULE;
            tree[index].SCREEN = item.SCREEN;
            tree[index].LABEL = item.LABEL;
            tree[index].ICON = item.ICON;
            tree[index].POSITION = item.POSITION;
            return true;
        } else {
            return false;
        }
    };

    Menu.prototype.buildListItem = function (parent, tree, level) {
        /// <summary>Genera una lista de elementos con sus respectivos sub elementos</summary>
        /// <param name="parent" type="object">Objeto padre donde se agregar� el men�</param>        
        /// <param name="tree" type="MenuItem[]">Listado o arbol del men�</param>        
        /// <param name="level" type="int">Nivel actual en el que se despliega el men�</param>        
        var navClass = "";
        var nextLvl = level + 1;

        //Se determina la clase a colocar a los elementos del nivel indicado
        if (this.settings.classes[level] !== null || this.settings.classes[level] !== undefined) {
            navClass = this.settings.classes[level];
        }

        //Ciclo para recorrer los elementos del mismo nivel
        for (var i in tree) { 

            if (level == 0 && tree[i].MODULE != Super.lastModule) {
                if (Super.lastModule != "") {
                    parent.append($('<li></li>').addClass('divider'));
                }
                Super.lastModule = tree[i].MODULE;
            }

            var listItem = $('<li></li>').addClass(navClass);
            var linkItem = $('<a></a>');
            var iconItem = null;
            var labelItem = $('<span></span>').addClass("masked");

            //Referencia a la p�gina misma
            linkItem.attr("href", "#");

            if (tree[i] === undefined || tree[i] === null) {
                continue;
            }

            //En caso de traer nombre de pantalla se asigna a la funci�n de abrir la pantalla
            if (tree[i].SCREEN !== undefined && tree[i].SCREEN !== null) {
                linkItem.attr('onclick', '');
                linkItem.attr('id', tree[i].SCREEN);
                linkItem.click(function () {
                    Super.onItemClick(this.id);
                });
            }

            //En caso de tener un �cono, se agrega el elemento de �cono
            if (tree[i].ICON !== undefined && tree[i].ICON !== null) {
                iconItem = $('<i data-toggle="tooltip" data-placement="right" title="" data-original-title="' + tree[i].LABEL + '"></i>');
                iconItem.addClass('fa ' + tree[i].ICON + ' fa-fw');
                linkItem.append(iconItem);
            }

            //Se agrega el nombre del elemento
            labelItem.append("&nbsp;" + tree[i].LABEL);
            linkItem.append(labelItem);

            if (tree[i].SUB_ITEMS.length > 0 && nextLvl < this.settings.levels) {
                Super.buildListItem(linkItem, tree[i].SUB_ITEMS, nextLvl);
            }
            listItem.append(linkItem);
            parent.append(listItem);
        }
    };

    Menu.prototype.buildLogout = function (parent, tree) {
        parent.append($('<li></li>').addClass('divider'));

        var logoutLi = $('<li></li>');
        var logoutA = $('<a href="' + Super.settings.logoutUrl + '"></a>');
        var logoutIcon = $('<i></i>').addClass('fa fa-sign-out fa-fw');
        var logoutLabel = $('<span></span>').addClass('masked').html('&nbsp;Logout');

        logoutA.append(logoutIcon);
        logoutA.append(logoutLabel);
        logoutLi.append(logoutA);
        parent.append(logoutLi);
    }

    Menu.prototype.onItemClick = function (screen) {
        /// <summary>Permite realizar una funci�n con el nombre de la pantalla</summary>
        /// <param name="menuitem" type="object">Nombre de la pantalla</param>
        /// <param name="screen" type="MenuItem">Nombre de la pantalla</param>
    };

    Menu.prototype.onMenuBuilt = function () {
        /// <summary>Permite realizar una funci�n al concluir con la carga del men�</summary>
    }

    // Ejecutar 
    this.setSettings(params);
};
