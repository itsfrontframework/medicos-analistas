﻿/**
 * Auto AutoForm 
 *
 * @summary   Permite generar un formulario básico con botones de acción en el fondo.
 *
 * @since     0.0.1
 * @class
 * @classdesc Genera un formulario a partir de 2 array de elementos.
 * @param parent 
 * @param progressbar 
 * @param params 
 */


var AutoForm = function (parent, params) {
    this.container = parent;
    this.formFields = {};

    this.fieldContainer = $('<div></div>').addClass('form-group input-group');
    this.validationIconSpan = $('<span></span>').addClass('input-group-addon');
    this.nameSpan = $('<span></span>').addClass('input-group-addon').css('float', 'none').css('width', '250px').css('text-align', 'left');
    this.fieldTypeSpan = $('<span></span>').addClass('input-group-addon');
    this.icon = $('<i></i>').addClass('fa');
    this.icons = { 'DROPDOWN': 'fa-list', 'FILE': 'fa-file-excel-o', 'TEXT': 'fa-keyboard-o', 'DATE': 'fa-calendar', 'NUMBER': 'fa-slack' };

    AutoForm.prototype.setSettings = function (params) {
        this.settings = $.extend({
            data: null
        }, params);
    }

    AutoForm.prototype.init = function () {

        this.settings.data.sort(function (a, b) {
            return a.ORDER - b.ORDER;
        });

        var componentContainer = $('<form role="form"></form>');
        for (var idx in this.settings.data) {
            this.formFields[this.settings.data[idx].ID] = this.settings.data[idx];
            componentContainer.append(this.builField(this.settings.data[idx]));
        }
        $(this.container).append(componentContainer);
    }


    AutoForm.prototype.builField = function (fieldData) {
        var currElem = this.fieldContainer.clone();
        currElem.append(this.validationIconSpan.clone().append('&nbsp;&nbsp;&nbsp;'));
        currElem.append(this.nameSpan.clone().html(fieldData.NAME));
        currElem.append(this.buildInput(fieldData));
        currElem.append(this.fieldTypeSpan.clone().append(this.icon.clone().addClass(this.icons[fieldData.CONTROL_TYPE])));

        return currElem;
    }

    AutoForm.prototype.buildInput = function (fieldData) {
        var input;

        switch (fieldData.CONTROL_TYPE) {
            case 'TEXT':
                input = $('<input type="text" id="' + fieldData.ID + '" placeholder="' + fieldData.PLACEHOLDER + '"></input>').addClass('form-control');
                break;

            case 'NUMBER':
                input = $('<input type="number" id="' + fieldData.ID + '" placeholder="' + fieldData.PLACEHOLDER + '"></input>').addClass('form-control');
                break;

            case 'DROPDOWN':
                input = $('<select id="' + fieldData.ID + '"></select>').addClass('form-control');
                input.append('<option value=""> -- Seleccione -- </option>');
                for (var idx in fieldData.OPTIONS) {
                    input.append('<option value="' + idx + '">' + fieldData.OPTIONS[idx] + '</option>');
                }
                break;

            case 'FILE':
                input = $('<input type="text" id="' + fieldData.ID + '" placeholder="' + fieldData.PLACEHOLDER + '"></input>').addClass('form-control');
                break;

            case 'DATE':
                input = $('<input type="text" id="' + fieldData.ID + '" placeholder="' + fieldData.PLACEHOLDER + '" readonly="readonly" style="background-color: #ffffff !important"></input>').addClass('form-control');
                input.datepicker({
                    format: "yyyy/mm/dd",
                    language: "es",
                    orientation: "bottom auto",
                    autoclose: true,
                    todayHighlight: true
                });
                break;
        }

        return input;
    }

    AutoForm.prototype.getFormData = function () {
        var result = [];
        for (var idx in this.formFields) {
            var obj = {
                'ID': idx,
                'NAME': this.formFields[idx].NAME,
                'VALUE': $('#' + idx).val()
            };
            result.push(obj);
        }
        return result;
    }

    AutoForm.prototype.validate = function () {
        for (var idx in this.formFields) {
            //if (str.match(/^(\d)$/g)) {
            //    alert("match!");
            //}
        }
    }


    this.setSettings(params);
    this.init();
}