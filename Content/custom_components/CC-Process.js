﻿var Process = function (parent, params) {

    Process.prototype.init = function (parent) {
        $(parent).html('');
        this.buildContainer(parent, this.settings.data);
    }

    Process.prototype.buildContainer = function (parent, data) {
        var style = {
            DONE: { 'color': '#33653F', 'font-weight': 'bold' },
            CURRENT: { 'color': '#000000', 'font-weight': 'bold' },
            PENDING: { 'color': '#C2C2C2', 'font-weight': 'bold' }
        };
        var afterCurrent = false;
        var table = $('<table></table>').css('margin-left', '32px');
        var tdSpace = $('<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>');
        var styleSpan = $('<span></span>');
        var currentStyle;
        var currentLabel;

        for (var idx in data.STEP_LIST) {
            var elem = data.STEP_LIST[idx];
            var tr = $('<tr></tr>');

            if (elem.id == data.CURRENT_STEP) {
                afterCurrent = true;
                currentStyle = 'CURRENT';
                if (data.MARRIED == 'Y') {
                    currentLabel = (data.SECOND_OPINION ? 'En espera' : 'En proceso');
                } else {
                    currentLabel = 'Pendiente';
                }
            } else if (afterCurrent) {
                currentStyle = 'PENDING';
                currentLabel = 'Pendiente';
            } else {
                currentStyle = 'DONE';
                currentLabel = 'Terminado';
            }

            var tdTitle = $('<td></td>').html(styleSpan.clone().css(style[currentStyle]).html(elem.title));
            var tdStatus = $('<td></td>').html(styleSpan.clone().css(style[currentStyle]).html(currentLabel));

            tr.append(tdTitle);
            tr.append(tdSpace.clone());
            tr.append(tdStatus);
            table.append(tr);
        }

        $(parent).append(table);
    }

    Process.prototype.setSettings = function (params) {
        this.settings = $.extend({
            data: null,
            barCode: ""
        }, params);
    }

    this.setSettings(params);
    this.init(parent);

}

