﻿
var Search = function (params) {
    this.fields = params;

    Search.prototype.getFilter = function () {

        var responseFields = {};

        for (var propertyName in this.fields) {
            var propertyValue = $('#' + this.fields[propertyName]).val();
            if ((typeof propertyValue != 'undefined') && propertyValue != null && propertyValue != '') {
                responseFields[propertyName] = propertyValue;
            }
        }

        if (Object.keys(responseFields).length == 0) {
            responseFields = null;
        }

        return responseFields;
    }
}