﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace base_front.Models
{
    public class Screen
    {
        /// <summary>	
        /// Nombre del módulo asociado a esta pantalla
        /// </summary>	
        [Required(ErrorMessage = "MENU_FR_MODULE")]
        [StringLength(maximumLength: 30, MinimumLength = 1, ErrorMessage = "SCREEN_SL_MODULE")]
        [RegularExpression(pattern: @"^[A-Z_]{3,30}$", ErrorMessage = "SCREEN_RE_MODULE")]
        public string MODULE { get; set; }

        /// <summary>	
        /// Nombre de la pantalla, utilizado por las vistas para solicitar la pantalla
        /// </summary>	
        [StringLength(maximumLength: 60, ErrorMessage = "SCREEN_SL_CODE")]
        [RegularExpression(pattern: @"^[A-Za-z0-9_-]{3,60}$", ErrorMessage = "SCREEN_RE_CODE")]
        public string CODE { get; set; }

        /// <summary>
        /// Nombre de la pantalla
        /// </summary>
        [Required(ErrorMessage = "SCREEN_FR_NAME")]
        [StringLength(maximumLength: 50, MinimumLength = 1, ErrorMessage = "SCREEN_SL_NAME")]
        public string NAME { get; set; }
    }
}