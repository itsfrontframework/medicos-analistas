﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Home.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="LoginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class="row">
            <br />
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="text-center">
                    <img src="../../Content/images/logo.png" style="max-height: 200px; margin: auto;" class="img-responsive" id="logo" alt="<%=BackendCore.Environment.Parameters.getValueOf("APPLICATION_TITLE")%>">
                </h1>
                <%--<h2 class="text-center" style="color: coral;">SITIO QA</h2>--%>
            </div>
        </div>
        <div class="row" style="margin-top: 50px;">
            <div class="col-md-4 col-md-offset-4">
                <form id="authenticationForm" method="post" action="/Account/Login">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Usuario" id="USERNAME" name="USERNAME" type="text" autofocus required>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Contrase&ntilde;a" id="PASSWORD" name="PASSWORD" type="password" required>
                        </div>

                        <div class="text-right">
                            <input type="submit" class="btn btn-md btn-primary" value="Ingresar" />
                        </div>

                    </fieldset>
                </form>
                <%--<div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Iniciar Sesi&oacute;n</h3>
                    </div>
                    <div class="panel-body">
                        
                    </div>
                </div>--%>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 col-md-offset-6">
                <span style="color: #FFFFFF">V. 1701300700</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-danger alert-dismissable" id="errorButton" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Por favor verifique sus datos de ingreso.</strong>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/jquery/dist/jquery.min.js") %>'></script>
    <!-- Custom components -->
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-XHRservice.js") %>'></script>
    <!-- Page methods -->

</asp:Content>
