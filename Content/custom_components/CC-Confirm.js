﻿
var Confirm = function (params) {
    this.modalObj = null;
    this.modalTitle = null;
    this.modalBody = null;
    this.btnCancel = null;
    this.btnAccept = null;

    Confirm.prototype.setSettings = function (params) {
        this.settings = $.extend({
            confirm: false,
            title: "",
            body: "",
            acceptText: "Aceptar",
            cancelText: "Cancelar"
        }, params);
    }

    Confirm.prototype.setTitle = function (title) {
        this.settings.title = title;
    }
    Confirm.prototype.setBody = function (body) {
        this.settings.body = body;
    }

    Confirm.prototype.init = function () {

        var modalAttributes = 'id="confirm-modal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static"';
        this.modalObj = $('<div ' + modalAttributes + '></div>').addClass('modal fade');
        
        var modalDialog = $('<div></div>').addClass('modal-dialog');
        var modalContent = $('<div></div>').addClass('modal-content');
        var modalHeader = $('<div></div>').addClass('modal-header bg-primary');
        this.modalTitle = $('<h4></h4>').addClass('modal-title');
        var modalBodyContainer = $('<div></div>').addClass('modal-body');
        this.modalBody = $('<p></p>');

        var modalFooter = $('<div></div>').addClass('modal-footer');
        this.btnAccept = $('<button type="button"></button>').addClass('btn btn-primary');
        this.btnCancel = $('<button type="button"></button>').addClass('btn btn-default');

        modalFooter.append(this.btnAccept);
        modalFooter.append(this.btnCancel);
        modalBodyContainer.append(this.modalBody);
        modalHeader.append(this.modalTitle);
        modalContent.append(modalHeader);
        modalContent.append(modalBodyContainer);
        modalContent.append(modalFooter);
        modalDialog.append(modalContent);
        this.modalObj.append(modalDialog);

        $('body').append(this.modalObj);
    }

    Confirm.prototype.show = function () {
        var Super = this;

        setTimeout(function () {
        
            $(Super.modalTitle).html(Super.settings.title);
            $(Super.modalBody).html(Super.settings.body);
            $(Super.btnCancel).html(Super.settings.cancelText);
            $(Super.btnAccept).html(Super.settings.acceptText);

        
            if (Super.settings.confirm) {
                $(Super.btnCancel).off('click').click(function () {
                    Super.modalObj.modal('hide');
                    Super.onCancelClick();
                });
                $(Super.btnAccept).off('click').click(function () {
                    Super.modalObj.modal('hide');
                    Super.onAcceptClick();
                });

                $(Super.btnCancel).show();
                $(Super.btnAccept).show();
            } else {
                $(Super.btnCancel).off('click');
                $(Super.btnAccept).off('click').click(function () {
                    Super.modalObj.modal('hide');
                    Super.onAcceptClick();
                });

                $(Super.btnCancel).hide();
                $(Super.btnAccept).show();
            }

            Super.modalObj.modal('show');

        }, 500);
    }

    Confirm.prototype.onCancelClick = function () {
    }
    Confirm.prototype.onAcceptClick = function () {
    }

    this.setSettings(params);
    this.init();
}