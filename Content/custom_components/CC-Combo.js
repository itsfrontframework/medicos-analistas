﻿
var Combo = function (parent, selectedValue, params) {
    this.cmbSelect = null;
    this.complementaryData = {};

    Combo.prototype.setSettings = function (params) {
        this.settings = $.extend({
            url: "",
            defaultValue: false,
            defaultLabel: "Seleccione una opci&oacute;n",
            defaultText: "-1",
            parameters: null,
            labelFieldName: "TEXTO",
            valueFieldName: "VALOR",
            emptyValue: "-2",
            emptyLabel: "Sin resultados",
            disabled: false,
            jsonData: null
        }, params);
    }

    Combo.prototype.init = function (parent, selectedItem) {
        this.cmbSelect = $(parent);

        if (this.settings.disabled) {
            this.cmbSelect.attr('disabled', 'disabled');
        }

        if (this.settings.jsonData == null) {
            this.cmbSelect.append("<option> Cargando... </option>");
            this.getData(selectedItem);
        } else {
            this.onReady(this.settings.jsonData, selectedItem);
        }
    }

    Combo.prototype.onReady = function (response, selectedItem) {
        var Super = this;
        var jsonArray = JSON.parse(response);

        $(this.cmbSelect).empty();
        this.complementaryData = {};

        if (this.settings.defaultValue) {
            $(this.cmbSelect).append("<option value='" + this.settings.defaultText + "'>" + this.settings.defaultLabel + "</option>");
        }


        if (jsonArray.length > 0) {
            for (var idx in jsonArray) {
                var jsonObj = jsonArray[idx];
                var selectedOption = '';

                this.complementaryData[jsonObj[this.settings.valueFieldName]] = jsonObj;

                if ((typeof selectedItem != 'undefined') && selectedItem != null && jsonObj[this.settings.valueFieldName] == selectedItem) {
                    selectedOption = " selected='selected '";
                }

                var optionHtml = "<option value='" + jsonObj[this.settings.valueFieldName];
                optionHtml += "'" + selectedOption;
                optionHtml += ">";
                optionHtml += jsonObj[this.settings.labelFieldName];
                optionHtml += "</option>";

                $(this.cmbSelect).append(optionHtml);
            }
        } else {
            $(this.cmbSelect).append("<option value='" + this.settings.emptyValue + "'>" + this.settings.emptyLabel + "</option>");
        }
        

        $(this.cmbSelect).change(function () {
            Super.onChange();
        });

        Super.onLoadEnd();
    }

    Combo.prototype.getData = function (selectedItem) {

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onReady(xmlhttp.responseText, selectedItem);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                $(this.cmbSelect).empty();
                Super.onError(xmlhttp.getResponseHeader("ErrorMsg"));
            }
        }

        if (this.settings.parameters != null) {
            this.settings.url += "?" + jQuery.param(this.settings.parameters);
        }

        xmlhttp.open("GET", this.settings.url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        //xmlhttp.setRequestHeader("USUARIO", this.settings.username);
        xmlhttp.send();

    }

    Combo.prototype.setData = function (selectedOption) {
        $(this.cmbSelect).find('option').filter(function () {
            return ($(this).val() == selectedOption);
        }).prop('selected', true);
    }

    Combo.prototype.setDisabled = function (data) {
        if (data) {
            this.cmbSelect.attr('disabled', 'disabled');
        } else {
            this.cmbSelect.removeAttr('disabled');
        }
    }

    Combo.prototype.getSelectedComplement = function () {
        return this.complementaryData[this.cmbSelect.val()];
    }

    //Eventos editables
    Combo.prototype.onError = function (response) {
    };

    Combo.prototype.onLoadEnd = function (response) {
    };

    Combo.prototype.onChange = function () {
    };

    this.setSettings(params);
    this.init(parent, selectedValue);
};