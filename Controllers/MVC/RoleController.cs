﻿using BackendCore.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace base_front.Controllers.MVC {
    public class RoleController : Controller {
        /// <summary>
        /// Listado de roles disponibles en el sistema
        /// </summary>
        /// <returns></returns>
        [HandleError()]
        public PartialViewResult List() {
            Sessions.hasScreenAccess("roles-list", Request.Headers.GetValues("UUID").FirstOrDefault(), (Request.IsAjaxRequest() || ControllerContext.IsChildAction));
            return PartialView();
        }

        /// <summary>
        /// Permite agregar nuevos roles al sistema
        /// </summary>
        /// <returns></returns>
        [HandleError()]
        public PartialViewResult Add() {
            Sessions.hasScreenAccess("roles-add", Request.Headers.GetValues("UUID").FirstOrDefault(), (Request.IsAjaxRequest() || ControllerContext.IsChildAction));
            return PartialView();
        }

        /// <summary>
        /// Permite editar un rol del sistema
        /// </summary>
        /// <returns></returns>
        [HandleError()]
        public PartialViewResult Edit() {
            Sessions.hasScreenAccess("roles-edit", Request.Headers.GetValues("UUID").FirstOrDefault(), (Request.IsAjaxRequest() || ControllerContext.IsChildAction));
            return PartialView();
        }
    }
}
