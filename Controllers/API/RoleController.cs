﻿using BackendCore.Exceptions;
using BackendCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WSSDAL.Exceptions;
using BackendCore.Authentication.Services;
using WSSDAL.Utilities;

namespace base_front.Controllers.API {
    public class RoleController : ApiController {

        [HttpGet]
        [ActionName("List")]
        public HttpResponseMessage getList() {
            try {
                RoleService service = new RoleService();
                DataFilter filter = new DataFilter();
                return Request.CreateResponse(HttpStatusCode.OK, service.getRoles(filter));
            } catch (DALException e) {
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.BadRequest, e);
            } catch (Exception e) {
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.NoContent, e);
            }
        }

        [HttpPost]
        [ActionName("SaveRole")]
        public HttpResponseMessage saveRole([FromBody] Role role) {
            try {
                RoleService service = new RoleService();
                return Request.CreateResponse(HttpStatusCode.OK, service.updateRole(role));
            } catch (DALException e) {
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.BadRequest, e);
            } catch (Exception e) {
                return HttpMessage.CreateErrorResponse(Request, HttpStatusCode.NoContent, e);
            }
        }

    }
}
