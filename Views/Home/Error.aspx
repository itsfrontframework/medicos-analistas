﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Home.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h1><i class="fa fa-warning"></i>Error: <span id="error-box-title"><%=ViewData["ERROR_TITLE"] %></span></h1>
                </div>
                <div class="panel-body">
                    <h3><span id="error-box-message"><%=ViewData["ERROR_MESSAGE"] %></span></h3>
                </div>

                <br />
                <br />
                <div class="panel-footer text-center">
                    <h2>
                        <a onclick="window.location.href='<%= ResolveUrl("~/") %>';">Regresar al sistema</a>
                    </h2>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
