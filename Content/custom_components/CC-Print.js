﻿/**
 * Print 
 *
 * @summary   Permite imprimir el contenido del contenido principal.
 *
 * @since     0.0.1
 * @class
 * @classdesc Permite imprimir el contenido del contenido principal.
 * @param params 
 */

var PrintContent = function (params) {
    this.titleContainer = null;
    this.idControl = Math.ceil(Math.random() * 100000000);
    var label = '#' + this.idControl;
    var Super = this;

    PrintContent.prototype.init = function () {
        //TODO:Agregar acciones en init
    }

    PrintContent.prototype.onReady = function (response) {
        //TODO:Agregar acciones en onReady
    }


    PrintContent.prototype.onError = function (response) {
        //TODO:Agregar acciones en onError
    }



    PrintContent.prototype.postData = function (url, postObject, single) {
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onReady(xmlhttp.responseText);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                Super.onError(xmlhttp.status);
            }
        }
        xmlhttp.open("POST", url + "?" + Math.ceil(Math.random() * 100000000));
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.setRequestHeader("SessionID", this.settings.sessionid);

        //
        if (single !== undefined) {
            xmlhttp.setRequestHeader("Service", this.settings.serviceName + "SINGLE");
        } else {
            xmlhttp.setRequestHeader("Service", this.settings.serviceName);
        }

        if (postObject !== undefined) {
            xmlhttp.send(JSON.stringify(postObject));
        } else {
            xmlhttp.send("{}");
        }
    }

    PrintContent.prototype.setSettings = function (params) {
        this.settings = $.extend({
            sessionid: "",
            serviceName:""
        }, params);
    }

    this.setSettings(params);
    this.init();
};