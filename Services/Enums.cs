﻿using BackendCore.Utils;

namespace base_front.Services
{
    /// <summary>
    /// Una enumeración con valor de ejemplo. Sirve de base y ejemplo...
    /// </summary>
    public enum ExampleEnum
    {
        /// <summary>
        /// Valor default.
        /// </summary>
        [StringValue("Valor ejemplo.")]
        EXAMPLE = 0
    }
}