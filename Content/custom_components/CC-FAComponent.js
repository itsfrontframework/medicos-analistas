﻿
var FAComponent = function (parent, selectedValues, params) {
    this.componentContainer = null;
    this.tableBody = null;
    this.itemIndex = 0;
    this.itemCount = 0;
    this.selectedGenotypes = {};
    this.genotypeList = ['AA', 'AC', 'AT', 'AG', 'TT', 'TA', 'TC', 'TG', 'CC', 'CA', 'CT', 'CG', 'GG', 'GA', 'GT', 'GC', 'G', 'A', 'C', 'T'];
    this.genotypeControlList = [];
    this.baseActionSelect = null;
    this.baseCriteria = null;

    FAComponent.prototype.init = function (parent, selectedValues) {
        var Super = this;

        for (var i = 0; i < this.genotypeList.length; i++) {
            this.genotypeControlList.push(this.genotypeList[i]);
        }

        this.baseActionSelect = $('<select></select>').addClass('form-control');
        this.baseActionSelect.append('<option value="agonist">Agonista</option>');
        this.baseActionSelect.append('<option value="agonist-partial">Agonista parcial</option>');
        this.baseActionSelect.append('<option value="agonist-antagonist">Agonista / Antagonista</option>');
        this.baseActionSelect.append('<option value="antagonist-partial">Antagonista parcial</option>');
        this.baseActionSelect.append('<option value="antagonist">Antagonista</option>');

        this.baseCriteria = $('<select></select>').addClass('form-control').css('width', '100%');

        this.componentContainer = $(parent);
        var columnDiv = $('<div></div>').addClass('col-sm-10 col-sm-offset-1').css('padding-right', '4px');
        var panelDiv = $('<div></div>').addClass('panel panel-default');
        var panelBodyDiv = $('<div></div>').addClass('panel-body');
        var addBtnContainer = $('<div></div>').addClass('text-right');
        var btnAdd = $('<a></a>').addClass('btn btn-success btn-sm').html('Agregar genotipo');
        var tableDiv = $('<div></div>').addClass('table-responsive');

        var table = $('<table></table>').addClass('table table-hover');
        table.append($('<thead></thead>').append($('<tr></tr>').
            append('<th>Genotipo</th>').
            append('<th>Acción Farmacológica</th>').
            append('<th>Criterio</th>').
            append('<th style="width: 70px;"></th>')
        ));
        this.tableBody = $('<tbody></tbody>');
        table.append(this.tableBody);

        tableDiv.append(table);
        addBtnContainer.append(btnAdd);
        panelBodyDiv.append(addBtnContainer);
        panelBodyDiv.append(tableDiv);
        panelDiv.append(panelBodyDiv);
        columnDiv.append(panelDiv);
        this.componentContainer.append(columnDiv);

        btnAdd.click(function () {
            Super.onAddRowClick();
        });

        // Load values
        if (selectedValues == null) {
            this.addDefaultRow(null, null);
        } else {
            var tmpActionType;
            var tmpCriteria;

            // Verify values for default row
            var isDefault = false;
            if (selectedValues.length == this.genotypeList.length) {
                isDefault = true;
                tmpActionType = selectedValues[0].ACTION_TYPE;
                tmpCriteria = selectedValues[0].CRITERIA;

                for (var kdx = 1; kdx < selectedValues.length; kdx++) {
                    if(selectedValues[kdx].ACTION_TYPE != tmpActionType || selectedValues[kdx].CRITERIA != tmpCriteria) {
                        isDefault = false;
                        break;
                    }
                }
            }

            if (isDefault) {
                this.addDefaultRow(tmpActionType, tmpCriteria);
            } else {
                for (var idx in selectedValues) {
                    var elem = selectedValues[idx];
                    this.addRow(elem.GENOTYPE, elem.ACTION_TYPE, elem.CRITERIA);
                }
            }
            
        }
        
    }

    FAComponent.prototype.addDefaultRow = function (selectedAction, selectedCriteria) {
        var tr = $('<tr id="faTableRow_default"></tr>');
        var td = $('<td></td>');

        tr.append(td.clone().append('<label style="padding-top: 5px; padding-left: 5px">Todos</label>'));
        tr.append(td.clone().append(this.baseActionSelect.clone().attr('id', 'selAction_default')));
        tr.append(td.clone().append(this.baseCriteria.clone().attr('id', 'selCriteria_default')));
        tr.append(td.clone().append(''));
        this.tableBody.append(tr);

        // Set values
        if (selectedAction != null) {
            $('#selAction_default').val(selectedAction);
        }
        var criteriaValue = null;
        if (selectedCriteria != null) {
            criteriaValue = [];
            criteriaValue.push("" + selectedCriteria);
        }

        var criteriaColors = { '1': '#63BE7B', '2': '#B5D77B', '3': '#FFEB84', '4': '#FFAA7B', '5': '#FF696B' };
        var selCriteria = new MultipleCombo('#selCriteria_default', criteriaValue, {
            labelFieldName: "text",
            valueFieldName: "id",
            placeholder: '-- Elija uno --',
            multiple: false,
            jsonData: '[{"id":"1","text":"1"}, {"id":"2","text":"2"}, {"id":"3","text":"3"}, {"id":"4","text":"4"}, {"id":"5","text":"5"}]',
            templateResult: function (item) {
                var colorSpan = $('<span></span>').html('&nbsp;&nbsp;&nbsp;&nbsp;').
                        css('margin-right', '10px').
                        css('background-color', criteriaColors[item.id]);
                var element = $('<span></span>').append(colorSpan).append(item.text);
                return element;
            },
            hideSearchBox: true
        });
    }

    FAComponent.prototype.onAddRowClick = function () {
        this.addRow(null, null, null);
    }

    FAComponent.prototype.addRow = function (selectedGenotype, selectedAction, selectedCriteria) {
        if (this.genotypeControlList.length == 0) {
            return false;
        }

        $('#faTableRow_default').remove();

        var Super = this;
        var tr = $('<tr id="faTableRow_' + this.itemIndex + '"></tr>');
        var td = $('<td></td>');
        var btnEliminar = $('<a data-btnDeleteFaElement="' + this.itemIndex + '"></a>').addClass('btn btn-danger btn-sm').html('Eliminar');

        var tmpGenotypeSel = this.buildGenotypeSelect('selGenotype_' + this.itemIndex);
        tr.append(td.clone().append(tmpGenotypeSel));
        tr.append(td.clone().append(this.baseActionSelect.clone().attr('id', 'selAction_' + this.itemIndex)));
        tr.append(td.clone().append(this.baseCriteria.clone().attr('id', 'selCriteria_' + this.itemIndex)));
        tr.append(td.clone().append(btnEliminar));
        this.tableBody.append(tr);

        // Set values
        if (selectedGenotype != null) {
            tmpGenotypeSel.val(selectedGenotype);
        }
        if (selectedAction != null) {
            $('#selAction_' + this.itemIndex).val(selectedAction);
        }
        var criteriaValue = null;
        if (selectedCriteria != null) {
            criteriaValue = [];
            criteriaValue.push("" + selectedCriteria);
        }

        this.refreshGenotypeValues(this.itemIndex, tmpGenotypeSel.val(), true);
        btnEliminar.click(function () {
            Super.onDeleteRowClick($(this).attr('data-btnDeleteFaElement'));
        });

        var criteriaColors = { '1': '#63BE7B', '2': '#B5D77B', '3': '#FFEB84', '4': '#FFAA7B', '5': '#FF696B' };
        var selCriteria = new MultipleCombo('#selCriteria_' + this.itemIndex, criteriaValue, {
            labelFieldName: "text",
            valueFieldName: "id",
            placeholder: '-- Elija uno --',
            multiple: false,
            jsonData: '[{"id":"1","text":"1"}, {"id":"2","text":"2"}, {"id":"3","text":"3"}, {"id":"4","text":"4"}, {"id":"5","text":"5"}]',
            templateResult: function (item) {
                var colorSpan = $('<span></span>').html('&nbsp;&nbsp;&nbsp;&nbsp;').
                        css('margin-right', '10px').
                        css('background-color', criteriaColors[item.id]);
                var element = $('<span></span>').append(colorSpan).append(item.text);
                return element;
            },
            hideSearchBox: true
        });

        this.itemIndex++;
        this.itemCount++;
    }

    FAComponent.prototype.refreshGenotypeValues = function (index, value, asign) {
        if (asign) {
            // Add to persistent object
            this.selectedGenotypes[index] = value;

            // Remove from available list
            var tmpIndex = this.genotypeControlList.indexOf(value);
            if (tmpIndex > -1) {
                this.genotypeControlList.splice(tmpIndex, 1);
            }

            // Remove from other selects
            for (var i = 0; i < this.itemIndex; i++) {
                if (i != index) {
                    $('#selGenotype_' + i + ' option[value="' + value + '"]').remove();
                }
            }
        } else {
            // Remove value from persistent object
            this.selectedGenotypes[index] = '';

            // Add to available list
            this.genotypeControlList.push(value);

            // Add to other selects
            for (var i = 0; i < this.itemIndex; i++) {
                if (i != index) {
                    $('#selGenotype_' + i).append('<option value="' + value + '">' + value + '</option');
                }
            }
        }
    }

    FAComponent.prototype.onDeleteRowClick = function (elementID) {
        var genotypeValue = $('#selGenotype_' + elementID).val();
        this.refreshGenotypeValues(elementID, genotypeValue, false);
        $('#faTableRow_' + elementID).remove();
        this.itemCount--;

        if (this.itemCount == 0) {
            this.addDefaultRow(null, null);
        }
    }

    FAComponent.prototype.onChangeGenotypeValue = function (index) {
        var oldValue = this.selectedGenotypes[index];
        var newValue = $('#selGenotype_' + index).val();

        this.refreshGenotypeValues(index, oldValue, false);
        this.refreshGenotypeValues(index, newValue, true);
    }

    FAComponent.prototype.buildGenotypeSelect = function (elementID) {
        var Super = this;
        var baseGenotypeSelect = $('<select id="' + elementID + '"></select>').addClass('form-control');
        for (var idx in this.genotypeControlList) {
            baseGenotypeSelect.append('<option value="' + this.genotypeControlList[idx] + '">' + this.genotypeControlList[idx] + '</option>');
        }

        baseGenotypeSelect.change(function () {
            Super.onChangeGenotypeValue(elementID.substring(12));
        });

        return baseGenotypeSelect;
    }

    FAComponent.prototype.getData = function () {
        var data = [];
        
        if (this.itemCount > 0) {
            for (var idx = 0; idx < this.itemIndex; idx++) {
                if ($('#faTableRow_' + idx).length > 0) {
                    var tmpObj = {
                        GENOTYPE: $('#selGenotype_' + idx).val(),
                        ACTION_TYPE: $('#selAction_' + idx).val(),
                        CRITERIA: $('#selCriteria_' + idx).val()
                    };
                    data.push(tmpObj);
                }
            }
        } else {
            for (var idx in this.genotypeList) {
                var tmpObj = {
                    GENOTYPE: this.genotypeList[idx],
                    ACTION_TYPE: $('#selAction_default').val(),
                    CRITERIA: $('#selCriteria_default').val()
                };
                data.push(tmpObj);
            }
        }

        return data;
    }

    FAComponent.prototype.setSettings = function (params) {
        this.settings = $.extend({
            test: false
        }, params);
    }

    this.setSettings(params);
    this.init(parent, selectedValues);
};