﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar Rol</h1>
    </div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-file-text-o"></i>&nbsp;Rol
            </div>

            <form class="form-horizontal">

                <div class="panel-body">

                    <input type="hidden" id="txtID" />
                    <div class="form-group">
                        <label for="txtNombre" class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txtNombre" placeholder="Nombre" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="txtDescripcion" class="col-sm-2 control-label">Descripción</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="txtDescripcion" placeholder="Descripción" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="selHabilitado" class="col-sm-2 control-label">Habilitado</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="selHabilitado">
                                <option value="Y">Si</option>
                                <option value="N">No</option>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-11 text-right">
                            <span id="btnCancelar"></span>
                            <span id="btnGuardar"></span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    var saveService = new XHRservice({
        url: '/api/Role/SaveRole',
        method: 'POST',
        responseType: 'JSON'
    });

    var btnGuardar = new Button('#btnGuardar', {
        icon: "fa-save",
        label: "Guardar",
        tooltip: "Guardar",
        defaultStyle: "btn-primary",
    });
    var btnCancelar = new Button('#btnCancelar', {
        icon: "fa-times",
        label: "Cancelar",
        tooltip: "Cancelar",
        defaultStyle: "btn-danger",
    });

    btnGuardar.onClick = function () {
        var data = {};
        data['ROLE_ID'] = $('#txtID').val();
        data['NAME'] = $('#txtNombre').val();
        data['DESCRIPTION'] = $('#txtDescripcion').val();
        data['ENABLED'] = $('#selHabilitado').val();

        saveService.send(data);
        screen.load('roles-list', null);
    }
    btnCancelar.onClick = function () {
        screen.load('roles-list', null);
    }

    function passParameters(data) {
        $('#txtID').val(data.ROLE_ID);
        $('#txtNombre').val(data.NAME);
        $('#txtDescripcion').val(data.DESCRIPTION);
        $('#selHabilitado').val(data.ENABLED);
    }



</script>
