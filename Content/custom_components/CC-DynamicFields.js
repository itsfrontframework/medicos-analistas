﻿
var DynamicFields = function (params) {
    this.modalElement = null;

    DynamicFields.prototype.init = function () {
        var Super = this;
        var modalAttributes = 'id="dynamicFieldsModal" tabindex="-1" role="dialog" aria-labelledby="dynamicFieldsModalLabel" aria-hidden="true"';
        this.modalElement = $('<div ' + modalAttributes + '></div>').addClass('modal fade');
        var modalDialog = $('<div></div>').addClass('modal-dialog');
        var modalContent = $('<div></div>').addClass('modal-content');
        var modalHeader = $('<div></div>').addClass('modal-header');
        var modalBody = $('<div></div>').addClass('modal-body');
        var modalFooter = $('<div></div>').addClass('modal-footer');
        var modalTitle = $('<h4 id="dynamicFieldsModalLabel"></h4>').addClass('modal-title').html(this.settings.title);
        var modalCloseBtn = $('<button type="button" data-dismiss="modal"></button>').addClass('btn btn-default').html(this.settings.cancelBtnLabel);
        var modalAcceptBtn = $('<button type="button"></button>').addClass('btn btn-primary').html(this.settings.acceptBtnLabel);

        modalBody.append(this.buildForm());

        modalHeader.append(modalTitle);
        modalFooter.append(modalCloseBtn);
        modalFooter.append(modalAcceptBtn);
        modalContent.append(modalHeader);
        modalContent.append(modalBody);
        modalContent.append(modalFooter);
        modalDialog.append(modalContent);
        this.modalElement.append(modalDialog);

        $('body').append(this.modalElement);
        if (this.settings.obtainURL != '') {
            this.obtainFieldsData();
        }

        modalAcceptBtn.click(function () {
            Super.onAcceptClick();
        });
        modalCloseBtn.click(function () {
            Super.onCancelClick();
        });
    }

    DynamicFields.prototype.onAcceptClick = function () {
        var Super = this;
        this.modalElement.modal('hide');
        this.sendResults();

        setTimeout(function () {
            Super.modalElement = null;
            $('#dynamicFieldsModal').remove();
            Super.settings.afterAction.apply(Super, Super.settings.afterActionParams);
        }, 500);
    }
    DynamicFields.prototype.onCancelClick = function() {
        var Super = this;
        this.modalElement.modal('hide');
        setTimeout(function () {
            Super.modalElement = null;
            $('#dynamicFieldsModal').remove();
        }, 500);
    }

    DynamicFields.prototype.show = function () {
        this.modalElement.modal('show');
    }

    DynamicFields.prototype.buildForm = function () {
        var formContainer = $('<form role="form"></form>');
        var formGroup = $('<div></div>').addClass('form-group');
        var fieldLabel = $('<label></label>');
        var textArea = $('<textarea rows="3"></textarea>').addClass('form-control');

        if (this.settings.fields == null) return '';

        for (var idx in this.settings.fields) {
            var tmp = formGroup.clone();
            if (this.settings.fields[idx].trim() != '') {
                tmp.append(fieldLabel.clone().html(this.settings.fields[idx]));
            }
            tmp.append(textArea.clone().attr('id', 'fld_' + idx));
            formContainer.append(tmp);
        }

        return formContainer;
    }

    DynamicFields.prototype.sendResults = function () {
        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // Successs
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                alert("En este momento no es posible conectarse a la base de datos");
            }
        }

        var data = {};
        for (var idx in this.settings.fields) {
            data[idx] = $('#fld_' + idx).val();
        }
        for (var idx in this.settings.aditionalData) {
            data[idx] = this.settings.aditionalData[idx];
        }

        xmlhttp.open("POST", this.settings.saveURL, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send(JSON.stringify(data));
    }

    DynamicFields.prototype.obtainFieldsData = function () {
        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.setFieldsData(xmlhttp.responseText);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                // No existe un registro previo para este expediente
            }
        }
        
        var requestUrl = this.settings.obtainURL + '?' + jQuery.param(this.settings.aditionalData);
        xmlhttp.open("GET", requestUrl, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send();
    }

    DynamicFields.prototype.setFieldsData = function (data) {
        var json = JSON.parse(data);
        if (json != null) {
            for (var idx in json) {
                if (typeof this.settings.fields[idx] != 'undefined') {
                    $('#fld_' + idx).val(json[idx]);
                }
            }
        }
    }

    DynamicFields.prototype.setSettings = function (params) {
        this.settings = $.extend({
            title: "Title",
            cancelBtnLabel: "Cancelar",
            acceptBtnLabel: "Guardar",
            afterAction: null,
            afterActionParams: null,
            fields: null,
            saveURL: null,
            obtainURL: null,
            aditionalData: null
        }, params);
    }


    this.setSettings(params);
    this.init();
}