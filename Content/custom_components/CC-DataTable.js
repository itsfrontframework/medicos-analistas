﻿
var DataTable = function (parent, params) {
    this.container = null;
    this.table = null;
    this.api = null;
    this.tableID = 'table_' + Math.ceil(Math.random() * 100000000);
    this.tableBody = null;
    this.data = null;
    this.hasColspan = false;
    this.realColumnCount = 0;

    DataTable.prototype.init = function (parent) {
        this.container = $(parent);

        this.container.addClass('dataTable_wrapper');
        this.table = $('<table id="' + this.tableID + '"></table>').addClass('table table-striped table-bordered table-hover');
        var thead = $('<thead></thead>');
        var theadRow = $('<tr></tr>');

        for (var idx in this.settings.columns) {
            var col = this.settings.columns[idx];

            // class attributes are for Specific modification
            if (typeof col.colspan != 'undefined' && col.colspan) {
                theadRow.append('<th colspan="' + col.columns.length + '" class="spanOrder_0 sorting">' + col.title + '</th>');
                this.hasColspan = true;
                this.realColumnCount += col.columns.length;
            } else {
                theadRow.append('<th class="noSpanColumn">' + col.title + '</th>');
                this.realColumnCount++;
            }
        }

        // Specific modification ===================================
        theadRow.append('<th>Order</th>');
        this.realColumnCount++;
        // Specific modification ===================================

        this.tableBody = $('<tbody></tbody>');

        if (this.hasColspan) {
            var spanDummyRow = $('<tr class="span-dummy-column"></tr>');
            for (var i = 0 ; i < this.realColumnCount; i++) {
                spanDummyRow.append('<th></th>');
            }
            thead.append(spanDummyRow);
        }

        thead.append(theadRow);
        this.table.append(thead);
        this.table.append(this.tableBody);
        this.container.append(this.table);
    }

    DataTable.prototype.loadTable = function (response) {
        var Super = this;
        this.data = response;
        for (var idx in response) {
            var respItem = response[idx];
            var tmpRow = $('<tr data-id="' + idx + '"></tr>');

            for (var j in this.settings.columns) {
                var currColumn = this.settings.columns[j];

                if (typeof currColumn.colspan != 'undefined' && currColumn.colspan) {
                    for (var k in currColumn.columns) {
                        var tmpCell;
                        if (typeof currColumn.columns[k].titleAsCellValue != 'undefined' && currColumn.columns[k].titleAsCellValue) {
                            tmpCell = $('<td>' + currColumn.columns[k].title + '</td>');
                        } else {
                            tmpCell = $('<td>' + respItem[currColumn.columns[k].name] + '</td>');
                        }

                        tmpCell.addClass(this.getConditionsStyle(currColumn.columns[k], respItem));
                        tmpRow.append(tmpCell);
                    }
                } else {
                    var tmpCell;
                    if (typeof currColumn.titleAsCellValue != 'undefined' && currColumn.titleAsCellValue) {
                        tmpCell = $('<td>' + currColumn.title + '</td>');
                    } else {
                        tmpCell = $('<td>' + respItem[currColumn.name] + '</td>');
                    }

                    tmpCell.addClass(this.getConditionsStyle(currColumn, respItem));
                    tmpRow.append(tmpCell);
                }

            }

            // Specific modification ===================================
            var orderValue = respItem['SNP_MODULE_FIN'] + respItem['INTER_MODULE_FIN'] + respItem['SIGN_MODULE_FIN'];
            tmpRow.append($('<td>' + orderValue + '</td>'));
            // Specific modification ===================================

            this.tableBody.append(tmpRow);
        }

        this.api = this.table.dataTable({
            responsive: true,
            bFilter: false,
            bLengthChange: false,
            pageLength: this.settings.pageLength,
            oLanguage: this.getLanguajeObject(),
            order: [[this.settings.corderColumn, "desc"]],
            columnDefs: [{ targets: [10], visible: false }] // Specific modification
        });

        // Specific modification ===================================
        this.table.on('click', '.spanOrder_0', function () {
            if ($(this).hasClass('sorting')) {
                $(this).removeClass('sorting');
                $(this).addClass('sorting_asc');
                Super.api.fnSort([[10, 'asc']]);
            } else if ($(this).hasClass('sorting_asc')) {
                $(this).removeClass('sorting_asc');
                $(this).addClass('sorting_desc');
                Super.api.fnSort([[10, 'desc']]);
            } else if ($(this).hasClass('sorting_desc')) {
                $(this).removeClass('sorting_desc');
                $(this).addClass('sorting_asc');
                Super.api.fnSort([[10, 'asc']]);
            }
        });
        this.table.on('click', '.noSpanColumn', function () {
            $('.spanOrder_0').removeClass('sorting_asc sorting_desc sorting');
            $('.spanOrder_0').addClass('sorting');
        });
        // Specific modification ===================================

        this.table.find('.span-dummy-column').hide();

        this.table.on('click', 'tr', function () {
            if ($(this).parent().is("thead")) {
                return false;
            }
            Super.onRowClick(Super.data[$(this).data('id')]);
        });

    }

    DataTable.prototype.getConditionsStyle = function (columnDefinition, dataRow) {
        var style = '';

        if (typeof columnDefinition.conditions != 'undefined' && columnDefinition.conditions != null) {
            var conditions = columnDefinition.conditions;
            var value = dataRow[columnDefinition.name];

            for (var idx in conditions) {
                if (conditions[idx].expression !== null && conditions[idx].expression !== undefined) {
                    var completeExpression = conditions[idx].expression.replace(/value/gi, value);
                    if (eval(completeExpression) === true) {
                        if (conditions[idx].style !== null && conditions[idx].style !== undefined) {
                            style = style + ' ' + conditions[idx].style;
                        }
                    }
                }
            }
        }

        return style;
    }

    DataTable.prototype.onRowClick = function (data) {
    }

    DataTable.prototype.getLanguajeObject = function () {
        var obj = {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ning&uacute;n dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": '<i class="fa fa-angle.double-left"></i>',
                "sLast": '<i class="fa fa-angle-double-right"></i>',
                "sNext": '<i class="fa fa-angle-right"></i>',
                "sPrevious": '<i class="fa fa-angle-left"></i>'
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        };
        return obj;
    }

    DataTable.prototype.setSettings = function (params) {
        this.settings = $.extend({
            columns: null,
            pageLength: 10,
            corderColumn: 0,
            ctypeorder: "asc"
        }, params);
    }

    this.setSettings(params);
    this.init(parent);
}