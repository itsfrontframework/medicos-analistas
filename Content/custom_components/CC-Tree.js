﻿
/*
** parent: ID del contenedor del arbol. El contenedor debe ser <div class="aciTree aciTreeBig" id="tree" role="tree"></div>
** params: Contiene la url que se utilizará para el lazy loading
** data: objeto json con los datos del arbol, en caso de no proporcionarse se utilizara la url
*/

var Tree = function (parent, params, data) {
    this.treeObj = null;
    this.selectedItem = null;

    Tree.prototype.setSettings = function (params) {
        this.settings = $.extend({
            apiPrefix: "~/api/",
            url: "Jerarquia/Display",
            currentuser: "",
            sortable: true
        }, params);
    };

    Tree.prototype.init = function (parent, data) {
        var Super = this;
        var options = {
            esize: true,
            ajax: {
                url: this.settings.apiPrefix + this.settings.url,
                headers: { 'USUARIO': this.settings.currentuser }
            }
        };

        if ((typeof data != 'undefined') && (data != null)) {
            options.rootData = data;
        }
        options.selectable = true;
        options.sortable = this.settings.sortable;

        $(parent).aciTree(options);
        this.treeObj = $(parent).aciTree('api');

        $(parent).on('click', '.aciTreeItem', function (e) {
            $(parent).removeClass('selected').find('.selected').removeClass('selected');
            $(this).addClass('selected');
            Super.selectedItem = this;
            Super.onItemSelect(Super.getSelectedItemID());
            e.stopPropagation();
        });
    };

    Tree.prototype.getSelectedItem = function () {
        if (this.selectedItem) {
            var item = this.treeObj.itemFrom(this.selectedItem);
            if (this.treeObj.isItem(item)) {
                return item;
            }
        }
        return null;
    };

    Tree.prototype.getSelectedItemID = function () {
        if (this.selectedItem) {
            var item = this.treeObj.itemFrom(this.selectedItem);
            if (this.treeObj.isItem(item)) {
                return this.treeObj.getId(item);
            }
        }
        return null;
    };

    Tree.prototype.onItemSelect = function (itemId) { };

    // regresa la estructura actual del componente
    Tree.prototype.setData = function (parent, data) {
        var Super = this;
        this.init(parent, data);
    };

    // regresa la estructura actual del componente
    Tree.prototype.getData = function () {
        return this.treeObj.serialize(null, {});
    };

    // Limpia los contenidos del arbol
    Tree.prototype.clean = function () {
        var Super = this;
        this.treeObj.destroy({
            success: function () {
                this.selectedItem = null;
                Super.destroyed();
            },
            fail: function () {
                Super.destroyfail();
            }
        });
    };

    // Recibe el parentItem, que se puede obtener con el metodo search y los datos del elemento a insertar como un objeto
    Tree.prototype.addElement = function (parentItem, childData) {
        var Super = this;
        this.treeObj.addIcon(parentItem, { icon: 'folder' });
        this.treeObj.setInode(parentItem, {});
        this.treeObj.append(parentItem, {
            uid: 'user-defined',
            success: function () {
                Super.addOnReady();
            },
            fail: function () {
                Super.addOnError();
            },
            itemData: childData
        });
    };
    Tree.prototype.addOnReady = function () {
    };
    Tree.prototype.addOnError = function () {
    };

    Tree.prototype.removeElement = function (item) {
        var Super = this;
        this.treeObj.remove(item, {
            uid: 'user-defined',
            success: function () {
                Super.removeOnReady();
            },
            fail: function () {
                Super.removeOnError();
            }
        });
    };
    Tree.prototype.removeOnReady = function () {
    };
    Tree.prototype.removeOnError = function () {
    };

    // id: ID del elemento buscado
    // load: Boolean que indica si se abrira el arbol para mostrar el lugar donde esta el elemento encontrado
    Tree.prototype.search = function (id, load) {
        var Super = this;
        var path = "";
        this.treeObj.search(null, {
            uid: 'user-defined',
            success: function (item, options) {
                Super.onSearchReady(item);
                if (load) {
                    uid: 'user-defined',
                    this.openPath(item, {
                        success: function (item, options) {
                            this.setVisible(item, {
                                uid: 'user-defined',
                                success: function (item, options) {
                                    path = 'root';
                                    this.path(item).each(this.proxy(function (element) {
                                        path += '.' + this.getId($(element)) + '';
                                    }, true));
                                },
                                fail: function (item, options) {
                                    Super.onSearchError("Error en setVisible");
                                }
                            });
                        },
                        fail: function (item, options) {
                            Super.onSearchError("Error en openPath");
                        }
                    });
                }
            },
            fail: function (item, options) {
                Super.onSearchError("Error en busqueda");
            },
            search: id,
            load: load
        });
    };
    Tree.prototype.onSearchReady = function (item) {
    };
    Tree.prototype.onSearchError = function (error) {
    };

    Tree.prototype.destroyed = function () { };
    Tree.prototype.destroyfail = function () { };

    this.setSettings(params);
    this.init(parent, data);
}