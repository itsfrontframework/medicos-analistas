﻿
var XHRprint = function (params) {
    this.ID = null;
    this.requestData = null;

    XHRprint.prototype.setSettings = function (params) {
        this.settings = $.extend({
            apiPrefix: "~/api/",
            url: "",
            method: "",
            responseType: ""
        }, params);
    }

    XHRprint.prototype.setId = function (data) {
        this.ID = data;
    }

    XHRprint.prototype.progressStep = function (step, error) {
        var size = step * 25;

        $('.progress-bar').css('width', size + '%').attr('aria-valuenow', size);

        if (size == 100) {
            setTimeout(function () {
                $('.progress-bar').addClass('progress-disable-animation');
                $('.progress-bar').css('width', '0%').attr('aria-valuenow', '0');
                setTimeout(function () {
                    $('.progress-bar').removeClass('progress-disable-animation');
                }, 600);
            }, 1000);
        }
    }

    XHRprint.prototype.send = function (dataObject) {
        Super = this;
        this.requestData = dataObject;

        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            var currentUrl = "";
            var statusOk = (xmlhttp.status >= 200 && xmlhttp.status < 300);

            if (xmlhttp.readyState == 0) {
                Super.progressStep(0, false);
            } else if (xmlhttp.readyState == 1) {
                Super.progressStep(1, false);
            } else if (xmlhttp.readyState == 2) {
                Super.progressStep(2, false);
            } else if (xmlhttp.readyState == 3) {
                Super.progressStep(3, false);
            }
            else if (xmlhttp.readyState == 4 && statusOk) {
                if (Super.settings.responseType == 'JSON') {
                    var jsonObj = null;
                    try {
                        jsonObj = JSON.parse(xmlhttp.responseText);
                    } catch (e) {
                        jsonObj = {};
                    }
                    Super.onReady(jsonObj);
                } else {
                    Super.onReady(xmlhttp.responseText);
                }

                Super.progressStep(4, false);
            } else if (xmlhttp.readyState == 4 && !statusOk) {
                Super.progressStep(4, true);
                if (Super.settings.responseType == 'XML') {
                    Super.onError(Super.getError(xmlhttp.responseText));
                } else {
                    Super.onError("El servicio contestó con un mensaje de error");
                }
            }
        }

        currentUrl = this.settings.url;
        if (this.ID != null) {
            currentUrl += "/" + this.ID;
        }

        if (this.settings.method == 'GET' && (typeof dataObject != 'undefined') && dataObject != null) {
            xmlhttp.open(this.settings.method, this.apiPrefix + currentUrl + '?' + jQuery.param(dataObject));
        } else {
            xmlhttp.open(this.settings.method, this.apiPrefix + currentUrl);
        }

        if (this.settings.responseType == 'JSON') {
            xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
            xmlhttp.setRequestHeader("Accept", "application/json");
        } else if (this.settings.responseType == 'XML') {
            xmlhttp.setRequestHeader("Content-type", "application/xml; charset=utf-8");
            xmlhttp.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        }

        if (this.settings.method != 'GET' && (typeof dataObject != 'undefined') && dataObject != null) {
            xmlhttp.send(JSON.stringify(dataObject));
        } else {
            xmlhttp.send();
        }

    }

    XHRprint.prototype.getError = function (data) {
        var errorMessage = "El servicio contestó con un mensaje de error XML";
        var xmlData = null;
        return errorMessage;
    }

    XHRprint.prototype.onReady = function (response) {
        openWindow(response);
    }

    XHRprint.prototype.onError = function (response) {
    }


    this.setSettings(params);
}