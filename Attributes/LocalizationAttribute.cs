﻿using BackendCore.Logging;
using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace base_front.Attributes {
    public class LocalizationAttribute : ActionFilterAttribute {
        private string defaultLanguage = "es";

        public LocalizationAttribute(string defaultLanguage) {
            this.defaultLanguage = defaultLanguage;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            string language = ((string)filterContext.RouteData.Values["lang"] ?? defaultLanguage);
            if (language != defaultLanguage) {
                try {
                    Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
                }
                catch (Exception e) {
                    Logger.Instance.Error(e);
                    throw new NotSupportedException(string.Format("ERROR: Invalid language code '{0}'.", language));
                }
            }
        }
    }
}