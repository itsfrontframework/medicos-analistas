﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace base_front.Controllers.MVC
{
    public class ModuleController : Controller
    {
        /// <summary>
        /// Lista los módulos 
        /// </summary>
        /// <returns></returns>
        public PartialViewResult List()
        {
            return PartialView();
        }

        /// <summary>
        /// Permite agregar un nuevo módulo
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Add()
        {
            return PartialView();
        }

        /// <summary>
        /// Permite editar la información de un módulo
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Edit()
        {
            return PartialView();
        }
    }
}
