﻿
var SpellChecker = function (params) {
    this.modalElement = null;
    this.textDiv = null;
    this.optionsSelect = null;
    this.finished = false;

    this.objResponse = null;
    this.currentToken = 0;
    this.currentTextToken = 0;
    this.originalText = null;
    this.textTokens = null;

    this.btnCancelar = null;
    this.btnTerminar = null;
    this.endMessage = null;

    SpellChecker.prototype.init = function() {
        var Super = this;
        var modalAttributes = 'id="spellCheckModal" tabindex="-1" role="dialog" aria-labelledby="spellCheckModalLabel" aria-hidden="true"';
        this.modalElement = $('<div ' + modalAttributes + '></div>').addClass('modal fade');
        var modalDialog = $('<div></div>').addClass('modal-dialog');
        var modalContent = $('<div></div>').addClass('modal-content');
        var modalHeader = $('<div></div>').addClass('modal-header');
        var modalBody = $('<div></div>').addClass('modal-body');
        var modalTitle = $('<h4 id="spellCheckModalLabel"></h4>').addClass('modal-title').html('Revisión ortográfica');
        modalHeader.append(modalTitle);        
        this.endMessage = $('<div></div>').addClass('alert alert-success').append('La revisión ortográfica terminó').css('padding', '2px').hide();
        modalBody.append(this.endMessage);


        var bodyDiv = $('<div></div>').addClass('row');
        var divFields = $('<div></div>').addClass('col-sm-9');
        var divButtons = $('<div></div>').addClass('col-sm-3');

        // Fields elements
        var textLabel = $('<label></label>').html('No se encontró:');
        this.textDiv = $('<div></div>').css({
            "border": "solid 1px #CFCFCF", 
            "height": "80px", 
            "padding": "5px"
        });
        var optionsDiv = $('<div></div>').addClass('form-group');
        var optionsLabel = $('<label></label>').html('Sugerencias:');
        this.optionsSelect = $('<select size="6"></select>').addClass('form-control');
        optionsDiv.append(optionsLabel);
        optionsDiv.append(this.optionsSelect);
        divFields.append(textLabel);
        divFields.append(this.textDiv);
        divFields.append('<br />');
        divFields.append(optionsDiv);
        
        // Buttons elements
        var btnOmitir = $('<button></button>').addClass('btn btn-default btn-block').html('Omitir');
        var btnCambiar = $('<button></button>').addClass('btn btn-default btn-block').html('Cambiar');
        this.btnCancelar = $('<button></button>').addClass('btn btn-default btn-block').css('margin-top', '95px').html('Cancelar');
        this.btnTerminar = $('<button></button>').addClass('btn btn-primary btn-block').html('Finalizar').prop("disabled", true);
        divButtons.append('<label>&nbsp;</label>');
        divButtons.append(btnOmitir);
        divButtons.append(btnCambiar);
        divButtons.append(this.btnCancelar);
        divButtons.append(this.btnTerminar);

        bodyDiv.append(divFields);
        bodyDiv.append(divButtons);
        modalBody.append(bodyDiv);

        modalContent.append(modalHeader);
        modalContent.append(modalBody);
        modalDialog.append(modalContent);
        this.modalElement.append(modalDialog);

        $('body').append(this.modalElement);
        
        btnCambiar.click(function () {
            // Change the token
            if (Super.optionsSelect.val() != null) {
                Super.textTokens[Super.currentTextToken][1] = Super.optionsSelect.val();
            }

            Super.currentToken++;
            if (Super.currentToken >= Super.objResponse.flaggedTokens.length) {
                Super.checkCompleted();
            } else {
                Super.processToken();
            }
        });
        btnOmitir.click(function () {
            Super.currentToken++;
            if (Super.currentToken >= Super.objResponse.flaggedTokens.length) {
                Super.checkCompleted();
            } else {
                Super.processToken();
            }
        });
        this.btnCancelar.click(function () {
            Super.modalElement.modal('hide');
            setTimeout(function () {
                Super.modalElement.remove();
                Super.modalElement = null;
                Super.settings.cancelFunction.apply(Super, Super.settings.cancelParameters);
            }, 500);
        });

        this.btnTerminar.click(function () {
            Super.checkClose();
        });
        
    }

    SpellChecker.prototype.checkCompleted = function () {
        this.btnCancelar.prop("disabled", true);
        this.btnTerminar.prop("disabled", false);
        this.endMessage.show();
    }

    SpellChecker.prototype.checkClose = function () {
        var Super = this;
        var finalText = '';
        for (var idx in this.textTokens) {
            finalText += this.textTokens[idx][1] + ' ';
        }
        finalText = finalText.substring(0, finalText.length - 1);

        var tmpParams = this.settings.successParameters;
        if (tmpParams == null) tmpParams = [];
        tmpParams.unshift(finalText);

        this.modalElement.modal('hide');
        setTimeout(function () {
            Super.modalElement.remove();
            Super.modalElement = null;
            Super.settings.successFunction.apply(Super, tmpParams);
        }, 500);
    }

    SpellChecker.prototype.getTokenArray = function (text) {
        var idx = 0;
        var arr = [];
        var tokens = text.split(' ');
        for (var i = 0; i < tokens.length; i++) {
            arr.push([idx, tokens[i]]);
            idx += tokens[i].length + 1;
        }
        return arr;
    }

    // Mostrar modal y mandar la petición al servicio
    SpellChecker.prototype.check = function (textToValidate) {

        // Tal vez se deba mostrar un loading
        this.finished = false;
        this.modalElement.modal('show');

        this.objResponse = null;
        this.originalText = textToValidate;
        this.textTokens = this.getTokenArray(textToValidate);

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onSpellDataReady(xmlhttp.responseText, textToValidate);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                // Error al obtener los datos
                console.debug("Error al consultar el servicio de spelling check");
            }
        }

        xmlhttp.open("POST", this.settings.spellCheckServiceUrl, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send(JSON.stringify(textToValidate));
    }

    // Recibir la respuesta del servicio y mandar a la primeriteracion  de processToken
    SpellChecker.prototype.onSpellDataReady = function (response, textToValidate) {
        try {
            this.objResponse = JSON.parse(response);
        } catch (e) {
            this.objResponse = null;
        }

        this.currentToken = 0;
        if (this.objResponse != null && this.objResponse.flaggedTokens.length > 0) {
            this.processToken();
        } else {
            this.checkCompleted();
        }
    }

    SpellChecker.prototype.processToken = function () {
        var token = this.objResponse.flaggedTokens[this.currentToken];

        // buscar el match con el offset
        while (token.offset > this.textTokens[this.currentTextToken][0]) {
            this.currentTextToken++;
            if (this.currentTextToken >= this.textTokens.length) {
                break;
            }
        }

        // Si el offset se encontro
        if (token.offset == this.textTokens[this.currentTextToken][0]) {
            
            var startIndex = this.currentTextToken;
            var originalStartIndex = startIndex;
            var tmpReviewToken = this.textTokens[this.currentTextToken][1]; 

            // Logica para palabras que se unen
            if (token.token != tmpReviewToken) {
                this.currentTextToken++;
                tmpReviewToken += ' ' + this.textTokens[this.currentTextToken][1];
                if (token.token != tmpReviewToken) {
                    console.debug("parece que se han unido mas de dos labras");
                }
            }

            var endIndex = this.currentTextToken;
            var originalEndIndex = endIndex + 1;

            // Se calculan los indices superiores e inferiores
            startIndex = ((startIndex - 5) >= 0) ? (startIndex - 5) : 0;
            endIndex = ((endIndex + 5) < this.textTokens.length) ? (endIndex + 5) : (this.textTokens.length - 1);

            var textToShow = '';
            while (startIndex < originalStartIndex) {
                textToShow += this.textTokens[startIndex][1] + ' ';
                startIndex++;
            }
            textToShow += '<span style="color: red;">' + tmpReviewToken + '</span> ';
            while (originalEndIndex < endIndex) {
                textToShow += this.textTokens[originalEndIndex][1] + ' ';
                originalEndIndex++;
            }

            this.textDiv.html(textToShow);
        }


        if (this.currentTextToken >= this.textTokens.length) {
            alert("Revisión finalizada");
        }

        // Cargar listado de opciones
        this.optionsSelect.html('');
        var firstOpt = true;
        for (var sugIdx in token.suggestions) {
            var sug = token.suggestions[sugIdx].suggestion;
            if (firstOpt) {
                firstOpt = false;
                this.optionsSelect.append('<option value="' + sug + '" selected>' + sug + '</option>');
            } else {
                this.optionsSelect.append('<option value="' + sug + '">' + sug + '</option>');
            }
            
        }

    }

    SpellChecker.prototype.setSettings = function (params) {
        this.settings = $.extend({
            test: true,
            spellCheckServiceUrl: "/MedicosAnalistas/api/External/SpellCheck",
            indexPadding: 10,
            successFunction: null,
            successParameters: null,
            cancelFunction: null,
            cancelParameters: null
        }, params);
    }

    this.setSettings(params);
    this.init();
}