﻿var Table = function (parent, params) {
    this.tablebody = null;
    this.datacontainerrow = new Array();
    this.datarows = new Array();
    this.xmlData = null;

    this.hasButtons = false;
    this.rowsNumber = 0;
    this.dataSector = 0;
    this.totalDataSector = 0;
    this.currentValue;

    this.idControl = Math.ceil(Math.random() * 100000000);
    var label = '#' + this.idControl;

    this.nodeName = "";
    this.collectionName = "";
    var Super = this;


    Table.prototype.init = function (parent) {

        var mainPanel = $('<div></div>').addClass("table-responsive");
        var tablecontainer = $('<table id =' + this.settings.tablename + ' class="datagrid" ></table>').addClass("table table-striped table-bordered table-hover dataTable no-footer").css("table-layout", "fixed");
        var tablehead = $('<thead style="background-color:#337ab7; color:#ffffff;"></thead>');
        this.tablebody = $('<tbody></tbody>');
        var thhrow = $('<tr></tr>');

        for (var field in this.settings.fields) {
            fld = this.settings.fields[field];
            if (fld.visible) {
                var showLabel = ((fld.showlabel === undefined || fld.showlabel === null) ? true : fld.showlabel);
                var width = "auto";
                var sortable = "";
                if (fld.width !== null && fld.width !== undefined && typeof fld.width === 'number') {
                    width = fld.width + "px";
                }
                if (fld.sortable !== null && fld.sortable !== undefined && typeof fld.sortable === 'boolean') {
                    sortable = ((fld.sortable) ? "sorting datagrid-sorting-color" : "");
                }
                $(thhrow).append($('<th id="' + field + '">' + ((showLabel) ? fld.label : '') + '</th>').css("width", width).addClass(sortable));
            }
        }

        tablehead.append(thhrow);
        tablecontainer.append(tablehead);
        tablecontainer.append(this.tablebody);
        mainPanel.append(tablecontainer);

        var divButtons = $('<div align="center"></div>').addClass('dataTables_paginate paging_simple_numbers');
        var buttons = $('<ul align="center"></ul>').addClass('pagination');
        var btn = new Array();
        btn[0] = $('<li id="first" data-toggle="tooltip" data-placement="bottom" title="Primero"><a href="#"><i class="fa fa-fast-backward"></i></a></li>').addClass("paginate-button first");
        btn[1] = $('<li id="previous" data-toggle="tooltip" data-placement="bottom" title="Anterior"><a href="#"><i class="fa fa-step-backward"></i></a></li>').addClass("paginate-button previous");
        btn[2] = $('<li id="next" data-toggle="tooltip" data-placement="bottom" title="Siguiente"><a href="#"><i class="fa fa-step-forward"></i></a></li>').addClass("paginate-button next");
        btn[3] = $('<li id="last" data-toggle="tooltip" data-placement="bottom" title="Última"><a href="#"><i class="fa fa-fast-forward"></i></a></li>').addClass("paginate-button last");
        buttons.append(btn[0]);
        buttons.append(btn[1]);
        buttons.append(btn[2]);
        buttons.append(btn[3]);
        $(parent).append(mainPanel);
        divButtons.append(buttons);
        $(parent).append(divButtons);
        for (var btns in btn) {
            $(btn[btns]).click(function () {
                if (this.id == "first") {
                    Super.first();
                } else if (this.id == "previous") {
                    Super.previous();
                } else if (this.id == "next") {
                    Super.next();
                } else if (this.id == "last") {
                    Super.last();
                }
                return false;
            });
        }

        $('#' + this.settings.tablename + ' thead').on('click', 'th', function () {
            Super.order(Super.settings.fields[this.id], this.id);
        });
    }

    Table.prototype.onRowClick = function (fields) {

    }

    Table.prototype.onButtonClick = function (btnName, fields) {

    }

    Table.prototype.showData = function (sector) {
        this.dataSector = sector;
        this.tablebody.empty();
        this.datarows = new Array();
        var serializer = new XMLSerializer();
        for (x = 0; x < this.settings.rowCount; x++) {
            var indexRow = x + (this.dataSector * this.settings.rowCount);
            if (this.xmlData.getElementsByTagName(this.nodeName)[indexRow] != null) {
                onError = false;
                var rows = new Array();
                for (var field in this.settings.fields) {
                    fld = this.settings.fields[field];

                    if (this.xmlData.getElementsByTagName(this.nodeName)[indexRow].getElementsByTagName(fld.name)[0] == null) {
                        onError = true;
                    } else {
                        var value = this.xmlData.getElementsByTagName(this.nodeName)[indexRow].getElementsByTagName(fld.name)[0].innerHTML;
                        if (value !== undefined) {
                            value = value.replace(/&amp;/gi, "&")
                                .replace(/&quot;/gi, "\"")
                                .replace(/&apos;/gi, "'")
                                .replace(/&lt;/gi, "<")
                                .replace(/&gt;/gi, ">");

                            rows[fld.name] = value;
                        } else {
                            value = serializer.serializeToString(this.xmlData.getElementsByTagName(this.nodeName)[indexRow].getElementsByTagName(fld.name)[0]);
                            value = value.replace(/<.*>(.*)<\/.*>/g, "$1")
                                .replace(/&amp;/gi, "&")
                                .replace(/&quot;/gi, "\"")
                                .replace(/&apos;/gi, "'")
                                .replace(/&lt;/gi, "<")
                                .replace(/&gt;/gi, ">");

                            rows[fld.name] = value;
                        }
                        if (this.settings.testRegEx.test(rows[fld.name]) && this.settings.formatDate) {
                            rows[fld.name] = rows[fld.name].replace(this.settings.groupsRegEx, this.settings.replaceExpression);
                        }
                    }

                }

                if (!onError) {
                    this.datarows[x] = rows;
                    this.datacontainerrow[x] = $('<tr id="' + x + '"></tr>');
                    for (var field in this.settings.fields) {
                        fld = this.settings.fields[field];
                        if (fld.isButton) { Super.hasButtons = true; }
                        if (fld.visible && !fld.isButton) {
                            var value = rows[fld.name];
                            if (fld.conditions === null || fld.conditions === undefined) {
                                this.datacontainerrow[x].append($('<td>' + value + '</td>').addClass('datagrid-automatic-truncate'));
                            }
                            else {
                                this.currentValue = value;
                                var styleClass = this.evaluate(fld.conditions, value, rows);
                                this.datacontainerrow[x].append($('<td>' + this.currentValue + '</td>').addClass('datagrid-automatic-truncate' + styleClass));
                            }
                        } else if (fld.visible && fld.isButton) {
                            var asign = $('<button id="' + fld.name + '_' + x + '" type="button" class="btn ' + ((fld.cssclass === undefined || fld.cssclass === null) ? 'btn-success' : fld.cssclass) + ' btn-block btn-xs">' + (fld.icon !== undefined ? '<i class="fa ' + fld.icon + ' fa-fw"></i>' : '') + fld.label/*rows[fld.name]*/ + '</button>');
                            $(asign).click(function () {
                                var campo = this.id.split("_");
                                Super.onButtonClick(this, Super.datarows[campo[campo.length - 1]]);
                            });
                            this.datacontainerrow[x].append($('<td></td>').append(asign));
                        }
                    }
                    $(this.datacontainerrow[x]).click(function () {
                        if (!Super.hasButtons) {
                            Super.onRowClick(Super.datarows[this.id]);
                        }
                    });
                    this.tablebody.append(Super.datacontainerrow[x]);
                }
            } else {
                x = Super.settings.rowCount;
            }
        }

    }

    Table.prototype.loadXML = function (xmlData) {
        this.onLoadXML(xmlData);
    }

    Table.prototype.loadEmpty = function () {
        this.tablebody.empty();
    }

    Table.prototype.onLoadXML = function (xmlData) {
        if ((typeof xmlData != 'undefined') && xmlData != null && xmlData.length > 0) {
            if (window.DOMParser) {
                parser = new DOMParser();
                this.xmlData = parser.parseFromString(xmlData, "text/xml");
            } else {
                this.xmlData = new ActiveXObject("Microsoft.XMLDOM");
                this.xmlData.async = false;
                this.xmlData.loadXML(xmlData);
            }

            if (this.xmlData != null) {
                Super.collectionName = this.xmlData.documentElement.nodeName;
                this.nodeName = this.xmlData.documentElement.nodeName.substring(7);

                if (this.xmlData.getElementsByTagName(this.nodeName) != null)
                    this.rowsNumber = this.xmlData.getElementsByTagName(this.nodeName).length;

                var div = Math.floor(this.rowsNumber / this.settings.rowCount);
                var rem = this.rowsNumber % this.settings.rowCount;
                if (rem > 0) {
                    this.totalDataSector = div;
                } else {
                    this.totalDataSector = (div - 1);
                }
                this.showData(0);
                this.onLoadEnd();
            }
        } else {
            this.tablebody.empty();
        }
    }

    Table.prototype.next = function () {
        if (this.totalDataSector >= (this.dataSector + 1)) {
            this.dataSector = this.dataSector + 1;
        }
        this.showData(this.dataSector);
    };

    Table.prototype.previous = function () {
        if (0 <= (this.dataSector - 1)) {
            this.dataSector = this.dataSector - 1;
        }
        this.showData(this.dataSector);
    };

    Table.prototype.last = function () {
        this.showData(this.totalDataSector);
    };

    Table.prototype.first = function () {
        this.showData(0);
    };

    Table.prototype.setSettings = function (params) {
        this.settings = $.extend({
            fields: "",
            rowCount: 10,
            dataXML: "",
            tablename: "",
            formatDate: true,
            testRegEx: /\d{4}-\d{2}-\d{2}T\d{2}\:\d{2}\:\d{2}\.\d+/,
            groupsRegEx: /^(\d{4})-(\d{2})-(\d{2})T(\d{2})\:(\d{2})\:(\d{2})\.\d+/,
            replaceExpression: "$3/$2/$1 $4:$5:$6"
        }, params);
    };

    Table.prototype.onLoadEnd = function () {

    };

    Table.prototype.addRow = function () {
        var newRow = $('<tr id="newAddingRow"></tr>');
        var tmpTd = null;
        this.last();

        for (var field in this.settings.fields) {
            tmpTd = $('<td></td>');
            tmpTd.append('<input type="text" />');
            newRow.append(tmpTd);
        }

        this.tablebody.append(newRow);
    };

    Table.prototype.evaluate = function (conditions, value, row) {
        var style = '';
        for (var idx in conditions) {
            if (conditions[idx].expression !== null && conditions[idx].expression !== undefined) {
                var completeExpression = conditions[idx].expression.replace(/value/gi, value);
                if (eval(completeExpression) === true) {
                    if (conditions[idx].style !== null && conditions[idx].style !== undefined) {
                        style = style + ' ' + conditions[idx].style;
                    }
                    if (conditions[idx].value !== null && conditions[idx].value !== undefined) {
                        this.currentValue = conditions[idx].value;
                    }
                }
            }
        }
        return style;
    };

    Table.prototype.order = function (field, id) {

        if (typeof field.sortable != 'undefined' && field.sortable) {

            if (Super.settings.fields[id].direction !== null && Super.settings.fields[id].direction !== undefined) {
                Super.settings.fields[id].direction = ((Super.settings.fields[id].direction == 'ASC') ? 'DESC' : 'ASC');
            } else {
                Super.settings.fields[id].direction = 'ASC';
            }

            var data = this.XML2JS(this.xmlData, this.collectionName);
            var orderArray = [];
            var strResult = "";

            for (var idx = 0; idx < data.length; idx++) {
                orderArray.push([data[idx][field.name], data[idx]]);
            }

            orderArray.sort(function (x, y) {
                var xVal = "";
                var yVal = "";

                if (typeof x[0] != 'undefined' && x[0] != null && typeof x[0].data != 'undefined' && x[0].data != null) {
                    xVal = x[0].data.trim();
                }
                if (typeof y[0] != 'undefined' && y[0] != null && typeof y[0].data != 'undefined' && y[0].data != null) {
                    yVal = y[0].data.trim();
                }

                if (Super.settings.fields[id].direction == 'ASC') {
                    //!isNaN(parseFloat(n)) && isFinite(n)
                    if ((!isNaN(parseFloat(x[0].data)) && isFinite(x[0].data)) && (!isNaN(parseFloat(y[0].data)) && isFinite(y[0].data))) {
                        return x[0].data - y[0].data;
                    } else {
                        if (xVal < yVal) return -1;
                        if (xVal > yVal) return 1;
                        return 0;
                    }
                } else {
                    if ((!isNaN(parseFloat(x[0].data)) && isFinite(x[0].data)) && (!isNaN(parseFloat(y[0].data)) && isFinite(y[0].data))) {
                        return y[0].data - x[0].data;
                    } else {
                        if (xVal < yVal) return 1;
                        if (xVal > yVal) return -1;
                        return 0;
                    }
                }
            });

            strResult = this.buildXMLString(orderArray);
            this.loadXML(strResult);
        }

    };

    // convert XML data into JavaScript array of JavaScript objects
    Table.prototype.XML2JS = function (xmlDoc, containerTag) {
        var output = new Array();
        var rawData = xmlDoc.getElementsByTagName(containerTag)[0];
        var i, j, oneRecord, oneObject;
        for (i = 0; i < rawData.childNodes.length; i++) {
            if (rawData.childNodes[i].nodeType == 1) {
                oneRecord = rawData.childNodes[i];
                oneObject = output[output.length] = new Object();
                for (j = 0; j < oneRecord.childNodes.length; j++) {
                    if (oneRecord.childNodes[j].nodeType == 1) {
                        oneObject[oneRecord.childNodes[j].tagName] = oneRecord.childNodes[j].firstChild;
                    }
                }
            }
        }
        return output;
    }

    Table.prototype.buildXMLString = function (data) {
        var strResult = "";
        var objectName = this.collectionName.substring(7, this.collectionName.length);

        strResult += '<' + this.collectionName + ' xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/MedicosAnalistas.Models">';
        for (var idx in data) {
            strResult += '<' + objectName + '>';

            for (var jdx in data[idx][1]) {
                var cellValue = data[idx][1][jdx];
                if (cellValue != null) {
                    cellValue = cellValue.data
                    cellValue = cellValue.replace(/&/gi, "&amp;")
                        .replace(/"/gi, "&quot;")
                        .replace(/'/gi, "&apos;")
                        .replace(/</gi, "&lt;")
                        .replace(/>/gi, "&gt;");
                } else {
                    cellValue = "";
                }

                strResult += '<' + jdx + '>';
                strResult += cellValue;
                strResult += '</' + jdx + '>';
            }
            strResult += '</' + objectName + '>';
        }
        strResult += '</' + this.collectionName + '>';

        return strResult;
    }

    this.setSettings(params);
    this.init(parent);
    this.onLoadXML(this.settings.dataXML);
};