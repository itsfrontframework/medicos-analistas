﻿
var Wysiwyg = function (parent) {
    this.container = parent;
    this.editor = '';

    Wysiwyg.prototype.init = function () {
        var Super = this;
        var idControl = Math.ceil(Math.random() * 100000000);
        Super.editor = '#wysiwyg' + idControl;

        Super.buildHtml();
        Super.initToolbarBootstrapBindings();
        $(Super.editor).wysiwyg({ fileUploadError: Super.showErrorAlert });
        window.prettyPrint && prettyPrint();
    }

    Wysiwyg.prototype.getHTML = function () {
        if (this.editor != '') return $(this.editor).html();
        else return '';
    }

    Wysiwyg.prototype.setHTML = function (html) {
        $(this.editor).html(html);
    }

    Wysiwyg.prototype.initToolbarBootstrapBindings = function () {
        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                      'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                      'Times New Roman', 'Verdana'],
                      fontTarget = $('[title=Fuente]').siblings('.dropdown-menu');
        $.each(fonts, function (idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
        });
        $('a[title]').tooltip({ container: 'body' });
        $('.dropdown-menu input').click(function () { return false; })
            .change(function () { $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); })
        .keydown('esc', function () { this.value = ''; $(this).change(); });

        $('[data-role=magic-overlay]').each(function () {
            var overlay = $(this), target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $(Super.editor).offset();
            $('#voiceBtn').css('position', 'absolute').offset({ top: editorOffset.top, left: editorOffset.left + $(Super.editor).innerWidth() - 35 });
        } else {
            $('#voiceBtn').hide();
        }
    }

    Wysiwyg.prototype.showErrorAlert = function (reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') { msg = "Formato no soportado " + detail; }
        else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert alert-warning" style="margin-bottom: 0px;"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
         '<strong>Error en la carga de la imagen:</strong> ' + msg + ' </div>').prependTo('#alerts');
    }

    Wysiwyg.prototype.buildHtml = function () {
        var html = '';
        html += '<div id="alerts"></div>';
        html += '<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor" style="padding-top: 10px;">';
        html += '   <div class="btn-group toolbar-btn-group">';
        html += '   <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Fuente"><i class="fa fa-font"></i><b class="caret"></b></a>';
        html += '       <ul class="dropdown-menu">';
        html += '       </ul>';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '   <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Tamaño de fuente"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>';
        html += '       <ul class="dropdown-menu">';
        html += '           <li><a data-edit="fontSize 5"><font size="5">Grande</font></a></li>';
        html += '           <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>';
        html += '           <li><a data-edit="fontSize 1"><font size="1">Chica</font></a></li>';
        html += '       </ul>';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '   <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Color de fuente"><i class="fa fa-paint-brush"></i>&nbsp;<b class="caret"></b></a>';
        html += '       <ul class="dropdown-menu">';
        html += '           <li><a data-edit="foreColor black" style="color: black;"> ';
        html += '               <span style="background-color: black;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Negro';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor red" style="color: red;"> ';
        html += '               <span style="background-color: red;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Rojo';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor orange" style="color: orange;"> ';
        html += '               <span style="background-color: orange;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Naranja';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor yellow" style="color: yellow;"> ';
        html += '               <span style="background-color: yellow;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Amarillo';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor green" style="color: green;"> ';
        html += '               <span style="background-color: green;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Verde';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor aqua" style="color: aqua;"> ';
        html += '               <span style="background-color: aqua;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Cian';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor blue" style="color: blue;"> ';
        html += '               <span style="background-color: blue;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Azul';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor purple" style="color: purple;"> ';
        html += '               <span style="background-color: purple;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Morado';
        html += '               </a></li>';
        html += '           <li><a data-edit="foreColor deeppink" style="color: deeppink;"> ';
        html += '               <span style="background-color: deeppink;">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; Rosa';
        html += '               </a></li>';
        html += '       </ul>';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '       <a class="btn btn-default btn-sm" data-edit="bold" title="Negrita (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="italic" title="Italica (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="strikethrough" title="Tachado"><i class="fa fa-strikethrough"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="underline" title="Subrayado (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '       <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="Lista"><i class="fa fa-list-ul"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="Lista numerada"><i class="fa fa-list-ol"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="outdent" title="Reducir identación (Shift+Tab)"><i class="fa fa-outdent"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="indent" title="Identación (Tab)"><i class="fa fa-indent"></i></a>';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '       <a class="btn btn-default btn-sm" data-edit="justifyleft" title="Alinear a la izquierda (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="justifycenter" title="Centrado (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="justifyright" title="Alinear a la derecha (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="justifyfull" title="Justificado (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '       <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Hipervinculo"><i class="fa fa-link"></i></a>';
        html += '       <div class="dropdown-menu input-append" style="min-width: 240px;">';
        html += '           <input class="span2" placeholder="URL" type="text" data-edit="createLink" style="margin-left: 10px;"/>';
        html += '           <button class="btn btn-default btn-sm" type="button" style="float: initial;">Agregar</button>';
        html += '       </div>';
        html += '       <a class="btn btn-default btn-sm" data-edit="unlink" title="Remover hipervinculo"><i class="fa fa-cut"></i></a>';
        html += '   </div>';
      
        html += '   <div class="btn-group toolbar-btn-group">';
        html += '       <a class="btn btn-default btn-sm" title="Insertar imágen (o solo arrastre)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>';
        html += '       <input type="file" data-role="magic-overlay" id="pictureFile" data-target="#pictureBtn" data-edit="insertImage" />';
        html += '   </div>';

        html += '   <div class="btn-group toolbar-btn-group">';
        html += '       <a class="btn btn-default btn-sm" data-edit="undo" title="Deshacer (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>';
        html += '       <a class="btn btn-default btn-sm" data-edit="redo" title="Rehacer (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>';
        html += '   </div>';
        html += '   <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">';
        html += '</div>';

        html += '<div id="' + this.editor.substring(1) + '" class="wysiwyg-editor">';
        html += '</div>';

        $(this.container).html(html);
    }

    this.init();
}