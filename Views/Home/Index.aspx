﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Home.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="<%=ResolveUrl("~/") %>">
                    <img src="../../Content/images/logo.png" class="img-responsive" id="logo" width="120" alt="<%=BackendCore.Environment.Parameters.getValueOf("APPLICATION_TITLE")%>">
                </a>
            </div>

            <div class="navbar-header">
                <span class="navbar-brand navbarBrandTitle" id="navbarTitle"></span>
            </div>

            <%--<div class="navbar-header">
                <span class="navbar-brand navbarBrandTitle" style="color:coral;"><b>SITIO QA</b></span>
            </div>--%>

            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <%=ViewData["CurrentUser_Name"].ToString() %>&nbsp;<i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu" id="dropdown-menu">
                        <%--<li>
                            <a href="#" onclick="screen.load('roles-list');return false;"><i class="fa fa-bullseye fa-fw"></i>Roles</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('parameters-list');return false;"><i class="fa fa-sliders fa-fw"></i>Parámetros</a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="#" onclick="screen.load('medical-record-list');return false;"><i class="fa fa-file-text-o fa-fw"></i>Expedientes</a>
                        </li>
                        <%--<li>
                            <a href="#" onclick="screen.load('workflows-list');return false;"><i class="fa fa-code-fork fa-fw"></i>Workflows</a>
                        </li>--% >
                        <li>
                            <a href="#" onclick="screen.load('glossary-list');return false;"><i class="fa fa-book fa-fw"></i>Glosario</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('genes-list');return false;"><i class="fa fa-flask fa-fw"></i>Genes</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('drugs-list');return false;"><i class="fa fa-medkit fa-fw"></i>Fármacos</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('diseases-list');return false;"><i class="fa fa-ambulance fa-fw"></i>Enfermedades</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('technical-list');return false;"><i class="fa fa-file-image-o fa-fw"></i>Informe técnico</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('actions-list');return false;"><i class="fa fa-stethoscope fa-fw"></i>Acciones farmacológicas</a>
                        </li>
                        <li>
                            <a href="#" onclick="screen.load('interactions-list');return false;"><i class="fa fa-random fa-fw"></i>Interacciones farmacológicas</a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="<%=Url.Action("Logout","Account") %>"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                        </li>--%>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-header navbar-right">
                <span class="navbar-brand navbarBrandTitle" id="navbarTitle2"></span>
            </div>

        </nav>
        <!--Main content area-->
        <div id="page-wrapper"></div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/jquery/dist/jquery.min.js") %>'></script>
    <!-- Bootstrap Core JavaScript -->
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/bootstrap/dist/js/bootstrap.min.js") %>'></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/metisMenu/dist/metisMenu.min.js") %>'></script>
    <!-- Custom Theme JavaScript -->
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/dist/js/sb-admin-2.js") %>'></script>

    <!-- NVD3 -->
    <script src='<%= ResolveUrl("~/Content/other_components/d3.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/other_components/nv.d3.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/other_components/stream_layers.js") %>'></script>

    <!-- DatePicker -->
    <script src='<%= ResolveUrl("~/Content/other_components/bootstrap-datepicker.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/other_components/bootstrap-datepicker.es.min.js") %>'></script>

    <!-- Select2 -->
    <script src='<%= ResolveUrl("~/Content/other_components/select2.full.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/other_components/select2-es.js") %>'></script>

    <!-- DataTables JavaScript -->
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/datatables/media/js/jquery.dataTables.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js") %>'></script>

    <!-- JSpdf -->
    <script src='<%= ResolveUrl("~/Content/other_components/jspdf.min.js") %>'></script>

    <%--<!-- Bootstrap wysiwyg -->
    <script src='<%= ResolveUrl("~/Content/other_components/bootstrap-wysiwyg.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/other_components/jquery.hotkeys.js") %>'></script>--%>

    <!-- Custom components -->
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-XHRservice.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Table.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-DataTable.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Button.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-RadioButton.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Combo.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-MultipleCombo.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Confirm.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-ScreenCaller.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-ResultsComponent.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-PDFgenerator.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Process.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Messages.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-SpellChecker.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-DynamicFields.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-BiblioComponent.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>
    <script src='<%= ResolveUrl("~/Content/custom_components/CC-Menu.js?version=0." + BackendCore.Environment.Parameters.getValueOf("FRONTEND_VERSION")) %>'></script>

    <!-- JavaScript -->
    <script type="text/javascript">
        <%string uuid = (Session["UUID"] != null ? Session["UUID"].ToString() : "");%>
        <%--var uuid = '<%=Session["UUID"]%>';--%>
        var uuid = '<%=uuid%>';
        var current_user = '<%=BackendCore.Authentication.Sessions.getOwner(uuid).USERNAME%>';
        var screen = new Screen("#page-wrapper", uuid);
        screen.passParameters = function (parameters) {
            //console.debug(parameters);
        };

        var menu = new Menu('#dropdown-menu', {
            headers: { UUID: '<%=Session["UUID"]%>' },
            logout: true,
            logoutUrl: "<%=Url.Action("Logout","Account") %>"
        });
        menu.getData();
        menu.onItemClick = function (menuItem) {
            screen.load(menuItem);
        }

        $(document).ready(function () {
            screen.load('dashboard');
        });
    </script>
</asp:Content>
