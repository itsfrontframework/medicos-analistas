﻿
var FileXHRservice = function (params) {
    this.USER = null;
    this.ID = null;
    this.contentType = "multipart/form-data;";

    FileXHRservice.prototype.setSettings = function (params) {
        this.settings = $.extend({
            apiPrefix: "~/api/",
            url: "",
            method: "",
            responseType: ""
        }, params);
    }

    FileXHRservice.prototype.setId = function (data) {
        this.ID = data;
    }

    FileXHRservice.prototype.setUser = function (data) {
        this.USER = data;
    }

    FileXHRservice.prototype.setContentType = function (data) {
        this.contentType = data;
    }

    FileXHRservice.prototype.progressStep = function (step, error) {
        var size = step * 25;

        $('.progress-bar').css('width', size + '%').attr('aria-valuenow', size);

        if (size == 100) {
            setTimeout(function () {
                $('.progress-bar').addClass('progress-disable-animation');
                $('.progress-bar').css('width', '0%').attr('aria-valuenow', '0');
                setTimeout(function () {
                    $('.progress-bar').removeClass('progress-disable-animation');
                }, 600);
            }, 1000);
        }
    }

    FileXHRservice.prototype.send = function (dataObject) {
        Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            var currentUrl = "";
            var statusOk = (xmlhttp.status >= 200 && xmlhttp.status < 300);

            if (xmlhttp.readyState == 0) {
                Super.progressStep(0, false);
            } else if (xmlhttp.readyState == 1) {
                Super.progressStep(1, false);
            } else if (xmlhttp.readyState == 2) {
                Super.progressStep(2, false);
            } else if (xmlhttp.readyState == 3) {
                Super.progressStep(3, false);
            }
            else if (xmlhttp.readyState == 4 && statusOk) {
                if (Super.settings.responseType == 'JSON') {
                    var jsonObj = null;
                    try {
                        jsonObj = JSON.parse(xmlhttp.responseText);
                    } catch (e) {
                        jsonObj = {};
                    }
                    Super.onReady(jsonObj);
                } else {
                    Super.onReady(xmlhttp.responseText);
                }

                Super.progressStep(4, false);
            } else if (xmlhttp.readyState == 4 && !statusOk) {
                Super.progressStep(4, true);
                if (Super.settings.responseType == 'XML') {
                    Super.onError(Super.getError(xmlhttp.responseText));
                } else {
                    //Message
                    var jsonObj = null;
                    try {
                        jsonObj = JSON.parse(xmlhttp.responseText);
                    } catch (e) {
                        jsonObj = {};
                    }

                    if (jsonObj.Message !== undefined) {
                        Super.onError(jsonObj.Message);
                    } else {
                        Super.onError("El servicio contestó con un mensaje de error");
                    }

                }
            }
        }

        currentUrl = this.settings.url;
        if (this.ID != null) {
            currentUrl += "/" + this.ID;
        }

        if (this.settings.method == 'GET' && (typeof dataObject != 'undefined') && dataObject != null) {
            xmlhttp.open(this.settings.method, this.settings.apiPrefix + currentUrl + '?' + jQuery.param(dataObject), true);
        } else {
            xmlhttp.open(this.settings.method, this.settings.apiPrefix + currentUrl, true);
        }

        if (this.USER != null) {
            xmlhttp.setRequestHeader("USUARIO", this.USER);
        }

        if (this.settings.responseType == 'JSON') {
            xmlhttp.setRequestHeader("Content-Type", this.contentType);
            xmlhttp.setRequestHeader("Accept", "application/json");
        } else if (this.settings.responseType == 'XML') {
            xmlhttp.setRequestHeader("Content-Type", this.contentType);
            xmlhttp.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        }

        if (this.settings.method != 'GET' && (typeof dataObject != 'undefined') && dataObject != null) {
            xmlhttp.send(dataObject);
        }
        else {
            xmlhttp.send();
        }

    }

    FileXHRservice.prototype.getError = function (data) {
        var errorMessage = "El servicio contestó con un mensaje de error XML";
        var xmlData = null;
        return errorMessage;
    }

    FileXHRservice.prototype.onReady = function (response) {
    }

    FileXHRservice.prototype.onError = function (response) {
    }


    this.setSettings(params);
}