﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace base_front.Controllers.MVC
{
    public class MenuController : Controller
    {
        /// <summary>
        /// Lista los elemento del menu
        /// </summary>
        /// <returns></returns>
        public PartialViewResult List()
        {
            return PartialView();
        }

        /// <summary>
        /// Agregar un elemento al menú
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Add()
        {
            return PartialView();
        }

        /// <summary>
        /// Edita un elemento del menú
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Edit()
        {
            return PartialView();
        }
    }
}
