﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using BackendCore.Exceptions;

namespace base_front.Services
{
    /// <summary>
    /// Servicio que permite manejar los módulos del sistema.
    /// </summary>
    /// <remarks>
    /// No se pueden agregar módulos, solo se pueden desactivar o modificar su descripción
    /// </remarks>
    public class ModuleService
    {
        //VARIABLES
        private ConnectionStringSettings csSettings;
        private string repositoryTable;
        private string query;

        /// <summary>
        /// Constructor de la clase. Genera una nueva instancia de <see cref="ScreenService"/>
        /// </summary>
        public ModuleService()
        {
            /*Connection Strings*/
            csSettings = ConfigurationManager.ConnectionStrings["SystemDB"];      //Sistema
            repositoryTable = ConfigurationManager.AppSettings["Repository"];

            //Verificar que se encuentre el repositorio
            if (string.IsNullOrWhiteSpace(repositoryTable))
            {
                //No se encontró el repositorio para realizar la consulta
                throw new ServiceException(HttpStatusCode.PreconditionFailed, ServiceError.NO_REPO_CONFIG_FOUND);
            }
        }
    }
}