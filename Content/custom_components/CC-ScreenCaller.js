﻿var Screen = function (parent, session_id, params) {
    /// <summary>"Servicio" ajax que permite abrir las distintas pantallas del sistema.</summary>
    /// <param name="parent" type="string">Nombre del div contenedor de la clase.</param>
    /// <param name="params" type="object">Los parametros que se obtienen para la clase.</param>
    /// <field name='parentControl'>Contenedor de la mayoria de los resultados del componente.</field>
    /// <field name='session_uuid'>Contenedor de la mayoria de los resultados del componente.</field>
    /// <field name='???'>?????????????????.</field>
    this.parentControl = $(parent);
    this.session_uuid = session_id;
    var Super = this;

    Screen.prototype.setSettings = function (params) {
        /// <summary>Determina el valor de las configuraciones del componente.</summary>
        /// <param name="params" type="object">Los parametros que se obtienen para la clase.</param>
        this.settings = $.extend({
            baseURL: "/api/Screen/Load",
            errorURL: "/Home/Error",
            startScreen: "#default#"
        }, params);
    }

    Screen.prototype.load = function (screenname, parameters) {
        /// <summary>Realiza la carga de la pantalla interna.</summary>
        /// <param name="screenname" type="string">Nombre del div contenedor de la clase.</param>
        /// <param name="parameters" type="object">Los parametros que se obtienen para la clase.</param>

        var url = this.settings.baseURL;
        var screen = ((screenname === "" || screenname === undefined || screenname === null) ? this.settings.startScreen : screenname);
        var params = JSON.stringify(parameters);

        $.ajax({
            url: url,
            data: {
                "screen": screen,  //Nombre de la pantalla a cargar
                "parameters": params
            },
            type: "POST",
            headers: {
                "UUID": Super.session_uuid // Use the actual type you need.
            },
            success: function (data) {

                $('#navbarTitle').html('');
                $('#navbarTitle2').html('');

                // Put the data into the element you care about.

                // ================================================================================================
                if (parameters !== undefined && parameters !== null) {
                    var tmpElem = document.createElement('textarea');
                    var scapedParameters = JSON.stringify(parameters).replace(/\\/gi, "\\\\");
                    scapedParameters = scapedParameters.replace(/'/gi, "__..___..__");

                    tmpElem.innerHTML = scapedParameters;
                    data += "<script>";
                    data += "   var _sysParams = '" + tmpElem.value + "';";
                    data += "   _sysParams = _sysParams.replace(/__..___..__/gi, \"'\");";
                    data += "   passParameters(JSON.parse(_sysParams));";
                    data += "</script>";
                }
                // ================================================================================================

                Super.parentControl.html(data);

                //if (parameters !== undefined && parameters !== null) {
                //    Super.passParameters(parameters);
                //}
            },
            error: function (jqXHR) {
                // Put whatever you need to do if the query fails here.
                //Super.loadErrorPage('No se pudo cargar la página', 'La página que intenta cargar no está disponible, es posible que el módulo se encuentre en mantenimineto.');
                //console.debug(jqXHR);
                Super.loadErrorPage(jqXHR.statusText, jqXHR.responseJSON.Message);
            }
        });
        return false;
    };

    Screen.prototype.loadErrorPage = function (title, message) {
        /// <summary>Realiza la carga de la pantalla de error interna.</summary>
        /// <param name="title" type="string">Nombre del div contenedor de la clase.</param>
        /// <param name="message" type="object">Los parametros que se obtienen para la clase.</param>
        Super.parentControl.load(this.settings.errorURL, function (r, s, x) {
            $('#error-box-title').html(title);
            $('#error-box-message').html(message);
        });
    };

    Screen.prototype.passParameters = function (parameters) {
        /// <summary>Pasa los parametros .</summary>
        /// <param name="title" type="string">Nombre del div contenedor de la clase.</param>
    };

    // Ejecutar 
    this.setSettings(params);
}
