﻿
var Messages = function (parent, params) {
    this.mainContainer = null;
    this.messageContainer = null;

    Messages.prototype.init = function (parent) {
        this.mainContainer = $('<div></div>').addClass('panel-box');
        this.messageContainer = $('<ul></ul>').addClass('chat');

        if (typeof this.settings.data != 'undefined' && this.settings.data != null) {
            for (var idx in this.settings.data) {
                this.addMessage(this.settings.data[idx]);
            }
        }

        this.mainContainer.append(this.messageContainer)
        $(parent).html(this.mainContainer);
        $('[data-toggle="tooltip"]').tooltip();

        this.onLoadEnd();
    }

    Messages.prototype.addMessage = function (mess) {
        var Super = this;
        var sameUser = (mess.AUTHOR_USERNAME.localeCompare(this.settings.user) == 0);
        var messageAction = (sameUser ? "sent" : "received");
        var messageImage = (sameUser ? "message" : "reply");

        var li = $('<li></li>').addClass('left clearfix message-entry ' + messageAction);
        var imageSpan = $('<span></span>').addClass('chat-img pull-left');
        var image = $('<img src="../../Content/images/' + messageImage + '.png" />').addClass('img-rounded').css('height', '48px');
        var messageBody = $('<div></div>').addClass('chat-body clearfix');
        var messageTitle = $('<div></div>').addClass('header');
        var messageAuthor = $('<strong></strong>').addClass('primary-font chat-message-title').html(mess.AUTHOR);
        var messageTimeContainer = $('<small data-toggle="tooltip" title="' + mess.DATE_FORMATTED + '"></small>').addClass('pull-right text-muted chat-message-time');
        var messageTime = $('<i></i>').addClass('fa fa-clock-o fa-fw');
        var messageText = $('<p></p>').addClass('chat-message-text text-muted').html(mess.MESSAGE);

        imageSpan.append(image);
        messageTimeContainer.append(messageTime).append(mess.DATE_AGO);
        messageTitle.append(messageTimeContainer).append('<div style="height: 10px;"></div>').append(messageAuthor);
        messageBody.append(messageTitle).append(messageText);
        li.append(imageSpan).append(messageBody);
        this.messageContainer.append(li);

        li.click(function () {
            Super.onMessageClick(mess);
        });
    }

    Messages.prototype.onMessageClick = function (obj) {
    }

    Messages.prototype.onLoadEnd = function () {
    }

    Messages.prototype.setSettings = function (params) {
        this.settings = $.extend({
            data: null,
            user: ""
        }, params);
    }

    this.setSettings(params);
    this.init(parent);

}