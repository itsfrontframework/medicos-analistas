﻿/**
 * File Chooser 
 *
 * @summary   Permite seleccionar archivos del equipo del usuario para su carga.
 *
 * @since     0.0.1
 * @class
 * @classdesc Permite seleccionar archivos del equipo del usuario.
 * @param parent 
 * @param progressbar 
 * @param params 
 */

var FileChooser = function (parent, progressbar, params) {
    this.mainPanel = null;
    this.headPanel = null;
    this.bodyPanel = null;
    this.footerPanel = null;
    this.messagePanel = null;

    this.footerRow = null;
    this.footerRowCol1 = null;
    this.footerRowCol2 = null;
    this.footerRowCol3 = null;
    this.footerRowCol4 = null;

    this.file = null;

    this.inputGroup = null;
    this.textInput = null;
    this.searchBtn = null;
    this.searchSpan = null;
    this.searchA = null;
    this.searchIcon = null;

    this.fileInput = null;
    this.messageBox = null;

    this.idControl = Math.ceil(Math.random() * 100000000);
    var label = '#' + this.idControl;
    var Super = this;

    var success = 'success';
    var info = 'info';
    var warning = 'warning';
    var danger = 'danger';


    FileChooser.prototype.init = function (parent) {
        this.mainPanel = $('<div></div>').addClass("panel panel-primary");
        this.headPanel = $('<div><b>' + this.settings.title + '</b></div>').addClass("panel-heading");
        this.bodyPanel = $('<div></div>').addClass("panel-body");
        this.footerPanel = $('<div></div>').addClass("panel-footer");
        this.messagePanel = $('<div id="messages"></div>');

        //Body panel

        if (Super.msieversion() === 0) {
            this.fileInput = $('<input type="file" name="inputFile" id="inputFile" accept=".csv" style="display:none">');
            this.inputGroup = $('<div></div>').addClass("form-group input-group");
            this.textInput = $('<input type="text" id="txtFileName" disabled>').addClass("form-control");
            this.searchSpan = $('<span></span>').addClass("input-group-btn");
            this.searchA = $('<a type="button" id="btnSearch"></a>').addClass("btn btn-default");
            this.searchIcon = $('<i></i>').addClass("fa " + this.settings.icon);
        } else {
            this.fileInput = $('<input type="file" name="inputFile" id="inputFile" accept=".csv" style="width: 100%;">');
            this.inputGroup = $('<div></div>').addClass("form-group input-group");
            this.textInput = $('<input type="text" id="txtFileName" style="display:none" disabled>').addClass("form-control");
            this.searchSpan = $('<span></span>').addClass("input-group-btn");
            this.searchA = $('<a type="button" id="btnSearch" style="display:none"></a>').addClass("btn btn-default");
            this.searchIcon = $('<i></i>').addClass("fa " + this.settings.icon);
        }

        //Footer
        this.footerRow = $('<div></div>').addClass("row");
        this.footerRowCol1 = $('<div></div>').addClass("col-lg-2");
        this.footerRowCol2 = $('<div></div>').addClass("col-lg-2");
        this.footerRowCol3 = $('<div></div>').addClass("col-lg-4");
        this.footerRowCol4 = $('<div></div>').addClass("col-lg-4");
        //this.searchBtn = $('<button  id="searchBtn" >' + this.settings.txtButton + '</button>').addClass("btn btn-primary btn-block");
        this.searchBtn = $('<input type="submit" id="searchBtn" value="' + this.settings.txtButton + '"/>').addClass("btn btn-primary btn-block");

        //Cuerpo del panel
        this.searchA.append(this.searchIcon);
        this.searchSpan.append(this.searchA);
        this.inputGroup.append(this.textInput);
        this.inputGroup.append(this.searchSpan);
        this.bodyPanel.append(this.inputGroup);
        this.bodyPanel.append(this.fileInput);

        //Footer del panel
        this.footerRowCol4.append(this.searchBtn);
        this.footerRow.append(this.footerRowCol1);
        this.footerRow.append(this.footerRowCol2);
        this.footerRow.append(this.footerRowCol3);
        this.footerRow.append(this.footerRowCol4);
        this.footerPanel.append(this.footerRow);

        this.mainPanel.append(this.headPanel);
        this.mainPanel.append(this.bodyPanel);
        this.mainPanel.append(this.footerPanel);

        $(parent).append(this.mainPanel);
        $(parent).append(this.messagePanel);

        if (Super.msieversion() === 0) { document.getElementById("btnSearch").href = "javascript:document.getElementById('txtFileName').value = '';document.getElementById('inputFile').click();"; }
        document.getElementById("inputFile").onchange = function () {
            //Super.updateFileName(document.getElementById("inputFile").files);
            Super.updateFileName(document.getElementById("inputFile").value);
        };
        document.getElementById("searchBtn").onclick = function (e) {
            Super.validateFile();
            //e.preventDefault();
        };


    };

    FileChooser.prototype.setSettings = function (params) {
        this.settings = $.extend({
            url: "",
            fields: "",
            sessionid: "",
            icon: "fa-folder-open",
            title: "Seleccione el archivo a cargar:",
            txtButton: "Cargar archivo seleccionado"
        }, params);
    };

    FileChooser.prototype.validateFile = function () {
        $("#messages").html('');
        if (document.getElementById("txtFileName").value !== "" && document.getElementById("txtFileName").value !== null) {
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                file = document.getElementById("inputFile").files[0];
                if (file !== null) {
                    Super.getFile(file);
                }
                else {
                    Super.showMessage("Archivo vacío!", danger);
                }
            } else {
                //Super.showMessage("Las APIs de archivos no son soportadas por este explorador.", danger);
                Super.getFile(undefined);
            }
        } else {
            //showMessage("Favor de seleccionar un archivo para continuar");
            Super.showMessage("Favor de seleccionar un archivo para continuar", danger);
        }
    };

    FileChooser.prototype.getFile = function (file) { };

    //Métodos de ayuda

    FileChooser.prototype.updateFileName = function (path) {
        var x;
        document.getElementById("txtFileName").value = "";
        if (path.substr(0, 12) == "C:\\fakepath\\") {
            try {
                document.getElementById("txtFileName").value = document.getElementById("inputFile").files[0].name;
            } catch (error) {
                try {
                    document.getElementById("txtFileName").value = path.substr(x + 1);
                } catch (e) {
                    Super.showMessage("No se ha seleccionado un archivo", danger);
                }
            }
        }
        x = path.lastIndexOf('/');
        if (x >= 0) {  // Unix-based path
            document.getElementById("txtFileName").value = path.substr(x + 1);
        }
        x = path.lastIndexOf('\\');
        if (x >= 0) {// Windows-based path
            document.getElementById("txtFileName").value = path.substr(x + 1);
        }
        return path; // just the file name
    };

    FileChooser.prototype.showMessage = function (mensaje, tipo) {
        this.messageBox = $('<div align="center"></div>').addClass("alert alert-" + tipo + " alert-dismissable");
        this.messageBox.append('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
        this.messageBox.append('<b>' + mensaje + '</b>');
        $("#messages").append(this.messageBox);
    };

    FileChooser.prototype.msieversion = function () {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        } else {
            return 0;
        }
    }

    //Llamada de inicialización

    this.setSettings(params);
    this.init(parent);
};

