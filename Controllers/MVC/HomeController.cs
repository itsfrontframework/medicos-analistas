﻿using BackendCore.Authentication;
using System.Web.Mvc;
using System.Web.Security;

namespace base_front.Controllers.MVC {
    public class HomeController : Controller {

        /// <summary>
        /// Página principal. Posee el marco general de la aplicación
        /// </summary>
        /// <remarks>GET: ~/Home </remarks>
        /// <returns></returns>
        public ActionResult Index() {
            string currentUser_Name = "";
            checkForDeadSession();
            currentUser_Name = Sessions.getOwner(Session["UUID"].ToString()).NAME;
            if (!string.IsNullOrWhiteSpace(currentUser_Name)) {
                string[] username_Split = currentUser_Name.Trim().Split(' ');
                if (username_Split.Length > 1) {
                    currentUser_Name = username_Split[0] + ((username_Split[0].ToUpper().Contains("DR")) ? " " + username_Split[1] : "");
                }
            } else {
                currentUser_Name = "";
            }

            ViewData["CurrentUser_Name"] = currentUser_Name;
            return View();
        }

        public ActionResult Maintenance() {
            return View();
        }

        /// <summary>
        /// Error manejado interno de la aplicación
        /// </summary>
        /// <remarks>GET: ~/Home/Error </remarks>
        /// <returns></returns>
        public ActionResult Error() {
            string title = "Error desconocido";
            string message = "Se ha presentado un error desconocido.";
            object temp = null;
            checkForDeadSession();
            if (!string.IsNullOrWhiteSpace(Session["UUID"].ToString())) {
                temp = Sessions.getFromSessionVariable(Session["UUID"].ToString(), "ERROR_TITLE");
                title = ((temp == null) ? title : temp.ToString());
                temp = Sessions.getFromSessionVariable(Session["UUID"].ToString(), "ERROR_MESSAGE");
                message = ((temp == null) ? message : temp.ToString());
            }
            ViewData.Add("ERROR_TITLE", title);
            ViewData.Add("ERROR_MESSAGE", message);
            return View();
        }

        private void checkForDeadSession() {
            if (Session["UUID"] == null) {
                Session.Clear();
                Sessions.cleanUpSessions();
                FormsAuthentication.SignOut();
            }
        }

    }
}
