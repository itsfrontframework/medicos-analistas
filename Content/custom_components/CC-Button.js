﻿
var Button = function (parent, params) {
    this.htmlElement = "";
    this.checked = false;

    Button.prototype.setSettings = function (params) {
        this.settings = $.extend({
            icon: "",
            label: "",
            tooltip: "",
            defaultStyle: "",
            checkedStyle: "",
            aditionalStyle: "",
            checkable: false,
            checked: false,
            disabled: false,
            hideText: true
        }, params);
    }

    Button.prototype.init = function (parent) {
        var Super = this;
        var hasIcon = false;

        var btnHtml = '<button type="button" data-toggle="tooltip" data-placement="bottom" title="' + this.settings.tooltip + '"';
        if (this.settings.disabled) {
            btnHtml += ' disabled="disabled"';
        }
        btnHtml += '></button>';

        this.htmlElement = $(btnHtml);
        this.htmlElement.addClass('btn');

        if (this.settings.icon != "") {
            this.htmlElement.append('<i class="fa ' + this.settings.icon + ' fa-fw"></i>');
            hasIcon = true;
        }

        if (this.settings.label != "") {
            //var text = (hasIcon) ? " " + this.settings.label : this.settings.label;
            var text = "";
            text = ((hasIcon && this.settings.hideText) ? '<span class="btn-mask">&nbsp;' + this.settings.label + '</span>' : this.settings.label);
            this.htmlElement.append(text);
        }

        if (this.settings.checked) {
            this.htmlElement.addClass(this.settings.checkedStyle);
        } else {
            this.htmlElement.addClass(this.settings.defaultStyle);
        }

        if (this.settings.aditionalStyle != "") {
            this.htmlElement.addClass(this.settings.aditionalStyle);
        }

        $(parent).append(this.htmlElement);
        $(this.htmlElement).click(function () {
            $(parent).find('.tooltip').tooltip('destroy');
            Super.chageStatus();
            Super.onClick();
            return false;
        });

        $(parent).tooltip({ selector: "[data-toggle=tooltip]" });
    }

    Button.prototype.chageStatus = function () {
        if (this.settings.checkable) {
            this.htmlElement.toggleClass(this.settings.checkedStyle);
            this.htmlElement.toggleClass(this.settings.defaultStyle);
        }
        this.checked = !this.checked;
    }

    Button.prototype.setDisabled = function (data) {
        if (data) {
            this.htmlElement.attr('disabled', 'disabled');
        } else {
            this.htmlElement.removeAttr('disabled');
        }
    }

    Button.prototype.onClick = function () {
    }

    this.setSettings(params);
    this.init(parent);
}