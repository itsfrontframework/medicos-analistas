﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace base_front.Controllers.MVC
{
    public class PermissionController : Controller
    {
        /// <summary>
        /// Lista de permisos disponibles
        /// </summary>
        /// <returns></returns>
        public PartialViewResult List()
        {
            return PartialView();
        }

        /// <summary>
        /// Permite agregar un permiso
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Add()
        {
            return PartialView();
        }

        /// <summary>
        /// Permite editar un permiso
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Edit()
        {
            return PartialView();
        }

    }
}
