﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>


<%--Titulo--%>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header-with-btns">
            <h1 class="title-responsive-text">Administración de Roles</h1>
            <div class="clearfix"></div>
        </div>
        <!-- /.page-header-with-btns -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-10 col-lg-offset-1" id="grid"></div>
    <!-- /.col-lg-10 col-lg-offset-1 -->
</div>
<!-- /.row -->

<script>
    var grid = new Table('#grid', {
        fields: {
            field1: {
                label: "ID",
                name: "ROLE_ID",
                visible: false
            },
            field2: {
                label: "Nombre",
                name: "NAME",
                sortable: true,
                visible: true
            },
            field3: {
                label: "Descripci&oacute;n",
                name: "DESCRIPTION",
                sortable: true,
                visible: true
            },
            field4: {
                label: "Habilitado",
                name: "ENABLED",
                sortable: true,
                visible: true
            },
            field5: {
                showlabel: false,
                label: "Editar",
                name: "NAME",
                visible: true,
                isButton: true,
                cssclass: 'btn-danger',
                icon: "fa-search",
                width: 70
            }
        },
        tablename: "roles_list",
        formatDate: false
    });


    var loadService = new XHRservice({
        url: '/api/Role/List',
        method: 'GET',
        responseType: 'XML'
    });

    grid.onLoadEnd = function () {
    };

    loadService.onReady = function (response) {
        grid.loadXML(response);
    };
    loadService.onError = function (response) {
    };

    grid.onButtonClick = function (btnName, rowInfo) {
        var campo = btnName.id.split("_");
        if (campo[0] === "NAME") {
            var data = {
                ROLE_ID: rowInfo["ROLE_ID"], NAME: rowInfo["NAME"],
                DESCRIPTION: rowInfo["DESCRIPTION"], ENABLED: rowInfo["ENABLED"]
            };
            screen.load('roles-edit', data);
        }
    };

    loadService.send();

    

</script>
