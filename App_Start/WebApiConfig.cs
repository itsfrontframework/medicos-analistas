﻿using System.Web.Http;

namespace base_front
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration configuration)
        {

            configuration.Routes.MapHttpRoute(
                name: "API ActionName",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            configuration.Routes.MapHttpRoute(
                name: "API Default",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}