﻿using BackendCore.Services;
using System;
using WSSDAL.Utilities;

namespace base_front.Services {
    /// <summary>
    /// Servicio que devuelve cualquier elemento a representar en la pantalla
    /// </summary>
    public class ScreenService : Service {

        /// <summary>
        /// Constructor de la clase. Genera una nueva instancia de <see cref="ScreenService"/>
        /// </summary>
        public ScreenService() : base() { }

        /// <summary>
        /// Obtiene la url de una pantalla a través de su nombre
        /// </summary>
        /// <returns>Regresa un <see cref="string"/> con la url de la pantalla</returns>
        public string getScreenURL(string screenName) {
            string url = null;
            DataFilter filter = new DataFilter() {
                { "CODE", screenName },
                { "ENABLED", "Y" }
            };

            try {
                url = select("GET_SCREEN_URL", filter).ToString();
            }
            catch (Exception) {
                url = null;
            }
            return url;
        }
    }
}