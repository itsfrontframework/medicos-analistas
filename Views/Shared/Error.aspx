﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Página de error">
    <meta name="author" content="ITS">
    <title>Error</title>
    <!-- Bootstrap Core CSS -->
    <link href="../../Content/sb-admin-2/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="../../Content/sb-admin-2/components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- Timeline CSS -->
    <link href="../../Content/sb-admin-2/dist/css/timeline.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../../Content/sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/dist/css/sbadmin2-sidebar-toggle.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/dist/css/custom.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="../../Content/sb-admin-2/components/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../../Content/sb-admin-2/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Error</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h1><i class="fa fa-warning"></i>Error: <span id="error-box-title"></span></h1>
                        </div>
                        <div class="panel-body">
                            <h3><span id="error-box-message">Ha ocurrido un error al procesar su solicitud.</span></h3>
                        </div>

                        <br />
                        <br />
                        <div class="panel-footer text-center">
                            <h2>
                                <a onclick="window.location.href='<%= ResolveUrl("~/") %>';">Regresar al sistema</a>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
</body>
</html>
