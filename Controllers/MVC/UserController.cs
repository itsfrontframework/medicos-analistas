﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace base_front.Controllers.MVC {
    public class UserController : Controller {
        /// <summary>
        /// Obtiene el listado de usuarios del sistema
        /// </summary>
        /// <returns></returns>
        public PartialViewResult List() {
            return PartialView();
        }

        /// <summary>
        /// Permite agregar un nuevo usuario
        /// </summary>
        /// <returns></returns>
        public PartialViewResult New() {
            return PartialView();
        }

        /// <summary>
        /// Permite editar la información de un usuario como administrador
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Edit() {
            return PartialView();
        }

        /// <summary>
        /// Permite ver el perfil de un usuario (como el usuario mismo)
        /// </summary>
        /// <returns></returns>
        public PartialViewResult Self() {
            return PartialView();
        }

    }
}
