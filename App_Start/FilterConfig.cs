﻿using base_front.Attributes;
using System.Web.Mvc;

namespace base_front {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LocalizationAttribute("es"));
            //filters.Add(new LocalizedAuthorizeAttribute("es-MX"));
        }
    }
}