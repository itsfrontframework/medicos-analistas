﻿
var RadioButton = function (parent, params) {
    this.controlName = 'radio_' + Math.ceil(Math.random() * 100000000);
    this.valueItem = null;
    this.radioElements = {};

    RadioButton.prototype.setSettings = function (params) {
        this.settings = $.extend({
            title: "",
            elements: "",
            disabled: false
        }, params);
    }

    RadioButton.prototype.init = function (parent) {
        var Super = this;
        var parentID = parent.substring(1);
        var parentNode = $(parent).parent();
        $(parent).remove();

        var container = $('<div></div>').addClass('form-group');
        var label = $('<label></label>');

        label.html(this.settings.title);
        container.append(label);

        for (var i = 0; i < this.settings.elements.length; i++) {
            var div = $('<div></div>').addClass('radio');
            var labelContainer = $('<label></label>');

            var inputHtml = '<input type="radio" name="' + this.controlName + '" value="' + this.settings.elements[i].value + '"';
            if (this.settings.disabled) {
                inputHtml += ' disabled="disabled"';
            }
            inputHtml += '> ';
            var input = $(inputHtml);

            this.radioElements[this.settings.elements[i].value] = input;

            labelContainer.append(input);
            labelContainer.append(' ' + this.settings.elements[i].label);
            div.append(labelContainer);

            container.append(div);
        }

        this.valueItem = $('<input type="text" style="display: none;" value="" id="' + parentID + '" />');
        
        if (this.settings.elements.length > 0) {
            this.setValue(this.settings.elements[0].value);
        }

        container.append(this.valueItem);
        parentNode.append(container);

        $("input[name='" + this.controlName + "']").click(function () {
            Super.valueItem.val(this.value);
            Super.onChange(this.value);
        });
    }

    RadioButton.prototype.setValue = function (value) {
        this.radioElements[value].prop("checked", true);
        this.valueItem.val(value);
    }

    RadioButton.prototype.setDisabled = function (data) {
        if (data) {
            $.each(this.radioElements, function (index, element) {
                element.attr('disabled', 'disabled');
            });
        } else {
            $.each(this.radioElements, function (index, element) {
                element.removeAttr('disabled');
            });
        }
    }

    RadioButton.prototype.onChange = function (value) {
    }

    this.setSettings(params);
    this.init(parent);
}