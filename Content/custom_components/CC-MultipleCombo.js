﻿
var MultipleCombo = function (parent, selectedValues, params) {
    this.cmbSelect = null;
    this.optionsData = null;

    MultipleCombo.prototype.init = function (parent, selectedValues) {
        this.cmbSelect = $(parent);

        if (this.settings.disabled) {
            this.cmbSelect.attr('disabled', 'disabled');
        }

        if (this.settings.jsonData == null) {
            this.cmbSelect.append("<option> Cargando... </option>");
            this.getData(selectedValues);
        } else {
            this.onReady(this.settings.jsonData, selectedValues);
        }
    }

    MultipleCombo.prototype.getData = function (selectedValues) {

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onReady(xmlhttp.responseText, selectedValues);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                $(this.cmbSelect).empty();
                Super.onError(xmlhttp.getResponseHeader("ErrorMsg"));
            }
        }

        if (this.settings.parameters != null) {
            this.settings.url += "?" + jQuery.param(this.settings.parameters);
        }

        xmlhttp.open("GET", this.settings.url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        //xmlhttp.setRequestHeader("USUARIO", this.settings.username);
        xmlhttp.send();

    }

    MultipleCombo.prototype.onReady = function (response, selectedValues) {
        var Super = this;
        var hasSelectedValues = ((typeof selectedValues != 'undefined') && selectedValues != null && selectedValues.length > 0);
        var jsonArray = JSON.parse(response);

        $(this.cmbSelect).empty();
        this.optionsData = [];

        if (this.settings.multiple) {
            $(this.cmbSelect).attr('multiple', 'multiple');
        }

        for (var idx in jsonArray) {
            var jsonObj = jsonArray[idx];

            this.optionsData.push({
                id: jsonObj[this.settings.valueFieldName],
                text: jsonObj[this.settings.labelFieldName],
                selected: (hasSelectedValues && selectedValues.indexOf(jsonObj[this.settings.valueFieldName]) >= 0)
            });
        }


        var comboOptions = {
            theme: "bootstrap",
            placeholder: this.settings.placeholder,
            data: this.optionsData,
            language: "es",
            tags: this.settings.tags
        };
        if (this.settings.templateResult != null) {
            comboOptions['templateResult'] = this.settings.templateResult;
            comboOptions['templateSelection'] = this.settings.templateResult;
        }
        if (this.settings.hideSearchBox) {
            comboOptions['minimumResultsForSearch'] = Infinity;
        }

        this.cmbSelect.select2(comboOptions);
        this.cmbSelect.on("change", function (e) {
            Super.onChange();
        });

        Super.onLoadEnd();
    }

    MultipleCombo.prototype.setData = function (selectedOptions) {
        //$(this.cmbSelect).val(null).trigger("change");
        $(this.cmbSelect).val(selectedOptions).trigger("change");
    }

    MultipleCombo.prototype.setDisabled = function (data) {
        if (data) {
            this.cmbSelect.attr('disabled', 'disabled');
        } else {
            this.cmbSelect.removeAttr('disabled');
        }
    }

    MultipleCombo.prototype.onLoadEnd = function (response) {
    };

    MultipleCombo.prototype.onChange = function () {
    };

    MultipleCombo.prototype.setSettings = function (params) {
        this.settings = $.extend({
            url: "",
            parameters: null,
            placeholder: "",
            labelFieldName: "TEXTO",
            valueFieldName: "VALOR",
            disabled: false,
            tags: false,
            jsonData: null,
            multiple: true,
            templateResult: null,
            hideSearchBox: false
        }, params);
    }

    this.setSettings(params);
    this.init(parent, selectedValues);
};