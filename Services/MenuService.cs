﻿using base_front.Models;
using System.Collections.Generic;
using BackendCore.Services;
using WSSDAL.Utilities;

namespace base_front.Services {
    /// <summary>
    /// Servicio que se encarga de manipular los elementos del menú
    /// </summary>
    public class MenuService : Service {

        /// <summary>
        /// Constructor de la clase. Genera una nueva instancia de <see cref="MenuService"/>
        /// </summary>
        public MenuService() : base() { }

        /// <summary>
        /// Obtiene el menú para el filtro especificado
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="order"></param>
        /// <returns>Se obtiene un </returns>
        public IEnumerable<MenuItem> getMenu(DataFilter filter) {
            return select<MenuItem>("GET_MENU", filter);
        }
    }
}