﻿
<!DOCTYPE html>

<html>
<head runat="server">
    <link rel="apple-touch-icon" sizes="57x57" href="../../Content/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../../Content/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../../Content/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../../Content/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../../Content/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../../Content/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../../Content/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../../Content/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../../Content/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../../Content/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../Content/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../../Content/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../Content/favicon/favicon-16x16.png">
    <link rel="manifest" href="../../Content/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../../Content/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Página principal">
    <meta name="author" content="ITS">

    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <title>CienciaMed - Médicos Analistas</title>

    <link href="../../Content/sb-admin-2/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/dist/css/sbadmin2-sidebar-toggle.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/dist/css/custom.css" rel="stylesheet">
    <link href="../../Content/sb-admin-2/components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
            </div>

            <div class="navbar-header">
            </div>

            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-header navbar-right">
            </div>

        </nav>
        <div id="page-wrapper">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="text-center">
                        <img src="../../Content/images/logo.png" style="max-height: 200px; margin: auto;" class="img-responsive" id="logo" alt="<%=BackendCore.Environment.Parameters.getValueOf("APPLICATION_TITLE")%>">
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="text-center">
                        <h1>Sitio en mantenimiento</h1>
                        <i class="fa fa-wrench fa-5x"></i>
                        <p>
                            La plataforma estará disponible a partir de enero de 2017 en <a href="http://cienciamed.net">http://cienciamed.net</a> 
                        </p>
                        
                    </div>
                </div>
            </div>

        </div>

    </div>
    

    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/jquery/dist/jquery.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/bootstrap/dist/js/bootstrap.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/components/metisMenu/dist/metisMenu.min.js") %>'></script>
    <script src='<%= ResolveUrl("~/Content/sb-admin-2/dist/js/sb-admin-2.js") %>'></script>


</body>

</html>

