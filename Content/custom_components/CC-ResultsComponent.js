﻿var ResultsComponent = function (parent, params, sections) {
    this.generalContainer = null;
    this.mainContainer = null;
    this.xmlDataSections = null;
    this.parentContainer = parent;
    this.objData = {};
    this.snpEditable = true;
    this.snpConfirmation = false;
    this.notOpeneableModules = [];
    this.loadedSections = 0;
    this.loadedSectionsEnd = false;
    this.integracionContainer = null;
    this.reportDiseases = null;
    this.monogenicCount = 0;
    this.moduleInterpretationsBodyContent = null;

    // Build the structure of the component
    ResultsComponent.prototype.init = function (sections) {
        /// <summary>Build each module and call the request function for get the SNP data of each section</summary>
        /// <param name="sections" type="object">Sections of SNP to load</param>

        var Super = this;
        this.moduleContainer = $('<div class="panel-group" id="generalAccordion" role="tablist" aria-multiselectable="true"></div>');

        // Build modules and append to generalAccordion
        if (this.settings.modulesVisible.SNP) {
            this.buildModuleCollapseSNP(sections);
        }
        if (this.settings.modulesVisible.Interpretation) {
            this.buildModuleCollapseInterpretation();
        }
        if (this.settings.modulesVisible.Signature) {
            this.buildModuleCollapseSignature();
        }
        if (this.settings.modulesVisible.Specialist) {
            this.buildModuleCollapseSpecialist();
        }
        $(this.parentContainer).html('');
        $(this.parentContainer).append(this.moduleContainer);

        // Asign start status to each module
        this.assignModuleStatus('SNP');
        this.assignModuleStatus('Interpretation');
        this.assignModuleStatus('Signature');
        this.assignModuleStatus('Specialist');

        // If there is just one module builded, open it
        if (this.moduleContainer.find('.generalAcordionPanel').length == 1) {
            this.moduleContainer.find('.generalAcordionPanel').children('.panel-collapse').addClass('in');
        }

        // Disable module opening
        this.moduleContainer.on('click', '.moduleCollapseControl', function (e) {
            var module = $(this).attr('aria-controls');
            if (Super.notOpeneableModules.indexOf(module) >= 0) {
                e.stopPropagation();
            }
        });

        // Load each SNP section
        if (this.settings.modulesVisible.SNP) {
            for (var idx in this.objData) {
                this.getTableData(idx);
            }
        }

        // Add Take case button
        this.buildTakeCaseButton();
    }
    ResultsComponent.prototype.buildModuleCollapseSNP = function (sections) {
        /// <summary>Buil the SNP module DOM struncture and Sections container</summary>
        /// <param name="sections" type="object">Sections of SNP to load</param>

        var modulePanel = $('<div></div>').addClass('panel generalAcordionPanel');
        var moduleHeading = $('<div role="tab" id="headingModuleCollapseSNP"></div>').addClass('panel-heading');
        var moduleH4 = $('<h4></h4>').addClass('panel-title');
        var moduleSpan = $('<span role="button" data-toggle="collapse" data-parent="#generalAccordion" href="#collapseModuleSNP" aria-expanded="false" aria-controls="collapseModuleSNP">').addClass('collapsed moduleCollapseControl');
        var modulCheckBox = $('<input type="checkbox" data-id="chkModuleSNP" class="moduleCheckbox" disabled />').css('margin-right', '10px');
        var moduleName = $('<span>').addClass('secction-module-color').append("SNPs");

        var dataDate = this.getRecordDate();
        if (dataDate != '') {
            dataDate = 'Análisis Genético del ' + dataDate;
            moduleName.append('<span style="font-weight: 100;margin-left: 30px;">' + dataDate + '</span>');
        }

        moduleSpan.append(modulCheckBox);
        moduleSpan.append(moduleName);
        moduleH4.append(moduleSpan);
        moduleHeading.append(moduleH4);

        var moduleBody = $('<div id="collapseModuleSNP" data-loaded="0" role="tabpanel" aria-labelledby="headingModuleCollapseSNP"></div>').addClass('panel-collapse collapse');
        this.buildSNPSection(sections);
        var moduleBodyContent = $('<div></div>').addClass('panel-body');

        moduleBodyContent.append(this.mainContainer);
        moduleBody.append(moduleBodyContent);
        modulePanel.append(moduleHeading);
        modulePanel.append(moduleBody);

        this.moduleContainer.append(modulePanel);
    }
    ResultsComponent.prototype.buildModuleCollapseInterpretation = function () {
        var Super = this;
        var modulePanel = $('<div></div>').addClass('panel generalAcordionPanel');
        var moduleHeading = $('<div role="tab" id="headingModuleCollapseInterpretation"></div>').addClass('panel-heading');
        var moduleH4 = $('<h4></h4>').addClass('panel-title');
        var moduleSpan = $('<span role="button" data-toggle="collapse" data-parent="#generalAccordion" href="#collapseModuleInterpretation" aria-expanded="false" aria-controls="collapseModuleInterpretation">').addClass('collapsed moduleCollapseControl');
        var modulCheckBox = $('<input type="checkbox" data-id="chkModuleInterpretation" class="moduleCheckbox" disabled />').css('margin-right', '10px');
        var moduleName = $('<span>').addClass('secction-module-color').append("Interpretación teórico clínica");
        moduleSpan.append(modulCheckBox);
        moduleSpan.append(moduleName);
        moduleH4.append(moduleSpan);
        moduleHeading.append(moduleH4);
        var moduleBody = $('<div id="collapseModuleInterpretation" data-loaded="0" role="tabpanel" aria-labelledby="headingModuleCollapseInterpretation"></div>').addClass('panel-collapse collapse');
        this.moduleInterpretationsBodyContent = $('<div></div>').addClass('panel-body');

        var divButtonBottom = $('<div class="interpretationActionButtonsContainer"></div>').addClass('text-right');
        var divButtonTop = divButtonBottom.clone();
        var buttonSaveChangesBottom = $('<button class="btnSaveIntegracionChanges"></button>').addClass('btn btn-primary btn-xs innerModuleControl');
        var buttonSaveChangesTop = buttonSaveChangesBottom.clone();
        var buttonPreloadInterpretationBottom = $('<button class="btnPreloadInterpretation"></button>').addClass('btn btn-info btn-xs innerModuleControl').css('margin-right', '15px');
        var buttonPreloadInterpretationTop = buttonPreloadInterpretationBottom.clone();

        var interData = this.settings.interpretationStartingData;
        this.reportDiseases = {};

        // Build interpretation textbox foreach selected disease
        this.integracionContainer = $('<div></div>');
        for (var qdx in interData) {
            this.buildInterpretationDisease(interData[qdx]);
        }

        if (interData.length == 0) {
            divButtonTop.hide();
            divButtonBottom.hide();
        }

        this.moduleInterpretationsBodyContent.
            append(
                divButtonTop.
                    append(buttonPreloadInterpretationTop.html('Pre-llenar')).
                    append(buttonSaveChangesTop.html('Guardar cambios'))).
            append(this.integracionContainer).
            append(
                divButtonBottom.
                    append(buttonPreloadInterpretationBottom.html('Pre-llenar')).
                    append(buttonSaveChangesBottom.html('Guardar cambios'))
        );

        // Save current values
        buttonSaveChangesBottom.click(function () {
            Super.updateAllInterpretations();
        });
        buttonSaveChangesTop.click(function () {
            Super.updateAllInterpretations();
        });

        // Preload content
        buttonPreloadInterpretationBottom.click(function () {
            Super.preloadInterpretationContent();
        });
        buttonPreloadInterpretationTop.click(function () {
            Super.preloadInterpretationContent();
        });

        moduleBody.append(this.moduleInterpretationsBodyContent);
        modulePanel.append(moduleHeading);
        modulePanel.append(moduleBody);
        this.moduleContainer.append(modulePanel);
    }
    ResultsComponent.prototype.buildModuleCollapseSignature = function () {
        /// <summary>Build the Signature module DOM structure and define its entire behavior</summary>

        var Super = this;
        var modulePanel = $('<div></div>').addClass('panel generalAcordionPanel');
        var moduleHeading = $('<div role="tab" id="headingModuleCollapseSignature"></div>').addClass('panel-heading');
        var moduleH4 = $('<h4></h4>').addClass('panel-title');
        var moduleSpan = $('<span role="button" data-toggle="collapse" data-parent="#generalAccordion" href="#collapseModuleSignature" aria-expanded="false" aria-controls="collapseModuleSignature">').addClass('collapsed moduleCollapseControl');
        var modulCheckBox = $('<input type="checkbox" data-id="chkModuleSignature" class="moduleCheckbox" disabled />').css('margin-right', '10px');
        var moduleName = $('<span>').addClass('secction-module-color').append("Firma del Reporte");
        moduleSpan.append(modulCheckBox);
        moduleSpan.append(moduleName);
        moduleH4.append(moduleSpan);
        moduleHeading.append(moduleH4);

        var moduleBody = $('<div id="collapseModuleSignature" data-loaded="0" role="tabpanel" aria-labelledby="headingModuleCollapseSignature"></div>').addClass('panel-collapse collapse');
        var moduleBodyContent = $('<div></div>').addClass('panel-body');

        var rowDiv = $('<div></div>').addClass('row');
        var simpleDiv = $('<div></div>');
        var imgClick = $('<a href="MedicosAnalistas/Report/GetInterpretationReport/' + this.settings.barCode + '" target="_blank"></a>');
        var pdfImg = $('<img src="../../Content/images/PDF-download-icon.png" class="img-responsive" alt="Descargar PDF" style="max-height: 150px;margin: auto;">');
        var btnSign = $('<button id="btnSignatureSing">Firmar</button>').addClass('btn btn-success innerModuleControl').css('margin', 'auto');
        var content = rowDiv.clone().attr('id', 'signatureMainpanel');
        var rowAbove = rowDiv.clone();
        var rowBelow = rowDiv.clone().css('margin-top', '15px');

        imgClick.append(pdfImg);
        rowAbove.append(simpleDiv.clone().addClass('col-sm-8 signatureExplanationDiv').html(this.settings.signatureExplanationText));
        rowAbove.append(simpleDiv.clone().addClass('col-sm-4 text-center').append(btnSign));

        var signatureImage = $('<img src="' + this.settings.signatureImagePath + this.settings.user.SIGNATURE + '" />');
        var descriptionDiv = simpleDiv.clone().addClass('col-sm-8');
        var imageDiv = simpleDiv.clone().addClass('col-sm-4');

        var userNameParagraph = $('<p></p>').css({ 'margin-bottom': '0px', 'font-weight': 'bold' }).html(this.settings.user.NAME);
        var userPositionParagraph = $('<p></p>').css({ 'margin-bottom': '0px', 'font-style': 'italic' }).html(this.settings.user.POSITION);
        var userEducationParagraph = $('<div></div>').css('white-space', 'pre-wrap').html(this.settings.user.EDUCATION);
        descriptionDiv.append(userNameParagraph);
        descriptionDiv.append(userPositionParagraph);
        descriptionDiv.append(userEducationParagraph);

        if (this.settings.user.SIGNATURE.trim().length > 0) {
            signatureImage.css('width', '200px');
        }

        imageDiv.append(signatureImage);
        rowBelow.append(descriptionDiv);
        rowBelow.append(imageDiv);

        content.append(simpleDiv.clone().addClass('col-sm-4').append(imgClick));
        content.append(simpleDiv.clone().addClass('col-sm-8').append(rowAbove).append(rowBelow));
        moduleBodyContent.append(content);

        btnSign.click(function () {
            btnSign.append('<i style="margin-left: 5px;" class="fa fa-cog fa-spin btnSignatureLoadingCog"></i>');
            if (Super.monogenicCount > 0) {
                var monogenicConfirm = new Confirm({ confirm: true });
                monogenicConfirm.setTitle('Enfermedad monogénica seleccionada');
                monogenicConfirm.setBody('Se ha seleccionado una enfermedad monogénica que se incluirá en el reporte, ¿es correcta está selección?.');
                monogenicConfirm.show();

                monogenicConfirm.onAcceptClick = function () {
                    Super.onSignClick();
                };
                monogenicConfirm.onCancelClick = function () {
                    $('.btnSignatureLoadingCog').remove();
                };

            } else {
                Super.onSignClick();
            }
        });

        moduleBody.append(moduleBodyContent);
        modulePanel.append(moduleHeading);
        modulePanel.append(moduleBody);
        this.moduleContainer.append(modulePanel);
    }
    ResultsComponent.prototype.buildModuleCollapseSpecialist = function () {
        /// <summary>Build the Specialist module DOM structure and define its entire behavior</summary>

        var Super = this;
        var modulePanel = $('<div></div>').addClass('panel generalAcordionPanel');
        var moduleHeading = $('<div role="tab" id="headingModuleCollapseSpecialist"></div>').addClass('panel-heading');
        var moduleH4 = $('<h4></h4>').addClass('panel-title');
        var moduleSpan = $('<span role="button" data-toggle="collapse" data-parent="#generalAccordion" href="#collapseModuleSpecialist" aria-expanded="false" aria-controls="collapseModuleSpecialist">').addClass('collapsed moduleCollapseControl');
        var modulCheckBox = $('<input type="checkbox" data-id="chkModuleSpecialist" class="moduleCheckbox" disabled />').css('margin-right', '10px');
        var moduleName = $('<span>').addClass('secction-module-color').append("Asignación de Especialidad Recomendada");
        moduleSpan.append(modulCheckBox);
        moduleSpan.append(moduleName);
        moduleH4.append(moduleSpan);
        moduleHeading.append(moduleH4);

        var moduleBody = $('<div id="collapseModuleSpecialist" data-loaded="0" role="tabpanel" aria-labelledby="headingModuleCollapseSpecialist"></div>').addClass('panel-collapse collapse');
        var moduleBodyContent = $('<div></div>').addClass('panel-body');

        var specialtiesSelectContainer = $('<div></div>').addClass('form-group');
        var specialtiesSelect = $('<select></select>').addClass('form-control innerModuleControl');
        specialtiesSelectContainer.append(specialtiesSelect);

        this.ajax('/MedicosAnalistas/api/Medicos/GetSpecialties',
                    'GET', 'JSON', null,
                    function (response) {
                        for (idx in response) {
                            specialtiesSelect.append('<option value="' + response[idx].SPECIALTY_ID + '">' + response[idx].SPECIALTY_NAME + '</option>');
                        }
                    }, null, null, null);

        var rowDiv = $('<div></div>').addClass('row').css('margin-bottom', '15px');
        var simpleDiv = $('<div></div>');
        var content = simpleDiv.clone();
        var explanationText = this.settings.specialistExplanationText;
        var asignSpecilistButton = $('<button> Terminar, Archivar y Enviar </button>').addClass('btn btn-success innerModuleControl');
        content.append(rowDiv.clone().append(simpleDiv.clone().addClass('col-sm-12').append(explanationText)));
        content.append(rowDiv.clone().append(simpleDiv.clone().addClass('col-sm-8 col-sm-offset-4').append(specialtiesSelectContainer)));
        content.append(rowDiv.clone().append(simpleDiv.clone().addClass('col-sm-12 text-right').append(asignSpecilistButton)));

        asignSpecilistButton.click(function () {
            Super.ajax('/MedicosAnalistas/api/Medicos/SaveSpecialtyAssignment',
                        'POST', 'JSON', { BARCODE: Super.settings.barCode, SPECIALTY_ID: specialtiesSelect.val() },
                        function (response) {
                            if (response === true) {
                                specialtiesSelect.prop('disabled', true);
                                asignSpecilistButton.parent().removeClass('text-right').addClass('text-center');
                                asignSpecilistButton.replaceWith($('<div></div>').addClass('alert alert-success').html('Proceso terminado'));
                            }
                        }, null, null, null);
        });

        moduleBodyContent.append(content);
        moduleBody.append(moduleBodyContent);
        modulePanel.append(moduleHeading);
        modulePanel.append(moduleBody);
        this.moduleContainer.append(modulePanel);
    }
    ResultsComponent.prototype.buildTakeCaseButton = function () {
        /// <summary>Build the Take case button</summary>

        var Super = this;
        var takeButton = $('<button></button>').addClass('btn btn-xs').attr('id', 'btnTakeTask').css('margin-right', '40px');
        var married = (this.settings.modulesData.MARRIED.trim() == 'Y');
        var marriedWithMe = (this.settings.modulesData.MARRIED_WITH_ME.trim() == 'Y');
        var inMyRole = (this.settings.modulesData.IN_MY_ROLE.trim() == 'Y');

        var showButton = false;
        var someModuleVisible = this.settings.modulesVisible.SNP || this.settings.modulesVisible.Interpretation || this.settings.modulesVisible.Specialist;
        var someModuleEditable = true;



        if (married) {
            if (marriedWithMe) {
                takeButton.addClass('btn-danger').html('Abandonar el caso');
                $('#btn2daOpinio').show();
                showButton = true;
            } else {
                if (this.settings.caseControlPermission) {
                    takeButton.addClass('btn-danger').html('Regresar al pool');
                    $('#btn2daOpinio').show();
                    showButton = true;
                }
            }
        } else {
            if (inMyRole && someModuleVisible && someModuleEditable) {
                takeButton.addClass('btn-primary').html('Tomar el caso');
                $('#btn2daOpinio').show();
                showButton = true;
            }
        }

        if (showButton) {
            if ($('#btnTakeTask').length) {
                $('#btn2daOpinio').show();
            } else {
                $('#navbarTitle2').find('span').prepend(takeButton);
            }

            takeButton.click(function () {
                Super.takeCase($(this));
            });
        }
    }
    ResultsComponent.prototype.buildSNPSection = function (sections) {
        /// <summary>Buil the structure of SNP Sections for all disease types and events declaration</summary>
        /// <param name="sections" type="object">Sections markers and structures data</param>

        var Super = this;
        var orderArray = [];
        this.mainContainer = $('<div class="panel-group" id="medicosAccordion" role="tablist" aria-multiselectable="true"></div>');

        for (var idx in sections) {
            var currentNode = sections[idx];
            var currentId = currentNode.SECTIONID;
            orderArray.push(currentId);

            this.objData[currentId] = {};
            this.objData[currentId]['name'] = currentNode.NAME;
            this.objData[currentId]['markersCount'] = currentNode.UPPERMARKERS;
            this.objData[currentId]['reportCount'] = currentNode.REPORTSELECTION;
            this.objData[currentId]['comentsCount'] = currentNode.COMMENTSCOUNT;
            this.objData[currentId]['columns'] = JSON.parse(currentNode.COLUMNS);
        }

        // Sections build =============================================================
        var idx;
        for (var i in orderArray) {
            idx = orderArray[i];
            var htmlSection = $('<div></div>').addClass('panel');
            var htmlHeading = $('<div role="tab" id="heading' + idx + '"></div>').addClass('panel-heading');
            var htmlH4 = $('<h4></h4>').addClass('panel-title');
            var htmlSpan = $('<span role="button" data-toggle="collapse" data-parent="#medicosAccordion" href="#collapse' + idx + '" aria-expanded="false" data-section-id="' + idx + '" aria-controls="collapse' + idx + '">').addClass('collapsed section-span-title');
            var htmlName = $('<span>').addClass('secction-header-color').append(this.objData[idx].name);
            htmlSpan.append(htmlName);

            // Set color to markers
            var currMarkersCount = this.objData[idx].markersCount;
            var currReportCount = this.objData[idx].reportCount;
            var currComentsCount = this.objData[idx].comentsCount;
            var markersCssClass = 'btn-gray';
            var reportCssClass = 'btn-gray-text';
            var comentsCssClass = 'btn-gray-text';
            if ($.isNumeric(currMarkersCount) && parseInt(currMarkersCount) > 0) {
                markersCssClass = 'btn-red';
            }
            if ($.isNumeric(currReportCount) && parseInt(currReportCount) > 0) {
                reportCssClass = 'btn-green';
            }
            if ($.isNumeric(currComentsCount) && parseInt(currComentsCount) > 0) {
                comentsCssClass = 'btn-yellow';
            }

            htmlSpan.append('&nbsp;&nbsp;<span type="button" class="btn ' + markersCssClass + ' btn-circle markersCount"> ' + this.objData[idx].markersCount + ' </span>');
            htmlSpan.append('&nbsp;&nbsp;<span type="button" class="btn ' + reportCssClass + ' btn-circle reportCount"> ' + this.objData[idx].reportCount + 'R </span>');
            htmlSpan.append('&nbsp;&nbsp;<span type="button" class="btn ' + comentsCssClass + ' btn-circle commentsCount"> ' + this.objData[idx].comentsCount + 'C </span>');

            htmlH4.append(htmlSpan);
            htmlHeading.append(htmlH4);
            var htmlBody = $('<div id="collapse' + idx + '" data-loaded="0" data-section-id="' + idx + '" role="tabpanel" aria-labelledby="heading' + idx + '"></div>').addClass('panel-collapse collapse');
            var htmlBodyContent = $('<div></div>').addClass('panel-body');
            htmlBodyContent.append('<div style="text-align: center;"> <h5 class="text-danger">Cargando ...</h5> <i class="fa fa-cog fa-spin fa-2x fa-fw"></i> </div>');
            htmlBody.append(htmlBodyContent);

            htmlSection.append(htmlHeading);
            htmlSection.append(htmlBody);
            this.mainContainer.append(htmlSection);
        }

        $(this.mainContainer).on('click', '.reportar-symbols-exclamation', function () {
            Super.toggleComments($(this));
        });

        $(this.mainContainer).on('click', '.reportar-symbols-plus', function () {
            if (!Super.snpEditable) return false;
            Super.toggleAddComments($(this));
        });

        $(this.mainContainer).on('click', '.btn-add-comment', function () {
            if (!Super.snpEditable) return false;
            Super.onAddCommentClick($(this));
        });

        $(this.mainContainer).on('click', '.btn-edit-comment', function () {
            if (!Super.snpEditable) return false;
            Super.onSaveEditCommentClick($(this));
        });

        $(this.mainContainer).on('click', '.btn-cancel-comment', function () {
            Super.onCancelCommentClick($(this));
        });

        $(this.mainContainer).on('click', '.btn-cancel-edit-comment', function () {
            $(this).closest('tr').prev().show();
            $(this).closest('tr').remove();
        });

        $(this.mainContainer).on('click', '.informeLink', function () {
            var id = $(this).data('element');
            var snpID = $(this).data('snp');
            Super.getTechnicalReportData(id, snpID);

            $('#informModal').find('.modal-body').html('<div style="text-align: center;"> <h5 class="text-danger">&nbsp;Cargando ...</h5> <i class="fa fa-cog fa-spin fa-2x fa-fw"></i> </div>');
            $('#informModal').modal('show');
        });

        $(this.mainContainer).on('change', '.cboRowSelect', function () {
            if (!Super.snpEditable) return false;
            if (!Super.snpConfirmation) {
                Super.snpConfirmation = confirm("¿Desea editar los SNPs seleccionados?");
            }
            if (!Super.snpConfirmation) {
                $(this).prop("checked", !$(this).is(':checked'));
                return false;
            }

            $(this).closest('tr').toggleClass('selected-row');
            var id = $(this).parent().find('.reportar-symbols-plus').data('element');
            var snpID = $(this).parent().find('.reportar-symbols-plus').data('snp');
            var type = $(this).parent().find('.reportar-symbols-plus').data('type');
            var checked = ($(this).is(':checked')) ? 'Y' : 'N';

            Super.changeSelectedStatus(type, id, snpID, checked, $(this));
        });

        $(this.mainContainer).on('click', '.order-button', function () {
            $('#reportCommentAddBox').remove();
            var asc = false;
            if ($(this).find('.fa').hasClass('fa-sort') || $(this).find('.fa').hasClass('fa-caret-down')) {
                asc = true;
            }
            $(this).closest('tr').find('.fa').removeClass('fa-sort');
            $(this).closest('tr').find('.fa').removeClass('fa-caret-down');
            $(this).closest('tr').find('.fa').removeClass('fa-caret-up');
            $(this).closest('tr').find('.fa').addClass('fa-sort');

            $(this).find('.fa').removeClass('fa-sort');
            if (asc) {
                $(this).find('.fa').addClass('fa-caret-up');
            } else {
                $(this).find('.fa').addClass('fa-caret-down');
            }

            Super.sortTable($(this).closest('table'), $(this).data('rowcolumnname'), asc);
        });

        $(this.mainContainer).on('click', '.delete-comment-button', function () {
            if (!Super.snpEditable) return false;
            var uuid = $(this).parent().data('uuid');
            var rowElement = $(this).closest('tr');
            Super.deleteComment(uuid, rowElement);
        });

        $(this.mainContainer).on('click', '.edit-comment-button', function () {
            if (!Super.snpEditable) return false;
            var rowElement = $(this).closest('tr');
            var section = rowElement.closest('.panel-collapse').data('section-id');
            var element = rowElement.data('element');

            var li = $(this).parent().clone();
            li.find('span').remove();
            var author = li.find('strong').html();
            li.find('strong').remove();
            var text = li.html();
            var editElement = {
                uuid: $(this).parent().data('uuid'),
                author: author,
                text: text,
                isForReport: $(this).parent().data('forreport')
            };

            rowElement.hide();
            Super.addCommentContents(section, element, rowElement, editElement);
        });
    }

    // Signature functions
    ResultsComponent.prototype.onSignClick = function () {
        /// <summary>Make the request for mark the process step as done</summary>

        var Super = this;
        $('.signatureSignFeedback').remove();

        Super.changeStateSign('Y');

        this.ajax('/MedicosAnalistas/api/Medicos/SaveReportSignature',
            'POST', 'JSON',
            { BARCODE: this.settings.barCode },
            function (response) {
                $('.btnSignatureLoadingCog').remove();
                if (response === true) {
                    $('#signatureMainpanel').after($('<div></div>').addClass('alert alert-success text-center signatureSignFeedback').
                                        html('Reporte firmado').css('margin-top', '10px'));
                    Super.changeStateAfterSign();
                } else {
                    Super.changeStateSign('N');
                    var message = "No fue posible firmar el reporte, verifique sus permisos";
                    $('#signatureMainpanel').after($('<div></div>').addClass('alert alert-danger text-center signatureSignFeedback').
                                        html(message).css('margin-top', '10px'));
                }

                setTimeout(function () { $('.signatureSignFeedback').remove(); }, 4000);

            }, null,
            function (response) {
                $('.btnSignatureLoadingCog').remove();
                Super.changeStateSign('N');
            }, null);
    }
    ResultsComponent.prototype.changeStateAfterSign = function () {
        /// <summary>Update module status after signature</summary>
        for (var mod in this.settings.modulesEditable) {
            if (this.settings.modulesEditable[mod]) {
                this.settings.modulesData.DETAIL[mod].STATUS = 'Y';
            }
            this.settings.modulesEditable[mod] = false;
        }

        this.ajax('/MedicosAnalistas/api/Medicos/SaveModuleChangeStatus',
            'POST', 'JSON',
            {
                //MODULES_DATA: this.settings.modulesData,
                DETAIL: [
                    {
                        MODULE: 'Interpretation',
                        SHOW: this.settings.modulesData.DETAIL['Interpretation'].SHOW,
                        STATUS: this.settings.modulesData.DETAIL['Interpretation'].STATUS
                    },
                    {
                        MODULE: 'Signature',
                        SHOW: this.settings.modulesData.DETAIL['Signature'].SHOW,
                        STATUS: this.settings.modulesData.DETAIL['Signature'].STATUS
                    },
                    {
                        MODULE: 'SNP',
                        SHOW: this.settings.modulesData.DETAIL['SNP'].SHOW,
                        STATUS: this.settings.modulesData.DETAIL['SNP'].STATUS
                    },
                    {
                        MODULE: 'Specialist',
                        SHOW: this.settings.modulesData.DETAIL['Specialist'].SHOW,
                        STATUS: this.settings.modulesData.DETAIL['Specialist'].STATUS
                    }
                ],
                HUSBAND: this.settings.modulesData.HUSBAND,
                IN_MY_ROLE: this.settings.modulesData.IN_MY_ROLE,
                MARRIED: this.settings.modulesData.MARRIED,
                MARRIED_WITH_ME: this.settings.modulesData.MARRIED_WITH_ME,
                TASK_ID: this.settings.modulesData.TASK_ID,
                TASK_ROLE: this.settings.modulesData.TASK_ROLE,
                TASK_UUID: this.settings.modulesData.TASK_UUID,
                WORKFLOW_ID: this.settings.modulesData.WORKFLOW_ID,
                WORKFLOW_UUID: this.settings.modulesData.WORKFLOW_UUID
            },
            null, null,
            null, null);


        this.assignModuleStatus('SNP');
        this.assignModuleStatus('Interpretation');
        this.assignModuleStatus('Signature');
        this.assignModuleStatus('Specialist');


        // Hide leave case button
        $('#btnTakeTask').hide();


        // Update process on external component
        this.refreshProcessComponent();
    }

    ResultsComponent.prototype.changeStateSign = function (status) {
        /// <summary>Update module status after signature</summary>
        if (this.settings.modulesEditable['Interpretation']) {
            this.settings.modulesData.DETAIL['Interpretation'].STATUS = status;
        }

        if (this.settings.modulesEditable['SNP']) {
            this.settings.modulesData.DETAIL['SNP'].STATUS = status;
        }

        this.ajax('/MedicosAnalistas/api/Medicos/SaveModuleChangeStatus',
            'POST', 'JSON',
            {
                //MODULES_DATA: this.settings.modulesData,
                DETAIL: [
                    {
                        MODULE: 'Interpretation',
                        SHOW: this.settings.modulesData.DETAIL['Interpretation'].SHOW,
                        STATUS: this.settings.modulesData.DETAIL['Interpretation'].STATUS
                    },
                    {
                        MODULE: 'SNP',
                        SHOW: this.settings.modulesData.DETAIL['SNP'].SHOW,
                        STATUS: this.settings.modulesData.DETAIL['SNP'].STATUS
                    }
                ],
                HUSBAND: this.settings.modulesData.HUSBAND,
                IN_MY_ROLE: this.settings.modulesData.IN_MY_ROLE,
                MARRIED: this.settings.modulesData.MARRIED,
                MARRIED_WITH_ME: this.settings.modulesData.MARRIED_WITH_ME,
                TASK_ID: this.settings.modulesData.TASK_ID,
                TASK_ROLE: this.settings.modulesData.TASK_ROLE,
                TASK_UUID: this.settings.modulesData.TASK_UUID,
                WORKFLOW_ID: this.settings.modulesData.WORKFLOW_ID,
                WORKFLOW_UUID: this.settings.modulesData.WORKFLOW_UUID
            },
            null, null,
            null, null);
    }

    // Interpretations functions
    ResultsComponent.prototype.buildInterpretationDisease = function (data) {
        /// <summary>Build an interpretation textbox and checkbox</summary>
        /// <param name="data" type="object">Disease data used as reference in the components</param>

        var Super = this;
        // Define HTML elements
        var divFormGroup = $('<div></div>').addClass('form-group');
        var divFormGroupLabel = $('<label></label>');
        var divFormGroupCheckbox = $('<input type="checkbox" />').addClass('cboInterpretationDone innerModuleControl').css('margin-right', '10px');
        var divFormGroupText = $('<textarea rows="6"></textarea>').addClass('form-control innerModuleControl');

        // Assign attributes and values
        divFormGroup.attr('id', 'inteContain_' + data.ID);
        divFormGroupLabel.html(data.NAME);
        divFormGroupCheckbox.attr('id', 'cboIntegracion_' + data.ID).prop('checked', (data.FINISHED == 'Y'));
        divFormGroupText.attr('id', 'txtIntegracion_' + data.ID).val(data.TEXT);

        // Build persistent array
        this.reportDiseases[data.ID + ''] = data.ELEMENTS.split(',');

        // Build DOM elements
        divFormGroup.append(divFormGroupCheckbox);
        divFormGroup.append(divFormGroupLabel);
        divFormGroup.append(divFormGroupText);
        this.integracionContainer.append(divFormGroup);

        // Disabled and green background if finished
        if (data.FINISHED == 'Y') {
            divFormGroupText.css('background-color', '#e9f6dc');
            divFormGroupText.prop('disabled', true);
        }

        divFormGroupCheckbox.click(function () {
            Super.onInterpretationCheckboxChange($(this));
        });
    }
    ResultsComponent.prototype.onInterpretationCheckboxChange = function (element) {
        /// <summary>Change the interpretation status and make the request for update</summary>
        /// <param name="element" type="object">DOM element of the invoking checkbox</param>

        var Super = this;
        var isChecked = element.is(':checked');
        var diseaseID = element.attr('id').substring(15);
        var interpretationText = $('#txtIntegracion_' + diseaseID).val();

        if (isChecked && interpretationText.trim() == '') {
            element.prop('checked', false);
            return false;
        }

        if (isChecked) {
            $('#txtIntegracion_' + diseaseID).css('background-color', '#e9f6dc');
            $('#txtIntegracion_' + diseaseID).prop('disabled', true);
        } else {
            $('#txtIntegracion_' + diseaseID).css('background-color', '');
            $('#txtIntegracion_' + diseaseID).prop('disabled', false);
        }

        this.updateSimpleInterpretatio(diseaseID);
        this.onSnpIntCheckChange();
    }
    ResultsComponent.prototype.refreshReportDiseases = function (type, id, snpID, isChecked, element) {
        /// <summary>Refresh report diseases for Interpretation, called when a SNP is checked/unchecked</summary>
        /// <param name="type" type="string">Type of result item (Complex_Diseases, traits, etc)</param>
        /// <param name="id" type="int">Result ID owner of the SNP element</param>
        /// <param name="snpID" type="int">TECHNICAL_RESULT_ID owner of the SNP</param>
        /// <param name="isChecked" type="string">Indicates if the SNP is being added or removed</param>
        /// <param name="element" type="object">DOM element of the checkbox in the SNP record on section</param>

        var Super = this;
        id = id + '';
        snpID = snpID + '';
        var diseaseName = element.closest('tr').find('[data-rowcolumnname="enfermedad"]').html().trim();

        if (isChecked == 'Y') {
            // If the disease is not in reportDiseases yet
            if (typeof this.reportDiseases[id] == 'undefined') {
                var data = { ID: id, NAME: diseaseName, TEXT: '', ELEMENTS: snpID, FINISHED: 'N' };
                this.buildInterpretationDisease(data);

                // Re-set the editable status
                var marriedWithMe = (this.settings.modulesData.MARRIED_WITH_ME.trim() == 'Y');
                this.setEditableModule('collapseModuleInterpretation', this.settings.modulesEditable['Interpretation'] && marriedWithMe);
            } else {
                this.reportDiseases[id].push(snpID);
            }
            // Show interpretation controls (pre-llenado and save all)
            $('.interpretationActionButtonsContainer').show();

        } else {
            // If the disease exist in reportDiseases
            if (typeof this.reportDiseases[id] != 'undefined') {
                // Remove the TECHNICAL_REPORT_ID from persistence data
                var index = this.reportDiseases[id].indexOf(snpID);
                if (index > -1) {
                    this.reportDiseases[id].splice(index, 1);
                }

                // If is the last element of the disease, remove the textbox and the record from reportDiseases
                if (this.reportDiseases[id].length == 0) {
                    $('#inteContain_' + id).remove();
                    delete this.reportDiseases[id];
                }
            }

            if (Object.keys(this.reportDiseases).length == 0) {
                $('.interpretationActionButtonsContainer').hide();
            }

        }

    }
    ResultsComponent.prototype.preloadInterpretationContent = function () {
        /// <summary>Load the templated text for all empty interpretations</summary>

        var data = {};
        data['BARCODE'] = this.settings.barCode;
        data['INTERPRETATIONS'] = [];
        for (var fdx in this.reportDiseases) {
            if ($('#txtIntegracion_' + fdx).val().trim() == '') {
                data['INTERPRETATIONS'].push({ ID: fdx });
            }
        }

        if (data['INTERPRETATIONS'].length > 0) {
            $('<span class="loadingPreloadedSpan text-danger"></span>').
                css('margin-right', '20px').
                html('<i class="fa fa-cog fa-spin"></i>&nbsp;Cargando... ').
                insertBefore('.btnPreloadInterpretation');

            this.ajax('/MedicosAnalistas/api/Medicos/GetPreloadInterpretation',
                'POST', 'JSON', data,
                function (objResponse) { // response callback
                    $('.loadingPreloadedSpan').remove();
                    if (objResponse != null && Object.keys(objResponse).length > 0) {
                        for (var tmpKey in objResponse) {
                            $('#txtIntegracion_' + tmpKey).val(objResponse[tmpKey]);
                        }
                    }
                }, null,
                function () { // Error callback
                    $('.loadingPreloadedSpan').remove();
                    $('<span class="errorPreloadedSpan text-danger"></span>').css('margin-right', '20px').
                        html('No se pudo pre-llenar el contenido.').
                        insertBefore('.btnPreloadInterpretation');
                    setTimeout(function () { $('.errorPreloadedSpan').remove(); }, 2000);
                }, null
            );
        } else {
            $('<span class="errorPreloadedSpan text-danger"></span>').css('margin-right', '20px').
                        html('Todos los campos de interpretación tienen contenido.').
                        insertBefore('.btnPreloadInterpretation');
            setTimeout(function () { $('.errorPreloadedSpan').remove(); }, 2000);
        }
    }
    ResultsComponent.prototype.updateAllInterpretations = function () {
        /// <summary>Make the requests for update all the interpretations</summary>

        var Super = this;
        var data = {};
        data['BARCODE'] = this.settings.barCode;
        data['INTERPRETATIONS'] = [];
        for (var fdx in this.reportDiseases) {
            var tmpElem = {
                ID: fdx,
                ELEMENTS: this.reportDiseases[fdx].join(),
                TEXT: $('#txtIntegracion_' + fdx).val(),
                FINISHED: ($('#cboIntegracion_' + fdx).is(':checked')) ? 'Y' : 'N'
            };
            data['INTERPRETATIONS'].push(tmpElem);
        }

        this.ajax('/MedicosAnalistas/api/Medicos/UpdateInterpretacion',
            'POST', 'JSON', data,
            function (objResponse) {

                var confirmation;
                var confirmationClone;
                if (objResponse) {
                    confirmation = $('<div></div>').addClass('alert alert-success').
                        css({ 'margin-top': '5px', 'padding': '5px', 'margin-bottom': '0px', 'text-align': 'center' }).
                        html('Cambios guardados');
                } else {
                    confirmation = $('<div></div>').addClass('alert alert-warning').
                        css({ 'margin-top': '5px', 'padding': '5px', 'margin-bottom': '0px', 'text-align': 'center' }).
                        html('No fue posible guardar los cambios, intentelo más tarde');
                }

                confirmationClone = confirmation.clone().css({ 'margin-top': '0px', 'margin-bottom': '5px' });
                Super.moduleInterpretationsBodyContent.append(confirmation);
                Super.moduleInterpretationsBodyContent.prepend(confirmationClone);
                setTimeout(function () { confirmation.remove(); confirmationClone.remove(); }, 2000);
            },
            null,
            function () {
                var confirmationClone;
                var confirmation = $('<div></div>').addClass('alert alert-warning').
                        css({ 'margin-top': '5px', 'padding': '5px', 'margin-bottom': '0px', 'text-align': 'center' }).
                        html('No fue posible guardar los cambios, revise su conexión');
                confirmationClone = confirmation.clone().css({ 'margin-top': '0px', 'margin-bottom': '5px' });

                Super.moduleInterpretationsBodyContent.append(confirmation);
                Super.moduleInterpretationsBodyContent.prepend(confirmationClone);
                setTimeout(function () { confirmation.remove(); confirmationClone.remove(); }, 2000);
            }, null);
    }
    ResultsComponent.prototype.updateSimpleInterpretatio = function (diseaseID) {
        var Super = this;
        var data = {};

        data['BARCODE'] = this.settings.barCode;
        data['INTERPRETATIONS'] = [];
        var interpretationData = {
            ID: diseaseID,
            ELEMENTS: (typeof this.reportDiseases[diseaseID] != 'undefined') ? this.reportDiseases[diseaseID].join() : '',
            TEXT: ($('#txtIntegracion_' + diseaseID).length > 0) ? $('#txtIntegracion_' + diseaseID).val().trim() : '',
            FINISHED: ($('#cboIntegracion_' + diseaseID).length > 0 && $('#cboIntegracion_' + diseaseID).is(':checked')) ? 'Y' : 'N'
        };
        data['INTERPRETATIONS'].push(interpretationData);

        this.ajax('/MedicosAnalistas/api/Medicos/UpdateSingleInterpretacion',
            'POST', 'JSON', data,
            function (objResponse) {
                var message = $('<span></span>').
                    addClass('text-success singleInterUpdateMessage').
                    css('margin-left', '10px').
                    css('font-weight', 'bold').
                    html('Cambio guardado.');

                message.insertBefore('#txtIntegracion_' + diseaseID);
                setTimeout(function () { message.remove(); }, 2000);
            },
            null,
            function () {
                var message = $('<span></span>').
                    addClass('text-danger singleInterUpdateMessage').
                    css('margin-left', '10px').
                    css('font-weight', 'bold').
                    html('No fue posible guardar el cambio');

                message.insertBefore('#txtIntegracion_' + diseaseID);
                setTimeout(function () { message.remove(); }, 2000);
            }, null);
    }

    // SNP selected functions
    ResultsComponent.prototype.changeSelectedStatus = function (type, id, snpID, isChecked, element) {
        /// <summary>Make the request for set/unset a SNP element as selected</summary>
        /// <param name="type" type="string">Type of result item (Complex_Diseases, traits, etc)</param>
        /// <param name="id" type="string">Result ID owner of the SNP element</param>
        /// <param name="snpID" type="int">Technical_Report_ID owner of the SNP element</param>
        /// <param name="isChecked" type="string">Indicates if the element is checked or unchecked</param>
        /// <param name="element" type="object">DOM element that contains the SNP element</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.updateReportSelectionCounter(element, isChecked);

                if (type == 'Inherited_Conditions') {
                    if (isChecked == 'Y') { Super.monogenicCount++; }
                    else { Super.monogenicCount--; }
                }

            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                // No fue posible actualizar la seleccion
            }
        }

        // Refresh reportDiseases
        if (this.settings.modulesVisible.Interpretation && type.trim() == 'Complex_Diseases') {
            this.refreshReportDiseases(type, id, snpID, isChecked, element);
        }
        this.onSnpIntCheckChange();

        var dataObject = { BARCODE: this.settings.barCode, TYPE: type, DISEASEID: id, SNPID: snpID, CHECKED: isChecked };
        var requestUrl = this.settings.changeCheckedStatusUrl + '?' + jQuery.param(dataObject);

        xmlhttp.open("GET", requestUrl, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send();
    }
    ResultsComponent.prototype.updateReportSelectionCounter = function (element, isChecked) {
        /// <summary>Update the selection counter on section when a SNP is selected or diselected</summary>
        /// <param name="element" type="object">DOM element that contains the SNP element</param>
        /// <param name="isChecked" type="string">Indicates if the SNP element is checked or not</param>

        var section = $(element).closest('.panel-collapse').data('section-id');
        var indicatorButton = $('.section-span-title[data-section-id="' + section + '"]').find('.reportCount');
        var currentValue = indicatorButton.html().trim().replace('R', '');
        var intValue = parseInt(currentValue);

        if (isChecked == 'Y') { intValue++; }
        else { intValue--; }

        indicatorButton.html(' ' + intValue + 'R ');
        indicatorButton.removeClass('btn-green btn-gray-text');
        if (intValue > 0) indicatorButton.addClass('btn-green');
        else indicatorButton.addClass('btn-gray-text');
    }

    // SNP Table functions
    ResultsComponent.prototype.getTableHead = function (sectionID) {
        /// <summary>Buil the HTML head of a SNP Table</summary>
        /// <param name="sectionID" type="string">ID of section to build</param>

        var html = "";
        var columns = this.objData[sectionID].columns;
        html += '<thead>';
        html += '   <tr>';

        html += '       <th style="width: 70px;">';
        html += '           REPORTAR';
        html += '           <span class="order-button" data-rowColumnName="reportar"> ';
        html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
        html += '           </span>';
        html += '       </th>';

        if (typeof columns['traitType'] != 'undefined') {
            html += '       <th class="border-color">';
            html += '           ' + columns['traitType'];
            html += '           <span class="order-button" data-rowColumnName="traitType"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['or'] != 'undefined') {
            html += '       <th  style="width: 35px;" class="border-color"> ';
            html += '           ' + columns['or'];
            html += '           <span class="order-button" data-rowColumnName="or"> ';
            html += '               <i class="fa fa-caret-down" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['snp'] != 'undefined') {
            html += '       <th style="width: 65px;" class="border-color">';
            html += '           ' + columns['snp'];
            html += '           <span class="order-button" data-rowColumnName="snp"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span> ';
            html += '       </th>';
        }

        if (typeof columns['gen'] != 'undefined') {
            html += '       <th class="border-color">';
            html += '           ' + columns['gen'];
            html += '           <span class="order-button" data-rowColumnName="gen"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['genotipo'] != 'undefined') {
            html += '       <th style="width: 65px;" class="border-color">';
            html += '           ' + columns['genotipo'];
            html += '           <span class="order-button" data-rowColumnName="genotipo"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['technicalReport'] != 'undefined') {
            html += '       <th class="border-color">' + columns['technicalReport'] + '</th>';
        }

        if (typeof columns['interpretation'] != 'undefined') {
            html += '       <th class="border-color">';
            html += '           ' + columns['interpretation'];
            html += '           <span class="order-button" data-rowColumnName="interpretation"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['enfermedad'] != 'undefined') {
            html += '       <th class="border-color">';
            html += '           ' + columns['enfermedad'];
            html += '           <span class="order-button" data-rowColumnName="enfermedad"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['confianza'] != 'undefined') {
            html += '       <th style="width: 65px;" class="border-color">';
            html += '           ' + columns['confianza'];
            html += '           <span class="order-button" data-rowColumnName="confianza"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['estatus'] != 'undefined') {
            html += '       <th class="border-color">';
            html += '           ' + columns['estatus'];
            html += '           <span class="order-button" data-rowColumnName="estatus"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['outcome'] != 'undefined') {
            html += '       <th class="border-color">';
            html += '           ' + columns['outcome'];
            html += '           <span class="order-button" data-rowColumnName="outcome"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['riesgo'] != 'undefined') {
            html += '       <th style="width: 60px;" class="border-color">';
            html += '           ' + columns['riesgo'];
            html += '           <span class="order-button" data-rowColumnName="riesgo"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['riesgoPromedio'] != 'undefined') {
            html += '       <th style="width: 75px;" class="border-color">';
            html += '           ' + columns['riesgoPromedio'];
            html += '           <span class="order-button" data-rowColumnName="riesgoPromedio"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        if (typeof columns['vsMedia'] != 'undefined') {
            html += '       <th style="width: 70px;" class="border-color">';
            html += '           ' + columns['vsMedia'];
            html += '           <span class="order-button" data-rowColumnName="vsMedia"> ';
            html += '               <i class="fa fa-sort" aria-hidden="true"></i> ';
            html += '           </span>';
            html += '       </th>';
        }

        html += '   </tr>';
        html += '</thead>';
        return html;
    }
    ResultsComponent.prototype.getTableData = function (sectionID) {
        /// <summary>Make the request for load a SNP Table</summary>
        /// <param name="sectionID" type="string">ID of section to load</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onTableReady(xmlhttp.responseText, sectionID);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                //$(Super.parentContainer).html('<div style="text-align: center;"><h4>Error cargando los datos</h4> </div>');
            }
        }

        var dataObject = { SECTIONID: sectionID, REASONID: this.settings.reasonDiseaseID, BARCODE: this.settings.barCode };
        var requestUrl = this.settings.tableUrl + '?' + jQuery.param(dataObject);

        xmlhttp.open("GET", requestUrl, true);
        xmlhttp.setRequestHeader("UUID", uuid);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        xmlhttp.send();
    }
    ResultsComponent.prototype.onTableReady = function (response, sectionID) {
        /// <summary>Load data into SNP table</summary>
        /// <param name="response" type="object">JSON object with the data to render on the table</param>
        /// <param name="sectionID" type="string">ID of the section to load</param>

        var container = $('#collapse' + sectionID).find('.panel-body');
        var outcomeExcludingValues = ['tipico', 'promedio', 'no hay datos', 'probable', 'promedio', 'percibir'];
        var traitsTypesNames = { 'Adiccion': 'Adicción', 'Medicina_Interna_Reumatologia': 'MIyReuma', 'MIyReuma': 'MIyReuma', 'Oncologia': 'Oncología', 'Psiquiatria': 'Psiquiatría', 'Rasgo_Personal': 'Rasgo', };
        if (container.length > 0) {
            var columns = this.objData[sectionID].columns;
            var html = '';
            container = container[0];

            var xmlRowData = null;
            if (window.DOMParser) {
                var parser = new DOMParser();
                xmlRowData = parser.parseFromString(response, "text/xml");
            } else {
                xmlRowData = new ActiveXObject("Microsoft.XMLDOM");
                xmlRowData.async = false;
                xmlRowData.loadXML(response);
            }

            if (xmlRowData != null) {
                html += '<table class="results-table" style="table-layout: fixed;">';
                html += this.getTableHead(sectionID);
                html += '   <tbody>';

                for (var idx = 0; idx < this.getNodesCount(xmlRowData, 'ReportElement') ; idx++) {
                    var node = this.getNodes(xmlRowData, 'ReportElement')[idx];
                    var id = this.getNodeData(node, 'ID');
                    var snpID = this.getNodeData(node, 'SNPID');
                    var type = this.getNodeData(node, 'TYPE');
                    var comments = this.getNoDocumentNodes(node, 'Comment');
                    var selected = (this.getNodeData(node, 'REPORT') == 'Y');
                    var hasComments = (comments.length > 0);

                    // ===================== Gen monogenic count
                    if ((sectionID == '2' || sectionID == 2) && selected) {
                        this.monogenicCount++;
                    }

                    // ===================== REPORTAR
                    var rowInfoClass = (selected) ? 'selected-row snp-row' : 'snp-row';
                    rowInfoClass = (hasComments) ? rowInfoClass + ' snp-row-comment' : rowInfoClass;

                    html += '   <tr class="' + rowInfoClass + '">';
                    html += '       <td data-rowColumnName="reportar">';
                    html += '           <div>';

                    if (hasComments) {
                        html += '           <span class="reportar-symbols-exclamation text-danger" data-section="' + sectionID + '" data-element="' + id + '"  data-snp="' + snpID + '" data-type="' + type + '"> <i class="fa fa-exclamation-circle" aria-hidden="true" data-toggle="tooltip" title="Mostrar/Ocultar comentarios"></i> </span>';
                    } else {
                        html += '           <span class="empty-exclamation">&nbsp;&nbsp;&nbsp;</span>';
                    }
                    if (selected) {
                        html += '           <input type="checkbox" class="cboRowSelect innerModuleControl" checked />';
                    } else {
                        html += '           <input type="checkbox" class="cboRowSelect innerModuleControl" />';
                    }

                    html += '               <span class="reportar-symbols-plus text-success innerModuleControl" data-section="' + sectionID + '" data-element="' + id + '"  data-snp="' + snpID + '" data-type="' + type + '"> &nbsp; <i class="fa fa-plus-circle" aria-hidden="true" data-toggle="tooltip" title="Agregar nuevo comentario"></i> </span>';
                    html += '           </div>';
                    html += '       </td>';

                    // ===================== TRAIT_TYPE
                    if (typeof columns['traitType'] != 'undefined') {
                        var currTRAIT = this.getNodeData(node, 'TRAIT_TYPE');
                        if (currTRAIT.trim() == '') { currTRAIT = 'NA'; }
                        else { currTRAIT = traitsTypesNames[currTRAIT]; }
                        html += '<td data-rowColumnName="traitType" class="colored-cell border-color"> ' + currTRAIT + ' </td>';
                    }

                    // ===================== OR
                    if (typeof columns['or'] != 'undefined') {
                        var currOR = this.getNodeData(node, 'OR');
                        if (currOR.trim() == 'NOT APPLICABLE' || currOR.trim() == '') currOR = 'NA';
                        if (currOR.trim().length > 4) currOR = currOR.substring(0, 4);

                        var tmpNumOR = parseFloat(currOR.trim());
                        if (!isNaN(tmpNumOR) && tmpNumOR >= this.settings.orRatioRange) {
                            currOR = '<span style="color: red;">' + currOR + '</span>';
                        } else {
                            currOR = '<span>' + currOR + '</span>';
                        }
                        html += '<td data-rowColumnName="or" class="colored-cell border-color"> ' + currOR + ' </td>';
                    }

                    // ===================== SNP
                    if (typeof columns['snp'] != 'undefined') {
                        html += '<td data-rowColumnName="snp" class="colored-cell border-color"> ' + this.getNodeData(node, 'SNP') + ' </td>';
                    }

                    // ===================== GEN
                    if (typeof columns['gen'] != 'undefined') {
                        var currGENORRE = this.getNodeData(node, 'GENORREGION');
                        if (currGENORRE.trim() == '') { currGENORRE = 'NA'; }
                        else {
                            currGENORRE = currGENORRE.toUpperCase();
                            currGENORRE = currGENORRE.replace(/,/g, '<br />');
                            currGENORRE = '<span style="font-style: italic;">' + currGENORRE + '</span>';
                        }
                        html += '<td data-rowColumnName="gen" class="colored-cell border-color"> ' + currGENORRE + ' </td>';
                    }

                    // ===================== GENOTYPE
                    if (typeof columns['genotipo'] != 'undefined') {
                        html += '<td data-rowColumnName="genotipo" class="colored-cell border-color"> ' + this.getNodeData(node, 'GENOTYPE') + ' </td>';
                    }

                    // ===================== TECHNICAL_REPORT
                    if (typeof columns['technicalReport'] != 'undefined') {
                        html += '<td data-rowColumnName="informe" class="colored-cell border-color"> <a class="informeLink" data-section="' + sectionID + '" data-element="' + id + '"  data-snp="' + snpID + '"> Informe Técnico </a> </td>';
                    }

                    // ===================== INTERPRETATION
                    if (typeof columns['interpretation'] != 'undefined') {
                        html += '<td data-rowColumnName="interpretation" class="colored-cell border-color"> ' + this.getNodeData(node, 'INTERPRETATION') + ' </td>';
                    }

                    // ===================== NAME (enfermedad)
                    if (typeof columns['enfermedad'] != 'undefined') {
                        html += '<td data-rowColumnName="enfermedad" class="colored-cell border-color"> ' + this.getNodeData(node, 'NAME') + ' </td>';
                    }

                    // ===================== CONFIANZA
                    if (typeof columns['confianza'] != 'undefined') {
                        var currCONF = this.getNodeData(node, 'CONFIDENCE');
                        if (currCONF.trim() == '') currCONF = 'NA';
                        html += '<td data-rowColumnName="confianza" class="colored-cell border-color"> ' + currCONF + ' </td>';
                    }

                    // ===================== ESTATUS
                    if (typeof columns['estatus'] != 'undefined') {
                        var currSTATUS = this.getNodeData(node, 'STATUS');
                        if (sectionID == 2 && currSTATUS.trim() == 'Variante Presente') {
                            currSTATUS = '<span style="color: red;">' + currSTATUS + '</status>';
                        }
                        if ((sectionID == 3 || sectionID == 'reasonmedSNP') && currSTATUS.toLowerCase().indexOf('normal') < 0) {
                            currSTATUS = '<span style="color: red;">' + currSTATUS + '</status>';
                        }
                        html += '<td data-rowColumnName="estatus" class="colored-cell border-color"> ' + currSTATUS + ' </td>';
                    }

                    // ===================== OUTCOME
                    if (typeof columns['outcome'] != 'undefined') {
                        var currOUTCOME = this.getNodeData(node, 'OUTCOME');
                        var mark = false;

                        if (sectionID == 4 && currOUTCOME.trim().length > 0) {
                            var outcomeLower = currOUTCOME.toLowerCase();
                            for (var jdx in outcomeExcludingValues) {
                                if (outcomeLower.indexOf(outcomeExcludingValues[jdx]) >= 0) {
                                    mark = true;
                                    break;
                                }
                            }
                        }

                        if (!mark) {
                            currOUTCOME = '<span style="color: red;">' + currOUTCOME + '</span>';
                        }
                        html += '<td data-rowColumnName="outcome" class="colored-cell border-color">' + currOUTCOME + '</td>';

                    }

                    // ===================== RIEZGO
                    if (typeof columns['riesgo'] != 'undefined') {
                        var currRISK = this.getNodeData(node, 'RISK');
                        var currAVGRISK = this.getNodeData(node, 'AVGRISK');
                        if (currRISK.trim() == 'NOT APPLICABLE') currRISK = 'NA';
                        currRISK = (currRISK != '-' && currRISK.length > 4) ? currRISK.substring(0, 4) + ' %' : currRISK;
                        var tmpNumRisk = parseFloat(currRISK.replace('%', '').trim());
                        var tmpNumRiskProm = parseFloat(currAVGRISK.replace('<', '').replace('%', '').trim());

                        if (!isNaN(tmpNumRisk) && !isNaN(tmpNumRiskProm) && tmpNumRisk > tmpNumRiskProm) {
                            currRISK = '<span style="color: red;">' + currRISK + '</span>';
                        } else {
                            currRISK = '<span>' + currRISK + '</span>';
                        }
                        html += '<td data-rowColumnName="riesgo" class="colored-cell border-color"> ' + currRISK + ' </td>';
                    }

                    // ===================== AVGRISK
                    if (typeof columns['riesgoPromedio'] != 'undefined') {
                        var currAVGRISK = this.getNodeData(node, 'AVGRISK');
                        if (currAVGRISK.trim() == 'NOT APPLICABLE' || currAVGRISK.trim() == '') currAVGRISK = 'NA';
                        html += '<td data-rowColumnName="riesgoPromedio" class="colored-cell border-color"> ' + currAVGRISK + ' </td>';
                    }

                    // ===================== VSAVG
                    if (typeof columns['vsMedia'] != 'undefined') {
                        var currVSAVG = this.getNodeData(node, 'VSAVG');
                        if (currVSAVG.trim() == 'NOT APPLICABLE') currVSAVG = 'NA';
                        var tmpNumVsMed = parseFloat(currVSAVG.replace('x', '').trim());
                        if (!isNaN(tmpNumVsMed) && tmpNumVsMed >= this.settings.vsAvgRange) {
                            currVSAVG = '<span style="color: red;">' + currVSAVG + '</span>';
                        } else {
                            currVSAVG = '<span>' + currVSAVG + '</span>';
                        }
                        html += '<td data-rowColumnName="vsMedia" class="colored-cell border-color"> ' + currVSAVG + ' </td>';
                    }

                    html += '   </tr>';

                    // Add comments
                    if (comments.length > 0) {
                        var completeList = this.buildCommentBox(sectionID, id, snpID, this.buildCommentList(comments));
                        if (completeList.length > 0) {
                            html += completeList[0].outerHTML;
                        }
                    }

                }

                html += '   </tbody>';
                html += '</table>';
            }

            $(container).html(html);
            $('[data-toggle="tooltip"]').tooltip();

        }

        this.onLoadSectionReady(sectionID);
    }
    ResultsComponent.prototype.sortTable = function (table, column, ascending) {
        /// <summary>Sort a SNP table</summary>
        /// <param name="table" type="object">Jquery object of the table to sort</param>
        /// <param name="column" type="string">Name of the column for sorting</param>
        /// <param name="ascending" type="bool">Indicates if the sort is in ascending order</param>

        var data = [];
        var tbody = table.find('tbody')[0];

        $(tbody).find('.snp-row').each(function () {
            var val;

            if (column == 'reportar') {
                val = ($(this).find('*[data-rowColumnName="' + column + '"]').find('.cboRowSelect').is(':checked')) ? '1' : '0';
            } else if ($(this).find('*[data-rowColumnName="' + column + '"]').find('span').length > 0) {
                val = $(this).find('*[data-rowColumnName="' + column + '"]').find('span').html();
            } else {
                val = $(this).find('*[data-rowColumnName="' + column + '"]').html();
            }

            if (val.trim() == 'NA') val = '';

            if ($(this).hasClass('snp-row-comment')) {
                data.push([val, $(this), $(this).next()]);
            } else {
                data.push([val, $(this)]);
            }
        });

        data.sort(function (x, y) {
            if (ascending) {
                if (x[0].trim() < y[0].trim()) return -1;
                if (x[0].trim() > y[0].trim()) return 1;
                return 0;
            } else {
                if (x[0].trim() < y[0].trim()) return 1;
                if (x[0].trim() > y[0].trim()) return -1;
                return 0;
            }
        });

        $(tbody).html('');
        for (var i = 0; i < data.length; i++) {
            $(tbody).append(data[i][1]);
            if (data[i].length > 2) {
                $(tbody).append(data[i][2]);
            }
        }
    }

    // Technical Report functions
    ResultsComponent.prototype.getTechnicalReportData = function (id, snpID) {
        /// <summary>Make the request for Technical Report data</summary>
        /// <param name="id" type="int">Result ID</param>
        /// <param name="snpID" type="int">TechnicalReportID</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.onTechnicalReportReady(xmlhttp.responseText);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                alert("No fue posible obtener la información del informe");
            }
        }

        var dataObject = { BARCODE: this.settings.barCode, DISEASEID: id, SNPID: snpID };
        var requestUrl = this.settings.technicalReportUrl + '?' + jQuery.param(dataObject);

        xmlhttp.open("GET", requestUrl, true);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send();
    }
    ResultsComponent.prototype.onTechnicalReportReady = function (response) {
        /// <summary>Load data into TechnicalReport modal and show it</summary>
        /// <param name="response" type="object">JSON object with the datato render</param>

        var jsonObj = JSON.parse(response);
        var showInter = (jsonObj.TYPE != 'Complex_Diseases');

        var header = $('<div></div>');
        var genSpan = $('<span></span>').html('<b>Gen o Región:</b> <span style="font-style: italic;">' + jsonObj.GENORREGION.toUpperCase() + '</span>');
        var snpSpan = $('<span></span>').html('<b>SNP:</b> ' + jsonObj.SNP);

        var tableDiv = $('<div></div>').addClass('table-responsive');
        var table = $('<table></table>').addClass('table table-striped table-bordered table-hover');
        var thead = $('<thead></thead>');
        var tbody = $('<tbody></tbody>');
        var theadRow = $('<tr></tr>');
        var tbodyRow = $('<tr></tr>');
        theadRow.append('<th>Número de estudio</th>');
        theadRow.append('<th>Gen o Región</th>');
        theadRow.append('<th>SNP</th>');
        theadRow.append('<th>Genotipo</th>');
        tbodyRow.append('<td>' + jsonObj.SEQUENCER_CODE + '</td>');
        tbodyRow.append('<td style="font-style: italic;">' + jsonObj.GENORREGION.toUpperCase() + '</td>');
        tbodyRow.append('<td>' + jsonObj.SNP + '</td>');
        tbodyRow.append('<td>' + jsonObj.GENOTYPE + '</td>');

        if (!showInter) {
            theadRow.append('<th>Odds Ratio Ajustado</th>');
            tbodyRow.append('<td>' + jsonObj.ODDSRATIO + '</td>');
        }

        thead.append(theadRow);
        tbody.append(tbodyRow);
        table.append(thead);
        table.append(tbody);
        tableDiv.append(table);

        var descriptionDiv = $('<div></div>').css({ 'max-height': '270px', 'overflow-y': 'auto' });
        if (showInter) {
            descriptionDiv.append('<p><b>Interpretación general:</b> ' + jsonObj.INTERPRETATION + '</p>');
        }
        descriptionDiv.append('<p>' + jsonObj.DESCRIPTION + '</p>');

        var bibliographyDiv = $('<div></div>');
        var biblioElements = $('<div></div>').css({ 'max-height': '50px', 'overflow-y': 'auto' });
        if (jsonObj.BIBLIOGRAPHY != null && jsonObj.BIBLIOGRAPHY.length > 0) {
            var pElem = $('<p></p>');
            var aElem = $('<a target="_blank"></a>');

            for (var idx in jsonObj.BIBLIOGRAPHY) {
                biblioElements.append(
                    pElem.clone().
                        append(aElem.clone().
                                attr("href", jsonObj.BIBLIOGRAPHY[idx].LINK).
                                html(jsonObj.BIBLIOGRAPHY[idx].CITATION)
                        )
                );
            }

            bibliographyDiv.append('<br />').append('<p><b>Bibliografía:</b></p>');
            bibliographyDiv.append(biblioElements);
        }

        $('#informModal').find('.modal-body').html('');
        $('#informModal').find('.modal-body').append(tableDiv).append(descriptionDiv).append(bibliographyDiv);

        setTimeout(function () {
            var maxSize = 270 - descriptionDiv.height();
            if (maxSize > 0) {
                biblioElements.css('max-height', (maxSize += 50) + 'px');
            }
        }, 300);
    }

    // SNP Comments build functions
    ResultsComponent.prototype.buildCommentBox = function (sectionID, id, snpID, htmlCommentsList) {
        /// <summary>Buil the table row for display the comments</summary>
        /// <param name="sectionID" type="string">ID of section owner of this comment list</param>
        /// <param name="id" type="int">Result ID</param>
        /// <param name="snpID" type="int">ID of technicalResult</param>
        /// <param name="htmlCommentsList" type="object">HTML list of comments</param>

        var colspan = Object.keys(this.objData[sectionID].columns).length;
        var trElement = $('<tr class="reportCommentViewBox" data-element="' + id + '"  data-snp="' + snpID + '"></tr>');
        var tdElement = $('<td colspan="' + colspan + '"></td>');
        var panelElement = $('<div></div>').addClass('panel panel-default comment-panel-box');
        var panelBody = $('<div></div>').addClass('panel-body comment-panel-body');

        panelBody.append(htmlCommentsList);
        panelElement.append(panelBody);
        tdElement.append(panelElement);
        trElement.append('<td></td>');
        trElement.append(tdElement);

        return trElement;
    }
    ResultsComponent.prototype.buildCommentList = function (commentsNode) {
        /// <summary>Buil the HTML list to show the comments</summary>
        /// <param name="commentsNode" type="object">XML children array that contains the comments to show</param>

        var ulElement = $('<ul></ul>').addClass('comment-panel-list');
        var strongElement = $('<strong></strong>').addClass('primary-font');
        var tmp = null;

        if (commentsNode.length > 0) {
            for (var j = 0; j < commentsNode.length; j++) {
                var isAuthor = (this.getNodeData(commentsNode[j], 'IS_AUTHOR') == 'Y');
                var forReportValue = this.getNodeData(commentsNode[j], 'IS_FOR_REPORT');
                var liElement = $('<li data-uuid="' + this.getNodeData(commentsNode[j], 'UUID') + '" data-forreport="' + forReportValue + '"></li>').addClass('comment-panel-list-item');

                var forReportTooltip = 'Este comentario no se incluirá en el reporte';
                var forReportClass = 'for-report-indicator-off';
                if (forReportValue == 'Y') {
                    forReportTooltip = 'Este comentario si se incluirá en el reporte';
                    var forReportClass = 'for-report-indicator-on';
                }

                liElement.append('<span class="' + forReportClass + '"><i class="fa fa-file-image-o" aria-hidden="true" data-toggle="tooltip" title="' + forReportTooltip + '"></i></span>');
                if (isAuthor) {
                    liElement.append('<span class="delete-comment-button innerModuleControl"><i class="fa fa-times" aria-hidden="true"></i></span>');
                    liElement.append('<span class="edit-comment-button innerModuleControl"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>');
                } else {
                    liElement.append('<span>&nbsp;&nbsp;&nbsp;</span>');
                    liElement.append('<span style="margin-right: 16px;">&nbsp;&nbsp;&nbsp;</span>');
                }

                liElement.append(strongElement.clone().append(this.getNodeData(commentsNode[j], 'AUTHOR') + ': '));
                liElement.append(this.getNodeData(commentsNode[j], 'TEXT'));

                ulElement.append(liElement);
            }
        }

        return ulElement;
    }
    ResultsComponent.prototype.addCommentContents = function (tmpSection, tmpElement, parent, editElement) {
        /// <summary>Build the table row for show the textbox for insert/update a comment</summary>
        /// <param name="tmpSection" type="string">ID of section owner of this comment</param>
        /// <param name="tmpElement" type="int">ID of result owner of this comment</param>
        /// <param name="parent" type="object">Parent DOM element in wich this textbox is going to showed</param>
        /// <param name="editElement" type="object">In case of edit the current data.</param>

        var colspan = Object.keys(this.objData[tmpSection].columns).length;
        var tr = $('<tr id="reportCommentAddBox" data-section="' + tmpSection + '" data-element="' + tmpElement + '"></tr>');
        var td = $('<td colspan="' + colspan + '"></td>');
        var textDiv = $('<div></div>').addClass('form-group').css('margin-bottom', '2px');
        var textArea = $('<textarea id="txtCommentBox"></textarea>').addClass('form-control').
                                                    attr('rows', '3').
                                                    css({ 'resize': 'none', 'margin-top': '2px', 'margin-bottom': '2px' });
        var buttonsDiv = $('<div></div>').addClass('pull-right');

        var checkboxIsForReport = $('<input id="chkCommentIsForReport" type="checkbox"/>').css('vertical-align', 'bottom');
        var checkboxIsForReportLabel = $('<label></label>').css({ 'margin-right': '10px', 'font-weight': '100', 'font-size': '8pt' }).
                                                    append(checkboxIsForReport).
                                                    append('Comentario para reporte');

        if (typeof editElement == 'undefined') {
            var saveButton = $('<button></button>').addClass('btn btn-success btn-xs btn-add-comment').
                                                    css({ 'margin-bottom': '2px', 'margin-right': '2px' }).html('Guardar comentario');
            var cancelButton = $('<button></button>').addClass('btn btn-default btn-xs btn-cancel-comment').
                                                    css('margin-bottom', '2px').html('Cancelar');
            buttonsDiv.append(checkboxIsForReportLabel);
            buttonsDiv.append(saveButton);
            buttonsDiv.append(cancelButton);

            textArea.attr('placeholder', 'Comentario para análisis (No se incluirá en el reporte)');
        } else {
            var editButton = $('<button></button>').addClass('btn btn-success btn-xs btn-edit-comment').
                                                    css({ 'margin-bottom': '2px', 'margin-right': '2px' }).html('Actualizar comentario');
            var cancelButton = $('<button></button>').addClass('btn btn-default btn-xs btn-cancel-edit-comment').
                                                        css('margin-bottom', '2px').html('Cancelar');
            buttonsDiv.append(checkboxIsForReportLabel);
            buttonsDiv.append(editButton);
            buttonsDiv.append(cancelButton);
            textArea.val(editElement.text);
            checkboxIsForReport.prop('checked', (editElement.isForReport == 'Y'));
            if (editElement.isForReport == 'Y') {
                textArea.attr('placeholder', 'Comentario para reporte (Si se incluirá en el reporte)');
            } else {
                textArea.attr('placeholder', 'Comentario para análisis (No se incluirá en el reporte)');
            }

            textDiv.append('<input type="hidden" id="txtCommentUUID" value="' + editElement.uuid + '" />');
        }

        textDiv.append(textArea);

        td.append(textDiv);
        td.append(buttonsDiv);
        tr.append('<td></td>').append(td);
        $(parent).closest('tr').after(tr);

        checkboxIsForReport.change(function () {
            if ($(this).is(':checked')) {
                textArea.attr('placeholder', 'Comentario para reporte (Si se incluirá en el reporte)');
            } else {
                textArea.attr('placeholder', 'Comentario para análisis (No se incluirá en el reporte)');
            }
        });
    }
    ResultsComponent.prototype.buildInsertedComment = function (saveData, prevRow, uuid, forreport) {
        /// <summary>Insert a new comment in the correspond comment list</summary>
        /// <param name="saveData" type="object">JSON object with the data to insert in the list</param>
        /// <param name="prevRow" type="object">Reference row for insert the DOM content</param>
        /// <param name="uuid" type="string">UUID of the comment</param>
        /// <param name="forreport" type="object">Indicates if the comment will be included in the report</param>

        $('#reportCommentAddBox').remove();
        var hasCommentBox = $(prevRow).next().hasClass('reportCommentViewBox');
        var liElement = $('<li data-uuid="' + uuid + '" data-forreport="' + forreport + '"></li>').addClass('comment-panel-list-item');
        var strongElement = $('<strong></strong>').addClass('primary-font');

        var forReportTooltip = 'Este comentario no se incluirá en el reporte';
        var forReportClass = 'for-report-indicator-off';
        if (forreport == 'Y') {
            forReportTooltip = 'Este comentario si se incluirá en el reporte';
            var forReportClass = 'for-report-indicator-on';
        }

        liElement.
                append('<span class="' + forReportClass + '"><i class="fa fa-file-image-o" aria-hidden="true" data-toggle="tooltip" title="' + forReportTooltip + '"></i></span>').
                append('<span class="delete-comment-button innerModuleControl"><i class="fa fa-times" aria-hidden="true"></i></span>').
                append('<span class="edit-comment-button innerModuleControl"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>').
                append(strongElement.clone().append(saveData.AUTHOR + ': ')).
                append(saveData.TEXT);

        if (hasCommentBox) {
            $(prevRow).next().show();
            $(prevRow).next().find('.comment-panel-list').append(liElement);
        } else {
            var ulElement = $('<ul></ul>').addClass('comment-panel-list');
            ulElement.append(liElement);

            var sectionID = $(prevRow).closest('.panel-collapse').data('section-id');
            var trElement = this.buildCommentBox(sectionID, saveData.RESULT_ID, saveData.TECHNICAL_RESULT_ID, ulElement);
            $(prevRow).after(trElement);
            $(prevRow).addClass('snp-row-comment')

            var plusElement = $(prevRow).find('.reportar-symbols-plus');
            var exclamationSpan = plusElement.parent().find('.empty-exclamation')[0];
            $(exclamationSpan).removeClass('empty-exclamation');
            $(exclamationSpan).addClass('reportar-symbols-exclamation text-danger');
            $(exclamationSpan).html('<i class="fa fa-exclamation-circle" aria-hidden="true" data-toggle="tooltip" title="Mostrar/Ocultar comentarios"></i>');
            $(exclamationSpan).find('[data-toggle="tooltip"]').tooltip();
        }

        liElement.find('[data-toggle="tooltip"]').tooltip();
    }
    ResultsComponent.prototype.refreshEditedComment = function (data, rowElement, forreport) {
        /// <summary>Update the content of an edited comment</summary>
        /// <param name="data" type="object">JSON object with the new data</param>
        /// <param name="rowElement" type="object">Reference DOM object for get the comment element</param>
        /// <param name="forreport" type="string">Indicates if the comment will be included in the report</param>

        var forReportTooltip = 'Este comentario no se incluirá en el reporte';
        var forReportClass = 'for-report-indicator-off';
        if (forreport == 'Y') {
            forReportTooltip = 'Este comentario si se incluirá en el reporte';
            var forReportClass = 'for-report-indicator-on';
        }

        var liElement = rowElement.find('li[data-uuid="' + data.UUID + '"]');
        liElement.data('forreport', forreport);
        liElement.html('');
        liElement.
            append('<span class="' + forReportClass + '"><i class="fa fa-file-image-o" aria-hidden="true" data-toggle="tooltip" title="' + forReportTooltip + '"></i></span>').
            append('<span class="delete-comment-button innerModuleControl"><i class="fa fa-times" aria-hidden="true"></i></span>').
            append('<span class="edit-comment-button innerModuleControl"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>').
            append($('<strong></strong>').append(data.AUTHOR + ' ')).
            append(data.TEXT);

        liElement.find('[data-toggle="tooltip"]').tooltip();

        $('#reportCommentAddBox').remove();
        rowElement.show();
    }
    ResultsComponent.prototype.destroyDeletedComment = function (uuid) {
        /// <summary>Remove the DOM element when delete a comment</summary>
        /// <param name="uuid" type="string">UUID of the comment</param>

        var liElement = $('.comment-panel-list-item[data-uuid="' + uuid + '"]');
        var ulElement = liElement.parent();
        liElement.remove();
        if (ulElement.find('.comment-panel-list-item').length == 0) {
            var commentRow = ulElement.closest('tr');
            var snpRow = commentRow.prev();
            commentRow.remove();
            snpRow.removeClass('snp-row-comment');
            var exclamationSpan = snpRow.find('.reportar-symbols-exclamation');
            $(exclamationSpan).removeClass('reportar-symbols-exclamation text-danger');
            $(exclamationSpan).addClass('empty-exclamation');
            $(exclamationSpan).html('&nbsp;&nbsp;&nbsp;');
        }
    }

    // SNP Comments DOM update
    ResultsComponent.prototype.toggleAddComments = function (element) {
        /// <summary>Callback for show/hide the add comment textbox when plus button is clicked</summary>
        /// <param name="element" type="object">DOM element of plus button</param>

        var tmpSection = $(element).data('section');
        var tmpElement = $(element).data('element');
        var isAddOpen = $('#reportCommentAddBox').length;

        if (isAddOpen > 0) {
            $('#reportCommentAddBox').remove();
        } else {
            var hasCommentBox = $(element).closest('tr').next().hasClass('reportCommentViewBox');
            var isCommentBoxVisible = $(element).closest('tr').next().is(":visible");

            if (hasCommentBox && isCommentBoxVisible) {
                $(element).closest('tr').next().hide();
            }

            this.addCommentContents(tmpSection, tmpElement, element);
        }
    }
    ResultsComponent.prototype.toggleComments = function (element) {
        /// <summary>Callback for show/hide the comment list when exclamation button is clicked</summary>
        /// <param name="element" type="object">DOM element of exclamation button</param>

        var tmpSection = $(element).data('section');
        var tmpElement = $(element).data('element');
        var tmpSnp = $(element).data('snp');

        var isAddOpen = $('#reportCommentAddBox').length;
        if (isAddOpen > 0) {
            $('#reportCommentAddBox').remove();
        }

        var hasCommentBox = $(element).closest('tr').next().hasClass('reportCommentViewBox');
        var isCommentBoxVisible = $(element).closest('tr').next().is(":visible");

        if (hasCommentBox) {
            if (isCommentBoxVisible) {
                $(element).closest('tr').next().hide();
            } else {
                $(element).closest('tr').next().show();
            }
        }
    }
    ResultsComponent.prototype.updateReportCommentCounter = function (rowElement, isAdd) {
        /// <summary>Update the comments counter on section when a comment is added or deleted</summary>
        /// <param name="rowElement" type="object">DOM element that contains the comment list</param>
        /// <param name="isAdd" type="bool">Indicates if the comment is added or deleted</param>

        var section = $(rowElement).closest('.panel-collapse').data('section-id');
        var indicatorButton = $('.section-span-title[data-section-id="' + section + '"]').find('.commentsCount');
        var currentValue = indicatorButton.html().trim().replace('C', '');
        var intValue = parseInt(currentValue);
        intValue = (isAdd) ? (intValue + 1) : (intValue - 1);
        indicatorButton.html(' ' + intValue + 'C ');

        indicatorButton.removeClass('btn-yellow btn-gray-text');
        if (intValue > 0) indicatorButton.addClass('btn-yellow');
        else indicatorButton.addClass('btn-gray-text');
    }

    // SNP Comments requests
    ResultsComponent.prototype.onSaveEditCommentClick = function (element) {
        /// <summary>Prepare the data of the comment to update and call to request method</summary>
        /// <param name="element" type="object">DOM element that contains the comment to edit</param>

        var prevRow = $(element).closest('tr').prev();
        var tmpUUID = $('#txtCommentUUID').val();
        var tmpText = $('#txtCommentBox').val();
        var tmpIsForReport = $('#chkCommentIsForReport').is(':checked') ? 'Y' : 'N';

        var saveData = {
            BARCODE: this.settings.barCode,
            RESULT_TYPE: null,
            RESULT_ID: 0,
            TECHNICAL_RESULT_ID: 0,
            AUTHOR: this.settings.authorName,
            UUID: tmpUUID,
            TEXT: tmpText,
            IS_FOR_REPORT: tmpIsForReport
        };

        this.sendUpdateComment(saveData, prevRow, tmpIsForReport);
    }
    ResultsComponent.prototype.sendUpdateComment = function (data, rowElement, forreport) {
        /// <summary>Make the request for update a comment</summary>
        /// <param name="data" type="object">JSON object with the data of the comment to update</param>
        /// <param name="rowElement" type="object">DOM element owner of the comment to edit</param>
        /// <param name="forreport" type="string">Indicates if the comment will be included in the report</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                // Revisar si fue exitosa la modificacion
                Super.refreshEditedComment(data, rowElement, forreport);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                // No fue posible registrar el comentario
            }
        }

        xmlhttp.open("POST", this.settings.editCommentUrl, true);
        xmlhttp.setRequestHeader("UUID", uuid);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send(JSON.stringify(data));
    }
    ResultsComponent.prototype.onAddCommentClick = function (element) {
        /// <summary>Prepare the data of the comment to insert and call to request method</summary>
        /// <param name="element" type="object">DOM element that contains the comment to add</param>

        var prevRow = $(element).closest('tr').prev();
        var plusElement = prevRow.find('.reportar-symbols-plus');
        var tmpType = plusElement.data('type');
        var tmpSection = plusElement.data('section');
        var tmpElement = plusElement.data('element');
        var tmpSnp = plusElement.data('snp');
        var tmpText = $('#txtCommentBox').val();
        var tmpIsForReport = $('#chkCommentIsForReport').is(':checked') ? 'Y' : 'N';

        var saveData = {
            BARCODE: this.settings.barCode,
            RESULT_TYPE: tmpType,
            RESULT_ID: tmpElement,
            TECHNICAL_RESULT_ID: tmpSnp,
            AUTHOR: this.settings.authorName,
            TEXT: tmpText,
            IS_FOR_REPORT: tmpIsForReport
        };

        this.sendNewComment(saveData, prevRow, tmpIsForReport);
    }
    ResultsComponent.prototype.sendNewComment = function (data, rowElement, forreport) {
        /// <summary>Make the request for add a new comment</summary>
        /// <param name="data" type="object">JSON object with the data of the comment to add</param>
        /// <param name="rowElement" type="object">DOM element owner of the new comment</param>
        /// <param name="forreport" type="string">Indicates if the comment will be included in the report</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var value = JSON.parse(xmlhttp.responseText);
                Super.buildInsertedComment(data, rowElement, value, forreport);
                Super.updateReportCommentCounter(rowElement, true);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                // No fue posible registrar el comentario
            }
        }

        xmlhttp.open("POST", this.settings.saveCommentUrl, true);
        xmlhttp.setRequestHeader("UUID", uuid);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send(JSON.stringify(data));
    }
    ResultsComponent.prototype.deleteComment = function (comment_uuid, rowElement) {
        /// <summary>Make the request for delete a comment</summary>
        /// <param name="comment_uuid" type="string">UUID of the comment</param>
        /// <param name="rowElement" type="object">DOM element owner of the deleted comment</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                Super.updateReportCommentCounter(rowElement, false);
                Super.destroyDeletedComment(comment_uuid);
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                // No fue posible eliminar el comentario
            }
        }

        var dataObject = { BARCODE: this.settings.barCode, UUID: comment_uuid };
        var requestUrl = this.settings.deleteCommentUrl + '?' + jQuery.param(dataObject);

        xmlhttp.open("GET", requestUrl, true);
        xmlhttp.setRequestHeader("UUID", uuid);
        xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send();
    }
    ResultsComponent.prototype.onCancelCommentClick = function (element) {
        /// <summary>Hide the add/update textbox when cancel is clicked</summary>
        /// <param name="element" type="object">DOM element that contains the comment</param>

        $('#reportCommentAddBox').fadeOut(250, function () {
            var next = element.closest('tr').next();
            if (next.hasClass('reportCommentViewBox')) {
                next.show();
            }
            $('#reportCommentAddBox').remove();
        });
    }

    // Module estatus functions
    ResultsComponent.prototype.setEditableModule = function (module, enabled) {
        /// <summary>Enable or disable the editable components of a module</summary>
        /// <param name="module" type="string">Module name (DOM id)</param>
        /// <param name="enabled" type="bool">Indicates if the module is editable</param>

        if (module == 'collapseModuleSNP') {
            this.snpEditable = enabled;
            $('#reportCommentAddBox').remove();
            $('#' + module).find('.innerModuleControl.cboRowSelect').prop('disabled', !enabled);

            if (enabled) {
                $('#' + module).find('.innerModuleControl:not(.cboRowSelect)').removeClass('moduleDisabled');
            } else {
                $('#' + module).find('.innerModuleControl:not(.cboRowSelect)').addClass('moduleDisabled');
            }
        } else if (module == 'collapseModuleInterpretation') {
            $('#' + module).find('.innerModuleControl').prop('disabled', !enabled);

            $('.cboInterpretationDone').each(function () {
                var interID = $(this).attr('id').substring(15);
                $('#txtIntegracion_' + interID).prop('disabled', !(enabled && !($(this).is(':checked'))));
            });
        } else {
            $('#' + module).find('.innerModuleControl').prop('disabled', !enabled);
        }

    }
    ResultsComponent.prototype.setCompletedModule = function (module, completed) {
        /// <summary>Change the color of the head of the module</summary>
        /// <param name="module" type="string">Module name (DOM id)</param>
        /// <param name="completed" type="bool">Indicates if module is completed</param>

        var statusClass = (completed) ? 'completed-module-header' : 'uncompleted-module-header';
        if ($('#' + module).prev().hasClass('panel-heading')) {
            $('#' + module).prev().removeClass('completed-module-header uncompleted-module-header');
            $('#' + module).prev().addClass(statusClass);
        }
    }
    ResultsComponent.prototype.setOpeneableModule = function (module, openeable) {
        /// <summary>Hide the collapse in order to disable the open event</summary>
        /// <param name="module" type="string">Module name (DOM id)</param>
        /// <param name="openeable" type="bool">Indicates if the module is openeable</param>

        if (openeable) {
            var elementIndex = this.notOpeneableModules.indexOf(module);
            if (elementIndex >= 0) {
                this.notOpeneableModules.splice(elementIndex, 1);
            }
        } else {
            this.notOpeneableModules.push(module);
            if ($('#' + module).css('display') != 'none') {
                $('#' + module).collapse('hide');
            }
        }
    }
    ResultsComponent.prototype.assignModuleStatus = function (module) {
        /// <summary>Set the module status properties based on settings</summary>
        /// <param name="module" type="string">Module name (DOM id)</param>

        var moduleID = 'collapseModule' + module;

        // Logic for editable permission
        var marriedWithMe = (this.settings.modulesData.MARRIED_WITH_ME.trim() == 'Y');
        this.setEditableModule(moduleID, this.settings.modulesEditable[module] && marriedWithMe);

        // Logic for complete module
        var tmpModuleCompleteStatus = (this.settings.modulesData.DETAIL[module].STATUS == 'Y');
        this.setCompletedModule(moduleID, tmpModuleCompleteStatus);
        $('*[data-id="chkModule' + module + '"]').prop('checked', tmpModuleCompleteStatus);

        this.onSnpIntCheckChange();
    }
    ResultsComponent.prototype.onSnpIntCheckChange = function () {
        /// <summary>Refresh the disabled/enable status of sign button based on the status of SNPs and Interpretations</summary>

        var signDisabled = false;
        var errorMessage = [];

        // Con base en los permisos del usuario (modulos que puede modificar) se deben de asignar
        // la propiedad de firmar el reporte. Debe de haber concluido el módulo que le corresponde
        var permisoSNP = this.settings.modulesEditable.SNP;
        var permisoInterpretations = this.settings.modulesEditable.Interpretation;
        var marriedWithMe = (this.settings.modulesData.MARRIED_WITH_ME.trim() == 'Y');

        $('.signatureExplanationDiv').html('');

        if (permisoSNP && $('.cboRowSelect:checked').length == 0) {
            signDisabled = true;
            errorMessage.push("Debe seleccionar para reporte por lo menos un registro de SNP.");
        }

        if (permisoInterpretations && $('.cboInterpretationDone').length > $('.cboInterpretationDone:checked').length) {
            signDisabled = true;
            errorMessage.push("Es necesario marcar todos los textos de interpretación como concluidos.");
        }

        $('#btnSignatureSing').prop('disabled', signDisabled || !this.settings.modulesEditable.Signature || !marriedWithMe);
        if (signDisabled) {
            $('.signatureExplanationDiv').append($('<div></div>').addClass('text-danger').css('font-weight', 'bold').html('Aun no puede firmar el reporte'));

            for (var idx in errorMessage) {
                $('.signatureExplanationDiv').
                    append($('<div></div>').addClass('text-danger disabledSignErrorMessage').html(errorMessage[idx]));
            }
        } else {
            $('.signatureExplanationDiv').html(this.settings.signatureExplanationText);
        }

    }

    // Section loading callbacks
    ResultsComponent.prototype.onLoadSectionReady = function (sectionID) {
        /// <summary>Callback called when a module ends its loading process</summary>
        /// <param name="sectionID" type="string">ID of section</param>

        this.loadedSections++;
        if (this.loadedSections >= Object.keys(this.objData).length && !this.loadedSectionsEnd) {
            this.loadedSectionsEnd = true;
            this.onLoadAllSectionsReady();
        }


        this.assignModuleStatus('SNP');
        this.assignModuleStatus('Interpretation');
        this.assignModuleStatus('Signature');
        this.assignModuleStatus('Specialist');
    }
    ResultsComponent.prototype.onLoadAllSectionsReady = function () {
        /// <summary>Callback called when all modules are loaded</summary>

        this.onSnpIntCheckChange();
        this.onComponentLoadedReady();
        this.onSnpIntCheckChange();
    }
    ResultsComponent.prototype.onComponentLoadedReady = function () {
        /// <summary>Callback for overwhite called when the entry component is loaded.</summary>

    }

    // Take case logic
    ResultsComponent.prototype.takeCase = function (element) {
        var Super = this;
        var takeTaskConfirm = new Confirm({ confirm: true });
        var btnText = element.html();

        if (btnText.trim() == 'Tomar el caso') {
            takeTaskConfirm.setTitle('Tomar el caso');
            takeTaskConfirm.setBody('¿Esta seguro que desea tomar este caso?');
        } else if (btnText.trim() == 'Regresar al pool') {
            takeTaskConfirm.setTitle('Remover el caso');
            takeTaskConfirm.setBody('¿Esta seguro que desea remover este caso del usuario actual?');
        }
        else {
            takeTaskConfirm.setTitle('Abandonar el caso');
            takeTaskConfirm.setBody('Esta a punto de abandonar este caso, ¿Está seguro?');
        }

        takeTaskConfirm.show();
        takeTaskConfirm.onAcceptClick = function () {

            var btnText = $('#btnTakeTask').html();
            var isTakingCase = (btnText.trim() == 'Tomar el caso');
            var data = {
                BARCODE: Super.settings.barCode,
                IS_TAKING_CASE: (isTakingCase) ? 'Y' : 'N'
            };

            Super.ajax('/MedicosAnalistas/api/Medicos/UpdateTakeCase',
                'GET', 'JSON', data,
                function (objResponse) {
                    if (objResponse) {
                        if (isTakingCase) {
                            $('#btnTakeTask').html('Abandonar el caso');
                            $('#btnTakeTask').removeClass('btn-primary');
                            $('#btnTakeTask').addClass('btn-danger');

                            // Update editable parameters and status
                            Super.settings.modulesData.MARRIED_WITH_ME = 'Y';
                            Super.setEditableModule('collapseModuleSNP', this.settings.modulesEditable.SNP);
                            Super.setEditableModule('collapseModuleInterpretation', this.settings.modulesEditable.Interpretation);
                            Super.setEditableModule('collapseModuleSignature', this.settings.modulesEditable.Signature);
                        } else {
                            $('#btnTakeTask').html('Tomar el caso');
                            $('#btnTakeTask').removeClass('btn-danger');
                            $('#btnTakeTask').addClass('btn-primary');

                            Super.setEditableModule('collapseModuleSNP', false);
                            Super.setEditableModule('collapseModuleInterpretation', false);
                            Super.setEditableModule('collapseModuleSignature', false);
                        }
                    }

                    Super.refreshProcessComponent();
                },
                null, null, null);
        }
    }
    ResultsComponent.prototype.refreshProcessComponent = function () {
    }

    // Helpers
    ResultsComponent.prototype.getRecordDate = function () {
        /// <summary>Get the Report date from settings and format it</summary>

        var strDate = '';
        try {
            var date = new Date(this.settings.reportDate);
            var months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
            strDate = date.getUTCDate() + ' de ' + months[date.getMonth()] + ' de ' + date.getFullYear();
        } catch (e) { }
        return strDate;
    }

    // XML functions
    ResultsComponent.prototype.getElementNodeName = function (rootNode) {
        /// <summary>Get the name of the node removing the fisrt 7 characters)</summary>
        /// <param name="rootNode" type="object">XML root node</param>

        return rootNode.childNodes[0].nodeName.substring(7);
    }
    ResultsComponent.prototype.getNodeData = function (parentNode, nodeName) {
        /// <summary>Get the inner content of a node</summary>
        /// <param name="parentNode" type="object">XML parent node</param>
        /// <param name="nodeName" type="string">Node to obtain the data</param>

        return parentNode.getElementsByTagName(nodeName)[0].innerHTML;
    }
    ResultsComponent.prototype.getNodesCount = function (parentNode, nodeName) {
        /// <summary>Get the number of child elements of a node</summary>
        /// <param name="parentNode" type="object">XML parent node</param>
        /// <param name="nodeName" type="string">Node name to review</param>

        return parentNode.documentElement.getElementsByTagName(nodeName).length;
    }
    ResultsComponent.prototype.getNodes = function (parentNode, nodeName) {
        /// <summary>Get the children elements of a node</summary>
        /// <param name="parentNode" type="object">XML parent node</param>
        /// <param name="nodeName" type="string">Node name to obtain its children</param>

        return parentNode.documentElement.getElementsByTagName(nodeName);
    }
    ResultsComponent.prototype.getNoDocumentNodes = function (parentNode, nodeName) {
        /// <summary>Get the children elements of a node directly from the parent node, without using the documentElement</summary>
        /// <param name="parentNode" type="object">XML parent node</param>
        /// <param name="nodeName" type="string">Node name to obtain its children</param>

        return parentNode.getElementsByTagName(nodeName);
    }

    // Ajax function
    ResultsComponent.prototype.ajax = function (url, method, returnType, sendData, successFunction, successParams, failFunction, failParams) {
        /// <summary>Make an asyncronous call to backend-server</summary>
        /// <param name="url" type="string">Service URL</param>
        /// <param name="method" type="string">Method (GET, POST, PUT, DELETE, ect)</param>
        /// <param name="returnType" type="string">Type of response (JSON, XML)</param>
        /// <param name="sendData" type="object">Parameter object</param>
        /// <param name="successFunction" type="function">Function for success callback</param>
        /// <param name="successParams" type="array">Parameter array for pass them to the success function</param>
        /// <param name="failFunction" type="function">Function for failure callback</param>
        /// <param name="failParams" type="array">Parameter array for pass them to the failure function</param>

        var Super = this;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        // Get the response
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if (successFunction != null) {
                    var tmpParams = successParams;
                    if (tmpParams == null) tmpParams = [];

                    if (xmlhttp.responseText == 'true') { tmpParams.unshift(true); }
                    else if (xmlhttp.responseText == 'false') { tmpParams.unshift(false); }
                    else {
                        if (returnType == 'JSON') {
                            var jsonObj = null;
                            try { jsonObj = JSON.parse(xmlhttp.responseText); }
                            catch (e) { jsonObj = {}; }
                            tmpParams.unshift(jsonObj);
                        } else {
                            tmpParams.unshift(xmlhttp.responseText);
                        }
                    }

                    successFunction.apply(Super, tmpParams);
                }
            } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
                if (failFunction != null) {
                    failFunction.apply(Super, failParams);
                }
            }
        }

        // Open de request
        if (method == 'GET' && (typeof sendData != 'undefined') && sendData != null) {
            xmlhttp.open(method, url + '?' + jQuery.param(sendData));
        } else {
            xmlhttp.open(method, url);
        }

        // Set uuid header
        xmlhttp.setRequestHeader("UUID", uuid);

        // Set contentType and accept headers
        if (returnType == 'JSON') {
            xmlhttp.setRequestHeader("Content-type", "application/json; charset=UTF-8");
            xmlhttp.setRequestHeader("Accept", "application/json");
        } else if (returnType == 'XML') {
            xmlhttp.setRequestHeader("Content-type", "application/xml; charset=utf-8");
            xmlhttp.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        }

        //Send the request
        if (method != 'GET' && (typeof sendData != 'undefined') && sendData != null) {
            xmlhttp.send(JSON.stringify(sendData));
        } else {
            xmlhttp.send();
        }
    }

    // Set settings
    ResultsComponent.prototype.setSettings = function (params) {
        /// <summary>Set the components settings</summary>
        /// <param name="params" type="object">Object for overwrite default settings</param>

        this.settings = $.extend({
            // Services URLs =====================================
            tableUrl: "/MedicosAnalistas/api/Medicos/TableData",
            technicalReportUrl: "/MedicosAnalistas/api/Medicos/TechnicalReportData",
            changeCheckedStatusUrl: "/MedicosAnalistas/api/Medicos/ChangeCheckedStatus",
            commentUrl: "/MedicosAnalistas/api/Medicos/CommentData",
            saveCommentUrl: "/MedicosAnalistas/api/Medicos/SaveComment",
            editCommentUrl: "/MedicosAnalistas/api/Medicos/UpdateComment",
            deleteCommentUrl: "/MedicosAnalistas/api/Medicos/DeleteComment",
            interpretationStartingData: [],
            reasonDiseaseID: 0,
            barCode: '',
            authorName: 'Yo',
            orRatioRange: 1.2,
            vsAvgRange: 1.0,
            caseControlPermission: false,
            modulesVisible: {
                SNP: false,
                Interpretation: false,
                Signature: false,
                Specialist: false
            },
            modulesEditable: {
                SNP: false,
                Interpretation: false,
                Signature: false,
                Specialist: false
            },
            modulesData: null,
            reportDate: '2016-08-17T00:00:00',
            signatureExplanationText: 'Dr. por favor revise el reporte y si está de acuerdo con el diagnóstico fírmelo.',
            specialistExplanationText: 'Dr. por favor en base a los resultados del estudio, asigne la especialidad recomendada para el paciente, para en caso de que no tenga un Dr. tratante, incluyamos la lista de doctores con dicha especialidad en nuestra red como un anexo a su estudio.',
            signatureImagePath: ''
        }, params);
    }

    this.setSettings(params);
    this.init(sections);
}